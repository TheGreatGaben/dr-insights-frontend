# Frontend Monorepo

This is the frontend project for our product named Dr Insights. Dr Insights had an admin web application (pce-admin) as well 
as a web mobile application (pce-app). This was supposed to be a monorepo that holds all our
Vue.js projects, and we would be able to reuse some base code across these projects. The folder structures and the base 
code was done by my previous tech lead. Unfortunately, Dr Insights was the only project where we used Vue.js for the frontend,
after that we decided that we prefer to use React than Vue.js.

The projects are in the `packages/` folder.

The backend project for Dr Insights can be found [here](https://gitlab.com/TheGreatGaben/dr-insights-backend).

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)

![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
![forthebadge](https://forthebadge.com/images/badges/contains-cat-gifs.svg)

## Why monorepo?

- Easier to maintain 
- Easier to share codes across projects (not recommended to share across projects directly, but thru a "base" package)
- Easier to setup simple CI/CD for multiple similar projects
- Clear separation of dependencies
- Is not a monolith

### Why lerna
Maintaining medium to big scale monolith is a bi*!ch. 
Some developers tend to share uncommon scripts/packages across different projects.
You can't have different package versions shared in a monolith  
With **Lerna**, all packages are maintained in an isolated environment without conflicting with each other 

Among the packages, there are projects too. 
To share code among packages, **Lerna** can handle package dependencies by installing one of the package in another package that dependent on it 

**Lerna** allows concurrent command execution for multiple packages. (It's not yet tested, stay tune.)

To read more about **Lerna**
Go familiarise with **Lerna**'s command [here](https://github.com/lerna/lerna/tree/master/commands)

## Development
## Setup
```bash
yarn install
yarn bootstrap
```

#### PCE project apps

#####Start Development

1 - if you haven't setup environment file (if done, skip this step)
```bash
cp ./packages/pce-admin/.env.dev ./packages/pce-admin/.env.local
cp ./packages/pce-app/.env.dev ./packages/pce-app/.env.local 
```

2 - Compile locally
```bash
yarn pce-dev # concurrently run compile 2 apps
# or
yarn pce-app-dev # concurrently run compile app
# or
yarn pce-admin-dev # concurrently run compile admin app
```
- PCE Admin: 
  - port `8080`
- PCE App:
  - port `8081` 

#### Base
- Start Development
```bash
yarn base-dev # in progress
```

## Package Manager
### Yarn
You might be wondering what's `yarn`, please install it, no question ask! 😅
##### What is Yarn
https://yarnpkg.com/lang/en/
##### Installation guide
https://yarnpkg.com/lang/en/docs/install/#mac-stable

