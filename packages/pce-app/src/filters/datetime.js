import {isDate, isValid, format} from 'date-fns';

function validateIsDate(value) {
  if (!isDate(value)) {
    const newDate = new Date(value);

    return isValid(newDate);
  }

  return true;
}

export function datetimeShort(date) {
  return validateIsDate(date) ? format(date, 'YYYYMMDDHHmm') : date;
}

export function datetime(date) {
  return validateIsDate(date) ? format(date, 'YYYY-MM-DD HH:mm:ss') : date;
}

export function monthAndDay(date) {
  if (!isDate(date)) {
    return date;
  }
  const day = format(date, 'DD');
  const lastDayLetter = day.substring(day.length - 1, day.length);
  let daySuffix = 'th';
  switch (lastDayLetter) {
    case '1':
      daySuffix = 'st';
      break;
    case '2':
      daySuffix = 'nd';
      break;
    case '3':
      daySuffix = 'rd';
      break;
  }
  return `${format(date, 'MMMM')} ${format(date, 'D')}${daySuffix}`;
}

export function date(date) {
  return validateIsDate(date) ? format(date, 'YYYY-MM-DD') : date;
}

function pluralize(time, label) {
  if (time === 1) {
    return time + label;
  }
  return time + label + 's';
}

export function timeAgo(time) {
  const between = Date.now() / 1000 - Number(time);
  if (between < 3600) {
    return pluralize(~~(between / 60), ' minute');
  } else if (between < 86400) {
    return pluralize(~~(between / 3600), ' hour');
  } else {
    return pluralize(~~(between / 86400), ' day');
  }
}
