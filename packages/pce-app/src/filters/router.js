import {getRouteTitle as getRouteTitleUtils} from '../utils/router';

export function getRouteTitle(value) {
  return getRouteTitleUtils(value);
}
