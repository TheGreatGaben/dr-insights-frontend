import {each, isEmpty, transform} from 'lodash';

/**
 * Caveat: don't add filters here!!
 *
 * with Vue2-filters
 * added filters:
 * capitalize
 * uppercase
 * lowercase
 * placeholder - exp: {{ msg | placeholder('Placeholder text') }}
 * truncate - exp: truncate(number)
 * currency
 * pluralize
 * limitedBy - exp: limitedBy(Array, number)
 * filterBy - exp: filterBy(Array, value, key?, key?...)
 * find - exp: find(Array, value, key?, key?...)
 * orderBy - exp: find(Array, key), descending exp: find(Array, key, -1)
 */

const requireModule = require.context('.', false, /\.js$/);

/**
 * auto import filters
 * @type {Dictionary<Function>}
 */
const filters = transform(requireModule.keys(), (list, fileName) => {
  if (fileName !== './index.js') {
    const module = requireModule(fileName);

    if (!isEmpty(module)) {
      each(module, (fnc, key) => {
        list[key] = fnc;
      });
    }
  }
});

export default filters;
