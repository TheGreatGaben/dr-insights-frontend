import {isArray, keys, each} from 'lodash'
import {auth} from '@/stores/auth';
import checkPermission from '../../utils/permission'

/**
 * Permission directives
 * default - hide component
 * modifiers:
 * - disabled - emit `disabled` event to component [v-permission.disabled=""]
 * - style - apply style to make element not clickable [v-permission.style=""]
 * - (readOnly|readonly) - add readonly classes and readonly property to input
 */
export const permission = {
  inserted(el, binding, vnode) {
    triggerPermissionCheck(el, binding, vnode)
  },
  componentUpdated(el, binding, vnode) {
    triggerPermissionCheck(el, binding, vnode)
  },
};

const triggerPermissionCheck = function(el, binding, vnode) {
  const {value} = binding;

  if (!value || !isArray(value)) {
    throw new Error(`need roles and permissions! Like v-permission="['{{role}}','{{permission}}']"`)
  } else if (value.length === 0) {
    console.warn('role/permission array is empty in the directive');
    return false
  }

  const hasPermission = checkPermission(value, auth.permissionNameList());

  if (hasPermission) {
    // escape if has permissions
    return false
  }

  const isDisabled = binding.modifiers && binding.modifiers.disabled;
  const isStyle = binding.modifiers && binding.modifiers.style;
  const isReadOnly = binding.modifiers && (binding.modifiers.readOnly || binding.modifiers.readonly);
  const noModifiers = keys(binding.modifiers).length === 0;

  if (noModifiers) {
    // remove node
    el.parentNode && el.parentNode.removeChild(el)
  }

  if (isDisabled) {
    // emit `disabled` event
    emitDisabledEvent(el, binding, vnode)
  }

  if (isReadOnly) {
    // inject readOnly css class and look for input element to inject `read-only` property
    injectReadOnlyProperty(el, binding, vnode)
  }

  if (isStyle) {
    // inject disabled style into element
    injectDisabledStyle(el, binding, vnode)
  }
};

export const emitDisabledEvent = (el, binding, vnode) => {
  vnode.componentInstance.$emit('disabled', true)
};

export const injectReadOnlyProperty = (el) => {
  if (el) {
    const INPUT_LIST = ['select', 'input', 'textarea'];
    const READ_ONLY_CLASSES = ' is-readonly readonly read-only readOnly';
    const isInput = INPUT_LIST.includes(el.tagName);
    const hasReadOnlyClassName = el.className.includes('readonly') || el.className.includes('read-only');

    // add classes to el, parent and child
    if (!hasReadOnlyClassName) {
      el.className += READ_ONLY_CLASSES;

      if (el.parentElement) {
        el.parentElement.className += READ_ONLY_CLASSES
      }

      if (el.childNodes) {
        each(el.childNodes, child => {
          child.className += READ_ONLY_CLASSES
        })
      }
    }

    // add readOnly property to el, parent and child
    if (isInput) {
      el.readOnly = true
    } else {
      if (el.parentElement) {
        el.parentElement.readOnly = true
      }

      if (el.childNodes) {
        each(el.childNodes, child => {
          child.readOnly = true
        })
      }
    }
  }
};

export const injectDisabledStyle = (el) => {
  if (el && el.style) {
    el.style.pointerEvents = 'none';
    el.style.opacity = '0.5'
  }
};

