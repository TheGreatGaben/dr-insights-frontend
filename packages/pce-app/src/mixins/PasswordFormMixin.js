export const PasswordFormMixin = {
  data() {
    return {
      passwordForm: {
        newPassword: '',
        confirmPassword: '',
      },

      rules: {
        password: [],
        confirmPassword: [],
      },
    };
  },
  methods: {
    passwordValidator(rule, value, callback) {
      const newPass = this.passwordForm.newPassword;
      const confirm = this.passwordForm.confirmPassword;
      if (newPass !== confirm) {
        callback(new Error('Passwords do not match'));
      }
      callback();
    },
  },
};
