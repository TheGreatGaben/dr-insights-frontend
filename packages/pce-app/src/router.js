import Vue from 'vue'
import Router from 'vue-router'
import {omit} from 'lodash'
import {sync as syncRouter} from 'vuex-router-sync'

import {isDevelopment, log} from './utils'
import {getToken} from './utils/auth'
import {filterAccessibleRoutes} from './utils/permission'
import {store} from './store'
import {auth} from './stores/auth'

import {authRoutes} from './modules/Auth/routes'
import {profileRoutes} from './modules/Profile/routes'
import {dashboardRoutes} from './modules/Dashboard/routes'
import {projectsRoutes} from './modules/Projects/routes'
import {exampleRoutes} from './modules/Examples/routes'
import {clientsRoutes} from './modules/Clients/routes'
import {contractorsRoutes} from './modules/Contractors/routes'
import {usersRoutes} from './modules/UserManagement/routes'
import {reportsRoutes} from './modules/Reports/routes'
import {pendingInvoicesRoutes} from './modules/PendingInvoices/routes'
import {bulkPaymentRoutes} from './modules/BulkPayment/routes'
import {documentPrintingRoutes} from './modules/DocumentPrinting/routes'
import {accountCodesRoutes} from './modules/AccountCodes/routes'

Vue.use(Router)

/**
 * These routes are dynamically registered into the router
 * @type {RouteConfig[]}
 */
const asyncRoutes = []

export function addAsyncRoutes(routes) {
  if (Array.isArray(routes)) {
    asyncRoutes.push(...routes)
  } else if (typeof routes === 'object') {
    // if it's just 1 route
    asyncRoutes.push(routes)
  } else {
    throw Error('Please pass in valid routes')
  }
}

addAsyncRoutes(dashboardRoutes)
addAsyncRoutes(projectsRoutes)
addAsyncRoutes(contractorsRoutes)
addAsyncRoutes(clientsRoutes)
addAsyncRoutes(usersRoutes)
addAsyncRoutes(pendingInvoicesRoutes)
addAsyncRoutes(bulkPaymentRoutes)
addAsyncRoutes(reportsRoutes)
addAsyncRoutes(profileRoutes)
addAsyncRoutes(documentPrintingRoutes)
addAsyncRoutes(accountCodesRoutes)

if (isDevelopment) {
  addAsyncRoutes(exampleRoutes)
}

/**
 * all routes inclusive of all static routes and async routes
 * @type {RouteConfig[]}
 */
const routes = [
  ...asyncRoutes,
  ...authRoutes,
  {
    path: '/404',
    name: '404',
    component: import(/* webpackChunkName: "404" */ './views/errorPage/404.vue'),
    props: true
  },
  {
    path: '*',
    redirect: '/404'
  }
]

// noinspection JSUnusedGlobalSymbols
export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return {selector: to.hash}
    }
    return {x: 0, y: 0}
  }
})

// sync router in store
syncRouter(store, router)

// const whiteList = constantRouterMap.map(route => route.path) // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  if (getToken()) {
    if (to.path === '/login') {
      log('has token, redirect to home')
      return next({path: '/'})
    } else {
      const isRequested = auth.userDetails.isRequested.get()
      const isLoading = auth.userDetails.isLoading.get()
      if (!isRequested && !isLoading) {
        try {
          await auth.userDetails.fetchData()

          const data = auth.userDetails.data.get()

          /**
           * update permission and role list
           */
          auth.permissions.assign(data.all_permissions)
          auth.roles.assign(data.roles)

          /**
           * filter list of routes that user can access
           * `auth.accessibleRoutes` will be used by components to display navigations
           */
          const filteredRoutes = omitRouteComponents(
            filterAccessibleRoutes(asyncRoutes, auth.permissionNameList())
          )

          auth.isAuthenticated.assign(true)
          auth.accessibleRoutes.assign(filteredRoutes)

          next()
        } catch (e) {
          log('router auth error: ', e)
          log('get user details error, why!!!')

          auth.logout()

          // reset store
          auth.resetAll(true)
          next('/login')
        }
      }

      next()
    }
  } else {
    // no token
    const isAuthPages = authRoutes.map(route => route.name).includes(to.name)
    if (isAuthPages) {
      next()
    } else {
      next({
        path: '/login',
        query: location.search
      })
    }
  }

  return next()
})

router.beforeResolve(async (routeTo, routeFrom, next) => {
  // Create a `beforeResolve` hook, which fires whenever
  // `beforeRouteEnter` and `beforeRouteUpdate` would. This
  // allows us to ensure data is fetched even when params change,
  // but the resolved route does not. We put it in `meta` to
  // indicate that it's a hook we created, rather than part of
  // Vue Router (yet?).
  try {
    // For each matched route...
    for (const route of routeTo.matched) {
      await new Promise((resolve, reject) => {
        // If a `beforeResolve` hook is defined, call it with
        // the same arguments as the `beforeEnter` hook.
        if (route.meta && route.meta.beforeResolve) {
          route.meta.beforeResolve(routeTo, routeFrom, (...args) => {
            // If the user chose to redirect...
            if (args.length) {
              // Complete the redirect.
              next(...args)
              reject(new Error('Redirected'))
            } else {
              resolve()
            }
          })
        } else {
          // Otherwise, continue resolving the route.
          resolve()
        }
      })
    }
    // If a `beforeResolve` hook chose to redirect, just return.
  } catch (error) {
    return
  }

  // If we reach this point, continue resolving the route.
  next()
})

/**
 * To remove component from routes before saving in auth store
 * @param {RouteConfig} route
 */
function omitRouteComponents(route) {
  const omittedRoute = omit(route, ['component'])

  if (omittedRoute.children) {
    omittedRoute.children.map(nestedRoute => omitRouteComponents(nestedRoute))
  }

  return omittedRoute
}

if (isDevelopment) {
  window['router'] = router
  window['routes'] = routes
}
