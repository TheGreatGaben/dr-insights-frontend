import Vue from 'vue'
import Confirm from '../components/DialogConfirm'
import Vuetify from './vuetify';

const defaultProps = {
  title: 'Confirmation',
  icon: 'mdi-alert',
  buttonTrueColor: 'error',
  buttonFalseColor: 'info',
}

let popupIsOpen = false;
/**
 * allow confirmation popup only show one at a time
 * @param {string} message
 * @param {VuetifyConfirmObject} options
 * @return {Promise<boolean | undefined>}
 */
export function confirmOnce(message, options = {}) {
  if (popupIsOpen) {
    return Promise.reject('dialog is open.');
  }

  const container = document.querySelector('[data-app=true]') || document.body
  const ConfirmConstructor = Vue.extend(Object.assign({vuetify: Vuetify}, Confirm))

  // update local state
  popupIsOpen = true;

  return new Promise(resolve => {
    const component = new ConfirmConstructor(Object.assign({}, {
      propsData: {
        ...defaultProps,
        ...options,
        message,
      },
      destroyed: () => {
        container.removeChild(component.$el)

        // update local state on close
        popupIsOpen = false;

        // resolve promise
        resolve(component.value)
      }
    }))
    container.appendChild(component.$mount().$el)
  });
}

export function installConfirmOnce(context) {
  context.prototype.$confirmOnce = confirmOnce.bind(context);
  context.$confirmOnce = confirmOnce.bind(context);
}
