import Vue from 'vue';
import {get} from 'lodash';
import axios from 'axios';
import VueAxios from 'vue-axios';
import {stringify} from 'query-string';
import {getImpersonationToken, getToken} from '../utils/auth';
import {isDevelopment} from '../utils';

const getJwtToken = () => {
  let token = getToken();
  const impersonationToken = getImpersonationToken()

  if (impersonationToken) {
    token = impersonationToken
  }

  return token
}

/**
 * Axios instance
 * @type {AxiosInstance}
 */
const http = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 60000, // request timeout
  maxRedirects: 10, // follow up to 10 HTTP 3xx redirects
  headers: {
    Authorization: `Bearer ${getJwtToken()}`,
  },
});

const jwtInterceptor = async config => {
  const token = getJwtToken();

  config.headers = {
    ...config.headers,
    ...(token ? {Authorization: `Bearer ${token}`} : {}),
  };
  return config;
};

const onAuthErrorRedirect = async (error) => {
  const status = get(error, 'response.status');

  if (status === 418) {
    const query = stringify({
      redirect: window.location.pathname,
    });

    Vue.$router.push(`/login?${query}`);
  } else if (status === 500) {
    // noinspection ES6MissingAwait
    Vue.$toast(
      'Something went wrong. Try refresh the page or contact the admin if the problem persist.',
      {
        color: 'error',
        closeBtn: true,
      }
    );
  }

  return Promise.reject(error);
};
/**
 * make axios auto read `access_token` in localStorage
 */
http.interceptors.request.use(jwtInterceptor);
http.interceptors.response.use(f => f, onAuthErrorRedirect);

Vue.use(VueAxios, http);

if (isDevelopment) {
  window['$http'] = http;
  window['$axios'] = http;
}
