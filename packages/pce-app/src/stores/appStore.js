import {BaseStore, Store, State} from '@nixel/stores';
import {isDevelopment} from '../utils';

@Store('app')
class AppStore extends BaseStore {
  @State
  isOffline = false;

  @State
  sidebar = {
    opened: localStorage.getItem('sidebarStatus'),
    withoutAnimation: false
  };

  @State
  window = {
    width: document.body.clientWidth,
    height: document.body.clientHeight,
  };

  @State
  sideNavIsOpen = false
}

export const appStore = new AppStore();

if (isDevelopment) {
  window.app = appStore
}
