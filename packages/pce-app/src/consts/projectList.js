import {isEmpty} from 'lodash';

export const PROJECT_LIST_COLUMNS = 'PROJECT_LIST_COLUMNS';
export const PROJECT_LIST_COLUMN = 'PROJECT_LIST_COLUMN';
export const DASHBOARD_PROJECT_LIST_COLUMNS = 'DASHBOARD_PROJECT_LIST_COLUMNS';

function getStoredColumns(key) {
  const columns = localStorage.getItem(key);

  if (!isEmpty(columns)) {
    return JSON.parse(localStorage.getItem(key));
  }

  return undefined;
}

function getStoredColumn(key) {
  const column = localStorage.getItem(key);
  return column;
}
// Project list
export const getProjectListSelectedColumns = () => {
  return getStoredColumns(PROJECT_LIST_COLUMNS);
};
export const setProjectListSelectedColumns = columns => {
  return localStorage.setItem(PROJECT_LIST_COLUMNS, columns);
};

// Project list for app
export const getProjectListSelectedColumn = () => {
  return getStoredColumn(PROJECT_LIST_COLUMN);
};
export const setProjectListSelectedColumn = column => {
  return localStorage.setItem(PROJECT_LIST_COLUMN, JSON.parse(column));
};

// Dashboard project list
export const getDashboardProjectListSelectedColumns = () => {
  return getStoredColumns(DASHBOARD_PROJECT_LIST_COLUMNS);
};
export const setDashboardProjectListSelectedColumns = columns => {
  return localStorage.setItem(DASHBOARD_PROJECT_LIST_COLUMNS, columns);
};
