export const ROLES = {
  SUPER_ADMIN: 1,
  ADMIN: 2,
  BUSINESS_OWNER: 3,
  PROJECT_MANAGER: 4,
};

export const ADMINS = [ROLES.SUPER_ADMIN, ROLES.BUSINESS_OWNER, ROLES.ADMIN];
