import {isString, isArray} from 'lodash';

/**
 * @param {Array} permissionRoles
 * @param rolesAndPermissions
 * @returns {Boolean}
 */
export default function checkPermission(permissionRoles, rolesAndPermissions) {
  if (!permissionRoles) {
    throw new Error(`permissionRoles not defined`);
  }

  if (isString(permissionRoles)) {
    // if just string
    return checkPermissionString(rolesAndPermissions, permissionRoles);
  } else if (isArray(permissionRoles) && permissionRoles.length > 0) {
    // loop permissionRoles
    return permissionRoles.some(permissionRole => {
      return checkPermissionString(rolesAndPermissions, permissionRole);
    });
  } else {
    return false;
  }
}

/**
 * check roles and permissionRole with wildcard
 * @param roles: role list
 * @param permissionRole
 *        - `permissionA.edit` match `permissionA.view`, does not match [`permissionA.edit`, `permissionA.delete`]
 *        - `permissionA.*`    match [`permissionA.view`, `permissionA.edit`, `permissionA.delete`]
 *        - [Caveat: currently does not support (*) wildcard in the start and mid string]
 */
export function checkPermissionString(roles, permissionRole) {
  const hasWildCard = isString(permissionRole) && permissionRole.includes('.*');

  if (hasWildCard) {
    const permissionRoleString = permissionRole.replace('.*', '');

    // make sure role is string, due to currently role could be role's id at the moment
    return roles.some(role => `${role}`.includes(permissionRoleString));
  } else {
    return roles.includes(permissionRole);
  }
}

/**
 * filter routes that are only accessible by the user
 * @param routes
 * @param {String[]} rolesAndPermissions
 * @return {*}
 */
export function filterAccessibleRoutes(routes, rolesAndPermissions) {
  return routes.filter(route => {
    if (route.meta && route.meta.permissions) {
      return checkPermission(route.meta.permissions, rolesAndPermissions);
    } else if (!route.component && !route.children) {
      // if route defined is just to redirect, filter them out
      return false;
    }

    // filter children routes
    if (route.children && route.children.length) {
      route.children = filterAccessibleRoutes(route.children, rolesAndPermissions);
    }

    return true;
  });
}
