import {reduce, get} from 'lodash';

/**
 * check list if there's any value
 * @param {any[]} list
 * @param {string} path
 * @param {string|number} value
 * @return {boolean}
 */
export function hasValue(list, path, value) {
  return reduce(
    list,
    (result, item) => {
      if (get(item, path) === value) {
        return true;
      }
      return result;
    },
    false
  );
}
