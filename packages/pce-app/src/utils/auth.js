const tokenKey = 'access_token';
const refreshTokenKey = 'refresh_token';
const impersonationTokenKey = 'impersonation_token';
const impersonationTokenIDKey = 'impersonation_token_id';

export function getToken() {
  return localStorage.getItem(tokenKey);
}

export function hasToken() {
  return !!getToken();
}

export function getRefreshToken() {
  return localStorage.getItem(refreshTokenKey);
}

export function setToken(token, refreshToken) {
  localStorage.setItem(tokenKey, token);
  localStorage.setItem(refreshTokenKey, refreshToken);
}

export function setImpersonationSession(token, tokenID) {
  sessionStorage.setItem(impersonationTokenKey, token);
  sessionStorage.setItem(impersonationTokenIDKey, tokenID);
}

export function getImpersonationToken() {
  return sessionStorage.getItem(impersonationTokenKey);
}

export function getImpersonationTokenID() {
  return sessionStorage.getItem(impersonationTokenIDKey);
}

export function removeImpersonationToken() {
  sessionStorage.removeItem(impersonationTokenKey);
  sessionStorage.removeItem(impersonationTokenIDKey);
}

export function removeToken() {
  localStorage.removeItem(tokenKey);
  localStorage.removeItem(refreshTokenKey);
}
