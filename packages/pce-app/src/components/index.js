import Vue from 'vue';

import MaterialCard from './material/Card';
import MaterialChartCard from './material/ChartCard';
import MaterialNotification from './material/Notification';
import MaterialStatsCard from './material/StatsCard';

import HelperOffset from './helper/Offset';

// Helper components from creative tim
Vue.component('HelperOffset', HelperOffset);

// Material components from creative tim
Vue.component('MaterialCard', MaterialCard);
Vue.component('MaterialChartCard', MaterialChartCard);
Vue.component('MaterialNotification', MaterialNotification);
Vue.component('MaterialStatsCard', MaterialStatsCard);
