interface ActionMenuNavigationItem {
  title: string;
  to?: string;
  color?: string;
  click?: (...args) => void;
}
