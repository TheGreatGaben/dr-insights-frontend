/**
 *
 * @type {RouterOptions[]}
 */
import AccountCodes from './AccountCodes';

export const ACCOUNT_CODE_LIST_PAGE_PATH = '/accountCodes';
export const accountCodeDetailsPagePath = (uuid) => `/accountCodes/${uuid}`;

export const accountCodesRoutes = [
  {
    path: ACCOUNT_CODE_LIST_PAGE_PATH,
    name: 'AccountCodes',
    meta: {
      authRequired: true,
      permissions: ['finance_account_code.*'], // hide accountCodes page if user don't have permission to use
      title: 'AccountCodes',
    },
    component: AccountCodes,
    children: [
      {
        path: ACCOUNT_CODE_LIST_PAGE_PATH,
        name: 'Account Code List',
        meta: {
          authRequired: true,
          title: 'Account Code List',
          icon: 'mdi-decagram-outline'
        },
        component: () => import(/* webpackChunkName: "accountCodeList" */ './AccountCodeList.vue'),
      },
      {
        path: ':uuid',
        name: 'Account Code Details',
        meta: {
          authRequired: true,
          title: 'Account Code Details',
          icon: 'mdi-decagram'
        },
        hidden: true,
        component: () => import(/* webpackChunkName: "accountCodeList" */ './AccountCodeList.vue'),
      },
    ]
  }
];
