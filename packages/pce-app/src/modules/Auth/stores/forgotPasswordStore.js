import {BaseStore} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {app} from '@/main';
import {forgotPasswordApi} from '@/apis';

@Store('forgotPassword')
class ForgotPasswordStore extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        email: {
          value: '',
          label: 'Email',
          placeholder: 'Please fill in your email here.',
          type: 'email',
          rules: 'required|email',
          validateOnBlur: true,
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
      hooks: {
        onSubmitFailed(formStore, response) {
          apiErrorNotification(response)
        },
        onSubmitSuccess() {
          app.$toast('Password has been updated.', {
            color: 'success',
            closeBtn: true,
          })
        },
      }
    }),
    new ApiStore(forgotPasswordApi)
  ]
}

export const forgotPasswordStore = new ForgotPasswordStore();
