import {
  BaseStore,
  FormStore,
  ApiStore,
  Store,
} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {loginApi} from '../apis';

@Store('login')
class LoginStore extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        username: {
          value: '',
          label: 'Email',
          placeholder: 'Please fill in your email here.',
          type: 'email',
          rules: 'required|email',
          validateOnBlur: true,
        },
        password: {
          value: '',
          label: 'Password',
          placeholder: 'Set your new password here.',
          type: 'password',
          rules: 'required',
          validateOnBlur: true,
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
      hooks: {
        onSubmitFailed(formStore, response) {
          apiErrorNotification(response)
        },
        onValidationFailed(formData, fieldName) {},
        onValidationSuccess(formData, fieldName) {},

      }
    }),
    new ApiStore(loginApi)
  ]
}

export const loginStore = new LoginStore();
