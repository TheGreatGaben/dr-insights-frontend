import {auth} from '@/stores/auth';
import {appStore} from '@/stores/appStore';
import {lazyLoadView} from '../../utils/router';
import {parse} from 'query-string';

export const authRoutes = [
  {
    path: '/login',
    name: 'login',
    component: () => lazyLoadView(import('./Login.vue')),
    meta: {
      hideToolBar: true,
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (auth.isAuthenticated.get()) {
          // Redirect to the home page instead
          next('/');
        } else {
          // Continue to the login page
          next();
        }
      },
    },
  },
  {
    path: '/forgot',
    name: 'Forgot Password',
    component: () => lazyLoadView(import('./ForgotPassword.vue')),
    meta: {
      hideToolBar: true,
    }
  },
  {
    path: '/password/reset/:email/:token',
    name: 'Reset Password',
    component: () => lazyLoadView(import('./ResetPassword.vue')),
    hidden: true,
    meta: {
      hideToolBar: true,
    }
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {
      authRequired: true,
      hideToolBar: true,
      beforeResolve(routeTo, routeFrom, next) {
        appStore.sideNavIsOpen.assign(false);
        auth.logout();

        const query = {
          ...parse(location.search),
          redirect: routeFrom.fullPath,
        };

        next({
          path: '/login',
          query,
        });
      },
    },
  },
];
