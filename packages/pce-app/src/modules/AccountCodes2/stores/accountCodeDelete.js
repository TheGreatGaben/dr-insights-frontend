import Vue from 'vue'
import {apiErrorNotification} from '@/utils/request';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';

const deleteAccountCodeApi = (params) => Vue.axios.delete(`/api/v2/finance_account_codes/${params.uuid}`);

@Store('accountCodeDelete')
class AccountCodeDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteAccountCodeApi, []),
  ];

  @Action
  async deleteAccountCode(accountCode, successCallback) {
    try {
      accountCodeDelete.fetchData({
        uuid: accountCode.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${accountCode.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

export const accountCodeDelete = new AccountCodeDelete();

