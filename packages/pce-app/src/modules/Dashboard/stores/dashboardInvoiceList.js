import Vue from 'vue'
import {aggregator} from '@/utils/math';
import {ApiStore, BaseStore, Getter, Store, TableStore} from '@nixel/stores';

export const dashboardInvoicesApi = (params) => Vue.axios.get('api/v2/invoices', {
  params,
});

@Store('invoiceList')
class InvoiceList extends BaseStore {
  static mixins = [
    new ApiStore(dashboardInvoicesApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'No.',
          type: 'index',
          align: 'left',
          width: '80px',
        },
        {
          text: 'Project',
          value: 'project_name',
          width: '120px',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Client',
          value: 'client_name',
          width: '120px',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Cert No.',
          value: 'cert_no',
          align: 'left',
          width: '60px',
          sortable: true,
        },
        {
          text: 'Invoice Date',
          value: 'invoice_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Due Date',
          value: 'due_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ];

  constructor(additionalTableColumns) {
    super();

    this.additionalTableColumns = additionalTableColumns;
  }

  storeHooks = {
    onStoreRegistered() {
      if (this.additionalTableColumns) {
        const columns = [
          ...this.tableColumns.default,
          ...this.additionalTableColumns,
        ];

        const newSelectedColumns = columns.map(column => column.value);

        this.tableColumns.default = columns;
        this.tableColumns.assign(columns);

        this.selectedColumns.default = newSelectedColumns;
        this.selectedColumns.assign(newSelectedColumns);
      }
    },
  };

  @Getter
  totalInvoiced() {
    return aggregator(this.data.get(), 'total_invoiced')
  }

  @Getter
  totalReceivable() {
    const items = this.data.get().filter(item => parseFloat(item.write_off_amt) === 0);
    return aggregator(items, 'total_receivable_in_period')
  }

  @Getter
  totalWriteOff() {
    return aggregator(this.data.get(), 'write_off_amt')
  }

  @Getter
  totalReceived() {
    return aggregator(this.data.get(), 'total_received_in_period')
  }

  @Getter
  totalAdvanceReceived() {
    return aggregator(this.data.get(), 'total_advance_received_in_period')
  }
}

export {InvoiceList};
