import Vue from 'vue'
import {aggregator} from '@/utils/math';
import {ApiStore, BaseStore, Getter, Store, TableStore} from '@nixel/stores';

export const dashboardExpensesApi = (params) => Vue.axios.get('api/v2/expenses', {
  params,
});

@Store('expensesList')
class ExpensesList extends BaseStore {
  static mixins = [
    new ApiStore(dashboardExpensesApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'No.',
          type: 'index',
          align: 'left',
          width: '80px',
        },
        {
          text: 'Project',
          value: 'project_name',
          width: '120px',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Contractor',
          value: 'contractor_name',
          width: '120px',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Date Received',
          value: 'date_received',
          type: 'date',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Invoice Date',
          value: 'invoice_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Due Date',
          value: 'due_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Ref No.',
          value: 'ref_no',
          align: 'left',
          sortable: true,
        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ];

  constructor(additionalTableColumns) {
    super();

    this.additionalTableColumns = additionalTableColumns;
  }

  storeHooks = {
    onStoreRegistered() {
      if (this.additionalTableColumns) {
        const columns = [
          ...this.tableColumns.default,
          ...this.additionalTableColumns,
        ];

        const newSelectedColumns = columns.map(column => column.value);

        this.tableColumns.default = columns;
        this.tableColumns.assign(columns);

        this.selectedColumns.default = newSelectedColumns;
        this.selectedColumns.assign(newSelectedColumns);
      }
    },
  };

  @Getter
  totalExpenses() {
    return aggregator(this.data.get(), 'total_expenses')
  }

  @Getter
  totalPaid() {
    return aggregator(this.data.get(), 'total_paid_in_period')
  }

  @Getter
  totalPayable() {
    const items = this.data.get().filter(item => parseFloat(item.write_off_amt) === 0);
    return aggregator(items, 'total_payable_in_period')
  }

  @Getter
  totalWriteOff() {
    return aggregator(this.data.get(), 'write_off_amt')
  }

  @Getter
  totalOverpaid() {
    return aggregator(this.data.get(), 'total_overpaid_in_period')
  }
}

export {ExpensesList};
