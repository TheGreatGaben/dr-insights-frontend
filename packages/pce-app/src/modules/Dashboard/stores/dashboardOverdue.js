import {addDays} from 'date-fns';
import Vue from 'vue';
import {ApiStore, BaseStore, Getter, State} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Nested} from '@nixel/stores';
import {InvoiceList} from './dashboardInvoiceList';
import {ExpensesList} from './dashboardExpensesList';
import {monthAndDay} from '@nixel/base/filters/datetime';

export const overdueSummaryApi = (params) => Vue.axios.get('api/v2/dashboard/overdue', {
  params,
});

const today = new Date();
const overdueDates = {
  today,
  after30: addDays(today, 30),
  after31: addDays(today, 31),
  after60: addDays(today, 60),
};

@Store('dashboardOverdue')
class DashboardOverdue extends BaseStore {
  static mixins = [
    new ApiStore(overdueSummaryApi, {
      invoice: {
        overdue_amt: 0,
        overdue_amt_30: 0,
        overdue_amt_60: 0,
        overdue_amt_above: 0,
      },
      expense: {
        overdue_amt: 0,
        overdue_amt_30: 0,
        overdue_amt_60: 0,
        overdue_amt_above: 0,
      },
    }),
  ];

  @State
  filterList = {
    overdue_amt_30: {
      label: '0 to 30 days',
      date: `${monthAndDay(overdueDates.today)} to ${monthAndDay(overdueDates.after30)}`,
      value: {
        [`overdue[start]`]: 1,
        [`overdue[end]`]: 30,
      },
    },
    overdue_amt_60: {
      label: '31 to 60 days',
      date: `${monthAndDay(overdueDates.after31)} to ${monthAndDay(overdueDates.after60)}`,
      value: {
        [`overdue[start]`]: 31,
        [`overdue[end]`]: 60,
      },
    },
    overdue_amt_above: {
      label: 'Above 60 days',
      date: `${monthAndDay(overdueDates.after60)} Onwards`,
      value: {
        [`overdue[start]`]: 60,
        [`overdue[end]`]: undefined,
      },
    },
  };

  @State
  overduePayload = {
    status: 'overdue',
    listType: 'invoices',
    overdueType: undefined,
  };

  @Nested
  invoiceList = new InvoiceList([
    {
      text: 'Balance',
      value: 'total_receivable',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
    {
      text: 'Overdue (days)',
      value: 'overdue',
      sortable: true,
      align: 'right',
    }
  ]);

  @Nested
  expensesList = new ExpensesList([
    {
      text: 'Balance',
      value: 'balance',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
    {
      text: 'Overdue (days)',
      value: 'overdue',
      sortable: true,
      align: 'right',
    }
  ]);

  @Getter
  selectedTableStore() {
    const listType = this.overduePayload.get('listType');

    if (listType === 'invoices') {
      return this.invoiceList;
    } else {
      return this.expensesList;
    }
  }
}

export const dashboardOverdue = new DashboardOverdue();
