import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {State} from '@nixel/stores';
import {defaultFilterQuery} from '@nixel/stores/src/consts/stores';
import {dashboardProjectListColumns} from '../resources/dashboardProjectList';
import {getDashboardProjectListSelectedColumns} from '@/consts/projectList';

const fetchDashboardProjectList = (params) => Vue.axios.get('/api/v2/dashboard/projects', {
  params
});

@Store('dashboardProjectList')
class DashboardProjectList extends BaseStore {
  static mixins = [
    new ApiStore(fetchDashboardProjectList, []),
    new TableStore({
      tableColumns: dashboardProjectListColumns,
      defaultSelectedColumns: getDashboardProjectListSelectedColumns,
      options: {
        serverSitePagination: false,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ];

  @State
  query = defaultFilterQuery;
}

export const dashboardProjectList = new DashboardProjectList();
