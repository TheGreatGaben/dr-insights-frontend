import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {Nested} from '@nixel/stores';
import {paymentTableColumns} from '@/consts/paymentList';

export const dashboardPaymentsApi = (params) => Vue.axios.get('api/v2/payments', {
  params,
});

@Store('dashboardSummaryPayments')
class DashboardSummaryPayments extends BaseStore {
  static mixins = [
    new ApiStore(dashboardPaymentsApi, []),
    new TableStore({
      tableColumns: paymentTableColumns,
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      },
    })
  ];

  // @State
  // query = defaultFilterQuery;
}

@Store('dashboardPayments')
class DashboardPayments extends BaseStore {
  @Nested
  thisYearPayments = new DashboardSummaryPayments();

  @Nested
  thisMonthPayments = new DashboardSummaryPayments();

  @Nested
  nextMonthPayments = new DashboardSummaryPayments();
}

export const dashboardPayments = new DashboardPayments();
