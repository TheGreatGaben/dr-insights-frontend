import {
  cashflowMarginFormula,
  currentMarginFormula,
  progressFormula,
  profitMarginFormula,
  workCertifiedMarginFormula,
  projectedMarginFormula
} from '@/consts/formulaLabels';

export const dashboardProjectListColumns = [
  {
    type: 'expand',
    width: '60px',
  },
  {
    text: 'No.',
    type: 'index',
    align: 'left',
    width: '80px',
  },
  {
    text: 'Project',
    value: 'name',
    width: '100px',
    align: 'left',
    sortable: true,
    type: 'singleLine',
  },
  {
    text: 'Contract Sum + VO',
    value: 'contract_sum_and_vo',
    align: 'right',
    sortable: true,
    tag: 'p',
    type: 'currency',
  },

  // total cashflow won't affected by payment dates
  {
    text: 'Total Cashflow',
    value: 'overall_cashflow_nett',
    align: 'right',
    sortable: true,
    tag: 'p',
    type: 'currency',
  },
  // cashflow
  {
    text: 'Net Cash Flow',
    value: 'cashflow_nett_in_period',
    align: 'right',
    sortable: true,
    tag: 'p',
    type: 'currency',
  },
  {
    text: 'Total Received',
    value: 'total_received_in_period',
    sortable: true,
    align: 'right',
    tag: 'p',
    type: 'currency',
  },
  {
    text: 'Total Paid',
    value: 'total_paid_in_period',
    sortable: true,
    align: 'right',
    tag: 'p',
    type: 'currency',
  },

  // stats
  {
    text: 'Duration Variance (%)',
    value: 'progress_diff',
    align: 'right',
    sortable: true,
    tooltip: {
      message: progressFormula.label,
      allowOverflow: false,
      // contentClass: 'text-nowrap',
    },
    columnTooltip: {
      message(prop) {
        return progressFormula.value({
          durationLA: prop.item.duration_la,
          currentDuration: prop.item.current_duration,
        })
      },
    },
  },
  {
    text: 'Current margin (%)',
    value: 'current_margin',
    align: 'right',
    sortable: true,
    tooltip: {
      message: currentMarginFormula.label,
      allowOverflow: true,
      // contentClass: 'text-nowrap',
    },
    columnTooltip: {
      message(prop) {
        // noinspection JSUnresolvedVariable
        return currentMarginFormula.value({
          invoiced: prop.item.overall_invoiced,
          contractSum: prop.item.contract_sum,
          variationOrder: prop.item.variation_order,
          retention: prop.item.retention,
          budget: prop.item.budget
        })
      },
    },
    // sortable: true,
  },
  {
    text: 'Cashflow Margin (%)',
    value: 'cashflow_margin_in_period',
    align: 'right',
    sortable: true,
    tooltip: {
      message: cashflowMarginFormula.label,
      allowOverflow: true,
      // contentClass: 'text-nowrap',
    },
    columnTooltip: {
      message(prop) {
        return cashflowMarginFormula.value({
          collected: prop.item.total_received_in_period,
          paid: prop.item.total_paid_in_period,
        })
      },
    },
    // sortable: true,
  },
  {
    text: 'Profit Margin (%)',
    value: 'profit_margin',
    align: 'right',
    sortable: true,
    tooltip: {
      message: profitMarginFormula.label,
      allowOverflow: true,
      // contentClass: 'text-nowrap',
    },
    columnTooltip: {
      message(prop) {
        // noinspection JSUnresolvedVariable
        return profitMarginFormula.value({
          invoiced: prop.item.overall_invoiced,
          expenses: prop.item.overall_expenses,
        })
      },
    },
    // sortable: true,
  },
  {
    text: 'Projected Margin (%)',
    value: 'projected_margin',
    align: 'right',
    sortable: true,
    tooltip: {
      message: projectedMarginFormula.label,
      allowOverflow: true,
    },
  },
  {
    text: 'Work Certified Margin (%)',
    value: 'work_certified_margin',
    align: 'right',
    sortable: true,
    tooltip: {
      message: workCertifiedMarginFormula.label,
      allowOverflow: true,
      // contentClass: 'text-nowrap',
    },
    columnTooltip: {
      message(prop) {
        // noinspection JSUnresolvedVariable
        return workCertifiedMarginFormula.value({
          invoiced: prop.item.overall_invoiced,
          contractSum: prop.item.contract_sum,
          variationOrder: prop.item.variation_order,
        })
      },
    },
    // sortable: true,
  },
];

export const dashboardProjectAllColumns = [
  'name',
  'contract_sum_and_vo',
  'overall_cashflow_nett',
  'cashflow_nett_in_period',
  'total_received_in_period',
  'total_paid_in_period',
  'duration_la',
  'current_duration',
  'current_margin',
  'cashflow_margin_in_period',
  'profit_margin',
  'work_certified_margin',
  'progress_diff'
];

export const dashboardProjectDurationColumns = [
  'name',
  'duration_la',
  'current_duration',
  'current_margin',
  'cashflow_margin',
  'profit_margin',
  'work_certified_margin',
  'progress_diff'
];

export const dashboardProjectCashflowColumns = [
  'name',
  'contract_sum_and_vo',
  'overall_cashflow_nett',
  'cashflow_nett_in_period',
  'total_received_in_period',
  'total_paid_in_period',
];
