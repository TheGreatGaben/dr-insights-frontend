/**
 *
 * @type {TableColumn[]}
 */
export const clientTableHeaders = [
  {
    align: 'center',
    type: 'expand',
    value: '_expand',
    width: '10px',
  },
  {
    text: 'No.',
    type: 'index',
    value: '_index',
    align: 'left',
    width: '80px',
  },
  {
    text: 'Client\'s Name',
    sortable: true,
    value: 'name',
    align: 'left',
  },
  {
    text: 'Person In Charge',
    sortable: true,
    value: 'person_in_charge',
    align: 'left',
    width: '25%',
  },
  {
    text: 'End Customer',
    sortable: true,
    value: 'end_customer',
    align: 'left',
    width: '25%',
  },
];
