import {FormStore} from '@nixel/stores';

export class ClientForm extends FormStore {
  constructor({fields, ...args}) {
    super({
      fields: {
        name: {
          value: '',
          label: 'Client\'s Name',
          placeholder: 'Client\'s name',
          rules: 'required|max:200',
        },
        person_in_charge: {
          value: '',
          label: 'Person In Charge.',
        },
        end_customer: {
          value: '',
          label: 'End Customer',
        },
        tel_no: {
          value: '',
          label: 'Telephone No.',
        },
        fax_no: {
          value: '',
          label: 'Fax No.',
        },
        address_1: {
          value: '',
          label: 'Address 1',
        },
        address_2: {
          value: '',
          label: 'Address 2',
        },
        city: {
          value: '',
          label: 'City',
        },
        postcode: {
          value: '',
          label: 'Postcode',
        },
        state: {
          value: '',
          label: 'State',
        },
        country: {
          value: 'MY',
          label: 'Country',
          rules: 'required',
        },
        ...fields,
      },
      ...args,
    });
  }

}
