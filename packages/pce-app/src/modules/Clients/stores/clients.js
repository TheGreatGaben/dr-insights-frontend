import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Getter} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {clientTableHeaders} from '../resources/clientList';

const fetchClientListApi = (params) => Vue.axios.get('/api/v2/clients', {
  params
});

@Store('clientTableList')
class ClientTableList extends BaseStore {
  static mixins = [
    new ApiStore(fetchClientListApi, []),
    new TableStore({
      tableColumns: clientTableHeaders,
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

@Store('clients')
class Clients extends BaseStore {
  static mixins = [
    new ApiStore(fetchClientListApi, [])
  ]

  @Getter
  clientNames() {
    const clients = this.data.get()
    return clients.map(client => client.name);
  }
}

export const clients = new Clients();

export const clientTableList = new ClientTableList();

