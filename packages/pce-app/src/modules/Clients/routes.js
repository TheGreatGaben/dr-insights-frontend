/**
 *
 * @type {RouterOptions[]}
 */
import Clients from './Clients';

export const clientsRoutes = [
  {
    path: '/clients',
    name: 'Clients',
    meta: {
      authRequired: true,
      permissions: ['client.*'], // hide clients page if user don't have permission to use
      title: 'Clients',
    },
    redirect: '/clients/list',
    component: Clients,
    children: [
      {
        path: 'list',
        name: 'Client List',
        meta: {
          authRequired: true,
          title: 'Client List',
          icon: 'mdi-account-supervisor'
        },
        component: () => import(/* webpackChunkName: "clientList" */ './ClientList.vue'),
      },
      {
        path: '*',
        redirect: '/clients/list',
      }
    ]
  }
];
