import Vue from 'vue'
import {apiErrorNotification} from '@/utils/request';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';

const deleteContractorApi = (params) => Vue.axios.delete(`/api/v2/contractors/${params.uuid}`);

@Store('contractorDelete')
class ContractorDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteContractorApi, []),
  ];

  @Action
  async deleteContractor(contractor, successCallback) {
    try {
      contractorDelete.fetchData({
        uuid: contractor.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${contractor.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

export const contractorDelete = new ContractorDelete();

