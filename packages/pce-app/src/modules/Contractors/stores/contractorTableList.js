import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {contractorTableHeaders} from '../resources/contractorList';

const fetchContractorListApi = (params) => Vue.axios.get('/api/v2/contractors', {
  params
});

@Store('contractorTableList')
class ContractorTableList extends BaseStore {
  static mixins = [
    new ApiStore(fetchContractorListApi, []),
    new TableStore({
      tableColumns: contractorTableHeaders,
      options: {
        selectableRows: true,
      }
    })
  ]
}

export const contractorTableList = new ContractorTableList();

