import Vue from 'vue';
import {BaseStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Getter, Nested, Store} from '@nixel/stores';
import {ContractorForm} from './contractorForm';
import {apiErrorNotification} from '@/utils/request';

const createContractorDetailsApi = (params) => Vue.axios.post('/api/v2/contractors', params);

const updateContractorDetailsApi = (params) => Vue.axios.put(`/api/v2/contractors/${params.uuid}`, params);

const fetchContractorDetailsApi = (params) => Vue.axios.get(`/api/v2/contractors/${params.uuid}`, {
  params
});

@Store('createContractorForm')
class CreateContractorForm extends BaseStore {
  static mixins = [
    new ApiStore(createContractorDetailsApi),
    new ContractorForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully created contractor.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}
@Store('updateContractorForm')
class UpdateContractorForm extends BaseStore {
  static mixins = [
    new ApiStore(updateContractorDetailsApi),
    new ContractorForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update contractor.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}

@Store('contractorDetails')
class ContractorDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchContractorDetailsApi, {}),
  ];

  @Nested
  createContractorForm = new CreateContractorForm();

  @Nested
  updateContractorForm = new UpdateContractorForm();

  @Getter
  name(state) {
    return state.data && state.data.name
  }

}

export const contractorDetails = new ContractorDetails();

