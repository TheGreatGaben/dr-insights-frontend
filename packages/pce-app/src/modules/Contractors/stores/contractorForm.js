import {FormStore} from '@nixel/stores';

export class ContractorForm extends FormStore {
  constructor({fields, ...args}) {
    super({
      fields: {
        name: {
          value: '',
          label: 'Contractor\'s Name',
          placeholder: 'Contractor\'s name',
          rules: 'required|max:200',
        },
        type: {
          value: '',
          label: 'Contractor Type',
          rules: 'integer|min:1'
        },
        tel_no: {
          value: '',
          label: 'Telephone No.',
        },
        fax_no: {
          value: '',
          label: 'Fax No.',
        },
        address_1: {
          value: '',
          label: 'Address 1',
        },
        address_2: {
          value: '',
          label: 'Address 2',
        },
        city: {
          value: '',
          label: 'City',
        },
        postcode: {
          value: '',
          label: 'Postcode',
        },
        state: {
          value: '',
          label: 'State',
        },
        country: {
          value: 'MY',
          label: 'Country',
          rules: 'required',
        },
        ...fields,
      },
      ...args,
    });
  }

}
