import Vue from 'vue';

export const fetchContractorMYOBApi = (params) => Vue.axios.get('/api/v2/contractors/myob', {
  params
});
