import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchPaymentReportListApi = (params) => Vue.axios.get('/api/v2/payments', {params});

@Store('paymentReports')
class PaymentReports extends BaseStore {
  static mixins = [
    new ApiStore(fetchPaymentReportListApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'No.',
          type: 'index',
          align: 'left',
          width: '70px',
        },
        {
          text: 'Payment Date',
          value: 'payment_date',
          type: 'date',
          width: '140px',
          align: 'right',
          sortable: true,
          class: 'pl-0',
        },
        {
          text: 'Project Name',
          value: 'project_name',
          width: '12%',
          type: 'singleLine',
          align: 'left',
          sortable: true,
        },
        {
          text: 'Client / Supplier',
          value: 'client_or_contractor_name',
          sortable: true,
          formatter(payment) {
            if (payment.cash_flow === 'IN') {
              return payment.client_name;
            } else if (payment.cash_flow === 'OUT') {
              return payment.supplier_name;
            } else {
              return '-'
            }
          },
          width: '20%',
          align: 'left',
          type: 'singleLine'
        },
        {
          text: 'Cert No. / Ref No.',
          value: 'cert_or_ref_no',
          sortable: true,
          formatter(payment) {
            if (payment.cash_flow === 'IN') {
              return payment.cert_no;
            } else if (payment.cash_flow === 'OUT') {
              return payment.cheque_no;
            } else {
              return '-'
            }
          },
          align: 'left',
          width: '120px',
          type: 'singleLine',
        },
        {
          text: 'Credit',
          value: 'credit',
          type: 'currency',
          align: 'right',
          tag: 'p',
          class: 'success--text',
          sortable: true,
          formatter(payment) {
            if (payment.cash_flow === 'IN') {
              return payment.payment_amt;
            } else {
              return '-'
            }
          }
        },
        {
          text: 'Debit',
          value: 'debit',
          type: 'currency',
          align: 'right',
          tag: 'p',
          class: 'error--text',
          sortable: true,
          formatter(payment) {
            if (payment.cash_flow === 'OUT') {
              return payment.payment_amt;
            } else {
              return '-'
            }
          }

        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

export const paymentReports = new PaymentReports();

