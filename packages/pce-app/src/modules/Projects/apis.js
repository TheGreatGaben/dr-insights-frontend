import Vue from 'vue';
import {ROLES} from '@/consts/roles'

export function createProject(payload) {
  return Vue.axios.post('/api/v2/projects', payload)
}

export function updateProject(payload) {
  return Vue.axios.put(`/api/v2/projects/${payload.uuid}`, payload)
}

export function deleteProjectApi(uuid) {
  return Vue.axios.delete(`/api/v2/projects/${uuid}`)
}

export function getEndCustomers() {
  return Vue.axios.get('/api/v2/end_customers')
}

export function getProjectManagers() {
  const pmRole = ROLES.PROJECT_MANAGER;
  return Vue.axios.get(`/api/v2/users/role/${pmRole}`)
}

export function getPrevCertifiedApi(uuid) {
  return Vue.axios.get(`/api/v2/projects/${uuid}/invoices/prev_certified`)
}

export const fetchProjectListApi = (params) => Vue.axios.get('/api/v2/projects', {
  params
});

export const fetchProjectDetailsApi = (params) => Vue.axios.get(`/api/v2/projects/${params.uuid}`, {
  params
});

// invoices

export const createProjectInvoiceApi = (payload) => Vue.axios.post('/api/v2/invoices', payload);

export const fetchProjectInvoicesApi = (params) => Vue.axios.get(`/api/v2/projects/${params.uuid}/invoices`, {
  params
});

export const updateProjectInvoiceApi = (payload) => Vue.axios.put(`/api/v2/invoices/${payload.uuid}`, payload);
export const deleteProjectInvoiceApi = (payload) => Vue.axios.delete(`/api/v2/invoices/${payload.uuid}`);

export const createInvoicePaymentApi = (payload) => Vue.axios.post('/api/v2/payments', payload);
export const updateInvoicePaymentApi = (payload) => Vue.axios.put(`/api/v2/payments/${payload.uuid}`, payload);

// export const deleteInvoicePaymentApi = (params) => Vue.axios.delete(`/api/payments/${params.uuid}`)
export const deleteInvoicePaymentApi = (params) => Vue.axios.delete(`/api/v2/payments/${params.uuid}`);

// contractors
export const fetchContractorsApi = (params) => Vue.axios.get(`/api/v2/contractors`, {
  params
});

export const fetchProjectContractorsApi = (params) => Vue.axios.get(`/api/v2/projects/${params.uuid}/contractors`, {
  params
});

export const createProjectContractorApi = (payload) => Vue.axios.post('/api/v2/contractors', payload);

export const fetchProjectContractorDetailsApi = (params) => Vue.axios.get(`/api/v2/contractors/${params.uuid}`);

export const updateProjectContractorApi = (payload) => Vue.axios.put(`/api/v2/contractors/${payload.uuid}`, payload);

// Special case as we only want to detach contractor from the project only and not actually delete it
export const deleteProjectContractorsApi = (params) => Vue.axios.delete(`/api/v2/contractors/${params.uuid}`, {params});

// these comments are from backend routes/api.php. for reference only.
// Route::get('pos', 'PurchaseOrderController@getAllPO');
// Route::post('pos', 'PurchaseOrderController@store');
export const createPurchaseOrderApi = (payload) => Vue.axios.post('/api/v2/purchaseOrders', payload);
export const fetchPurchaseOrderApi = (params) => Vue.axios.get('/api/v2/purchaseOrders', {params});
export const updatePurchaseOrderApi = (payload) => Vue.axios.put(`/api/v2/purchaseOrders/${payload.uuid}`, payload);
export const deletePurchaseOrderApi = (payload) => Vue.axios.delete(`/api/v2/purchaseOrders/${payload.uuid}`, payload);

// Route::put('pos/{po}', 'PurchaseOrderController@update');
// Route::delete('pos/{po}', 'PurchaseOrderController@delete');

// Route::get('vos', 'VariationOrderController@getAllVO');
// Route::post('vos', 'VariationOrderController@store');
export const createVariationOrderApi = (payload) => Vue.axios.post('/api/v2/variationOrders', payload);
export const fetchVariationOrderApi = (params) => Vue.axios.get('/api/v2/variationOrders', {params});
export const updateVariationOrderApi = (payload) => Vue.axios.put(`/api/v2/variationOrders/${payload.uuid}`, payload);
export const deleteVariationOrderApi = (payload) => Vue.axios.delete(`/api/v2/variationOrders/${payload.uuid}`, payload);

// Route::put('vos/{vo}', 'VariationOrderController@update');
// Route::delete('vos/{vo}', 'VariationOrderController@delete');

// expenses

export const fetchProjectExpensesApi = (params) => Vue.axios.get(`/api/v2/projects/${params.uuid}/expenses`, {
  params
});

export const createProjectExpensesApi = (payload) => Vue.axios.post('/api/v2/expenses', payload);

export const updateProjectExpensesApi = (payload) => Vue.axios.put(`/api/v2/expenses/${payload.uuid}`, payload);

export const deleteExpensesApi = (payload) => Vue.axios.delete(`/api/v2/expenses/${payload.uuid}`);

// expenses payments

export const createExpensesPaymentApi = (payload) => Vue.axios.post('/api/v2/payments', payload);

export const updateExpensesPaymentApi = (payload) => Vue.axios.put(`/api/v2/payments/${payload.uuid}`, payload);

export const deleteExpensesPaymentApi = (params) => Vue.axios.delete(`/api/v2/payments/${params.uuid}`);

// cashflow projection

export const fetchProjectCashflowProjectionApi = (params) => Vue.axios.get(`/api/v2/projects/${params.uuid}/cashflow-projection`, {params});

// Export MYOB

export const fetchProjectInvoicesMYOBApi = (id, params) => Vue.axios.get(`/api/v2/projects/${id}/invoices/myob`, {params})

export const fetchProjectExpenseMYOBApi = (id, params) => Vue.axios.get(`/api/v2/projects/${id}/expenses/myob`, {params})
