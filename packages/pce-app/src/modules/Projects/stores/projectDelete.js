import {Store} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {deleteProjectApi} from '../apis';

@Store('projectDelete')
class ProjectDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteProjectApi),
  ];
}

export const projectDelete = new ProjectDelete();
