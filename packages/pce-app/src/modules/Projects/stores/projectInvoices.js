import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {Action, Store, State} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {createProjectInvoiceApi, fetchProjectInvoicesApi, updateProjectInvoiceApi, deleteProjectInvoiceApi} from '../apis';
import {invoiceTableHeaders, projectInvoicesDataFormatter} from '../resources/projectInvoices';
import {InvoiceForm} from './projectInvoicesForm';

@Store('createInvoicesForm')
class CreateInvoicesForm extends BaseStore {
  static mixins = [
    new InvoiceForm({
      hooks: {
        onSubmitSuccess() {
          this._vm.$toast('Invoice created successfully.', {
            color: 'success',
            closeBtn: true,
          })
        },
      }
    }),
    new ApiStore(createProjectInvoiceApi)
  ]
}

@Store('updateInvoiceForm')
class UpdateInvoiceForm extends BaseStore {
  static mixins = [
    new InvoiceForm({
      hooks: {
        onSubmitSuccess() {
          this._vm.$toast('Invoice updated successfully.', {
            color: 'success',
            closeBtn: true,
          })
        },
      },
      options: {
        resetFormOnSubmitSuccess: false,
      },
    }),
    new ApiStore(updateProjectInvoiceApi)
  ];

  @State
  invoiceID = undefined
}

@Store('projectInvoicesTable')
class ProjectInvoicesTable extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectInvoicesApi, [], projectInvoicesDataFormatter),
    new TableStore({
      tableColumns: invoiceTableHeaders,
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

@Store('projectInvoices')
class ProjectInvoices extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectInvoicesApi)
  ];

  @State
  showCreateForm = false;

  @State
  showUpdateForm = false;

  @State
  showPaymentForm = false
}

@Store('projectInvoiceDelete')
class ProjectInvoiceDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteProjectInvoiceApi, []),
  ];

  @Action
  async delete(invoice, successCallback) {
    try {
      await this.fetchData({
        uuid: invoice.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${invoice.cert_no}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

export const projectInvoices = new ProjectInvoices();
export const createInvoicesForm = new CreateInvoicesForm();
export const projectInvoicesTable = new ProjectInvoicesTable();
export const updateInvoiceForm = new UpdateInvoiceForm();
export const projectInvoiceDelete = new ProjectInvoiceDelete();
