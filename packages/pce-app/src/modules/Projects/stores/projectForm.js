import {FormStore} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';

export class ProjectForm extends FormStore {
  constructor({fields, options, hooks, ...args}) {
    super({
      fields: {
        name: {
          value: '',
          label: 'Project\'s Name',
          placeholder: '',
          rules: 'required'
        },
        status: {
          value: 'New',
          label: 'Status',
          rules: 'required'
        },
        contract_sum: {
          value: 0,
          format: 'float',
          label: 'Contract Sum',
          rules: 'required|min:0|max:999999999999999',
        },
        client: {
          value: '',
          label: 'Client',
          rules: 'required'
        },
        variation_order: {
          value: 0,
          format: 'float',
          label: 'Variation Order',
          rules: 'min:0',
        },
        end_customer: {
          value: '',
          label: 'End Customer',
        },
        retention_percent: {
          value: 10,
          label: 'Retention Percent',
          rules: 'required|numeric|max:100|gte:0',
        },
        retention: {
          value: 0,
          format: 'float',
        },
        schedule: {
          value: '',
          label: 'Services',
        },
        project_managers: {
          value: [],
          label: 'Project Managers',
          rules: 'required',
          payloadFormatter(field) {
            const managers = field.value;
            return managers.map(pm => pm.id)
          }
        },
        budget: {
          value: 0,
          format: 'float',
          label: 'Budget',
          rules: 'min:0|max:999999999999999',
        },
        start_date: {
          value: undefined,
          label: 'Start Date',
          rules: 'required|before:end_date|date_format:Y-m-d',
        },
        end_date: {
          value: undefined,
          label: 'End Date',
          rules: 'required|after:start_date|date_format:Y-m-d',
        },
        contingency_amt: {
          value: 0,
          format: 'float',
          label: 'Contingency Amount',
          rules: 'min:0|max:999999999999999',
        },
        omission_value: {
          value: 0,
          format: 'float',
          label: 'Omission Amount',
          rules: 'min:0|max:999999999999999',
        },
        ...fields,
      },
      options: {
        resetFormOnSubmitSuccess: true,
        ...options,
      },
      hooks: {
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
        ...hooks,
      },
      ...args
    })
  }
}
