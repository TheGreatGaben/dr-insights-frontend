import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Getter} from '@nixel/stores';
import {State} from '@nixel/stores';
import {fetchProjectDetailsApi} from '../apis'

import {ProjectForm} from './projectForm'
import {updateProject} from '../apis'

@Store('updateProjectForm')
class UpdateProjectForm extends BaseStore {
  static mixins = [
    new ProjectForm({
      options: {
        resetFormOnSubmitSuccess: false,
      }
    }),
    new ApiStore(updateProject)
  ]
}

@Store('projectDetails')
class ProjectDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectDetailsApi, {})
  ];

  @State
  showUpdateForm = false;

  // for breadcrumb to get project's name
  @Getter
  name() {
    return this.data.get('name')
  }

  clientname() {
    return this.data.get('client')
  }

  projectmanager() {
    return this.data.get('project_managers')
  }
  projectuuid() {
    return this.data.get('uuid')
  }

  @Getter
  client() {
    return {
      name: this.data.get('client'),
      uuid: this.data.get('client_uuid'),
    }
  }
}

export const projectDetails = new ProjectDetails();
export const updateProjectForm = new UpdateProjectForm();
