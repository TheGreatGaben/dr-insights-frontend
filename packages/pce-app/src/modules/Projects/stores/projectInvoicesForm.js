import {addDays} from 'date-fns'
import {FormStore} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {date} from '@nixel/base/filters/datetime';

export class InvoiceForm extends FormStore {
  constructor({fields, options, hooks}) {
    super({
      fields: {
        invoice_date: {
          value: date(new Date()),
          label: 'Invoice Date',
          rules: 'date_format:Y-m-d',
        },
        invoice_no: {
          value: 'Inv: ',
          label: 'Invoice No.',
        },
        cert_no: {
          value: '',
          label: 'Cert No.',
          rules: 'required'
        },
        term: {
          value: 30,
          label: 'Term',
          format: 'int',
          rules: 'required|integer|min:0',
        },
        due_date: {
          // Calculated based on term days
          value: date(addDays(new Date(), 30)),
          label: 'Due Date',
          rules: 'date_format:Y-m-d',
        },
        work_done_certified: {
          value: 0,
          format: 'float',
          label: 'Work Done Cert.',
          rules: 'min:0',
        },
        vo: {
          value: 0,
          label: 'VO',
          format: 'float',
          rules: 'min:0',
        },
        retention_released: {
          value: 0,
          format: 'float',
          label: 'Retention Released',
          rules: 'min:0',
        },
        retention: {
          value: 0,
          format: 'float',
          label: 'Less Retention',
          rules: 'min:0',
          formatter(field) {
            const value = parseFloat(field.value);
            if (value < 0) {
              return value * -1
            }
            return value
          }
        },
        debit_note: {
          value: 0,
          format: 'float',
          label: 'Backcharge',
          rules: 'min:0',
          formatter(field) {
            const value = parseFloat(field.value);
            if (value < 0) {
              return value * -1
            }
            return value
          }
        },
        tax_type: {
          value: 'sst',
        },
        tax_amt: {
          value: 0,
          format: 'float',
          label: 'Tax',
          rules: 'min:0',
        },
        exclude_tax: {
          value: true,
        },
        prev_certified: {
          value: 0,
          format: 'float',
          label: 'Prev Certified',
          rules: 'min:0',
          formatter(field) {
            const value = parseFloat(field.value);
            if (value < 0) {
              return value * -1
            }
            return value
          }
        },
        nett_certified: {
          value: 0,
          format: 'float',
          label: 'Nett Certified',
          rules: 'min:0',
        },
        client_id: {
          label: 'Client',
          payloadFormatter(field) {
            const client = field.value;
            return client ? client.uuid : undefined
          },
          rules: 'required',
        },
        acc_code_id: {
          value: undefined,
          label: 'Account Code',
          rules: 'required',
        },
        description: {
          value: '',
          label: 'Description',
          type: 'textarea',
        },
        ...fields,
      },
      options: {
        resetFormOnSubmitSuccess: true,
        ...options,
      },
      hooks: {
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
        ...hooks,
      },
    })
  }
}
