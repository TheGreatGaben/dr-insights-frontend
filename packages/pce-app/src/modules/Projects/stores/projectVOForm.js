import {get, compact} from 'lodash';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';
import {State} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {date} from '@nixel/base/filters/datetime';
import {deleteVariationOrderApi,
  updateVariationOrderApi,
  createVariationOrderApi} from '../apis';
import {apiErrorNotification} from '@/utils/request';
import {formHooksHandler} from '@nixel/stores';

@Store('projectContractorVOForm')
class ProjectContractorVOForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        vos: {
          value: []
        },
        vosToBeDeleted: {
          value: []
        },
      },
      hooks: {
        onSubmitFailed(formStore, error) {
          // update responseError to update nested fields
          this.responseError.assign(get(error, 'response.data.data.errors'));

          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error, 'An error occurred on the server')
        },
      }
    }),
  ];

  @State
  project_id = '';

  @State
  responseError = {};

  @State
  contractor_id = '';

  @Action
  setVOs(newVOs) {
    this.fields.get().vos.setValue(newVOs);
    this.fields.get().vosToBeDeleted.setValue([])
  }

  @Action
  updateVO = function(index, key, newVal) {
    const vos = this.fields.get().vos.value;
    vos[index][key] = newVal;
    this.fields.get().vos.setValue(vos)
  };

  @Action
  addVO(vo) {
    const vos = [...this.fields.get().vos.value];
    vos.push(vo);
    this.fields.get().vos.setValue(vos)
  }

  @Action
  removeVO(voToBeRemoved, index) {
    const vos = this.fields.get().vos.value;
    const filteredVOs = vos.filter((item, current) =>
      current !== index
    );
    this.fields.get().vos.setValue(filteredVOs);
    if (voToBeRemoved.uuid) {
      const vosToBeDeleted = this.fields.get().vosToBeDeleted.value;
      vosToBeDeleted.push(voToBeRemoved);
      this.fields.get().vosToBeDeleted.setValue(vosToBeDeleted)
    }
  }

  @Action
  async submit(params) {
    this.project_id.assign(params.project_id);
    this.contractor_id.assign(params.contractor_id);
    const formStore = this;
    try {
      await Promise.all([formStore.createVOs(), formStore.updateVOs(), formStore.deleteVOs()]);
      formHooksHandler('onSubmitSuccess', formStore, true)
    } catch (error) {
      formHooksHandler('onSubmitFailed', formStore, error);
      return Promise.reject(error)
    }
  }

  @Action
  async createVOs() {
    const vos = this.fields.get().vos.value;
    return new Promise(async (resolve, reject) => {
      for (const index in vos) {
        // noinspection JSUnfilteredForInLoop
        const vo = vos[index];
        if (!vo.uuid) {
          try {
            const newVO = {...vo};
            newVO.project_id = this.project_id.get();
            newVO.contractor_id = this.contractor_id.get();
            newVO.issued_date = date(newVO.issued_date); // reformat date string to Y-m-d
            await createVariationOrderApi(newVO)
          } catch (error) {
            return reject(error)
          }
        }
      }

      // resolve promise
      resolve()
    })
  }

  @Action
  async updateVOs() {
    const vos = this.fields.get().vos.value;

    return new Promise(async (resolve, reject) => {
      for (const index in vos) {
        // noinspection JSUnfilteredForInLoop
        const vo = vos[index];
        if (vo.uuid) {
          const newVO = {...vo};
          newVO.issued_date = date(newVO.issued_date); // reformat date string to Y-m-d
          try {
            await updateVariationOrderApi(newVO)
          } catch (error) {
            // reject promise
            return reject(error)
          }
        }
      }
      // resolve promise
      resolve()
    })
  }

  @Action
  async deleteVOs() {
    const vosToBeDeleted = this.fields.get().vosToBeDeleted.value;
    return new Promise(async (resolve, reject) => {
      for (const index in vosToBeDeleted) {
        // noinspection JSUnfilteredForInLoop
        const vo = vosToBeDeleted[index];
        try {
          await deleteVariationOrderApi(vo);
          // noinspection JSUnfilteredForInLoop
          delete vosToBeDeleted[index];

          this.updateValues({
            vosToBeDeleted: compact(vosToBeDeleted),
          });
        } catch (error) {
          // reject promise
          return reject(error)
        }
      }

      return resolve()

    });
  }
}

export const projectContractorVOForm = new ProjectContractorVOForm();
