import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';
import {State} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {createExpensesPaymentApi, updateExpensesPaymentApi, deleteExpensesPaymentApi} from '../apis';
import {apiErrorNotification} from '@/utils/request';
import {formHooksHandler} from '@nixel/stores';

@Store('expensesPaymentForm')
class ExpensesPaymentForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        payments: {
          value: []
        },
        paymentsToBeDeleted: {
          value: []
        },
      },
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Payments updated successfully.', {
            color: 'success',
            closeBtn: true,
          })
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error, 'An error occurred on the server')
        },
      }
    }),
  ];

  @State
  expenses = {};

  @Action
  setPayments(newPayments) {
    this.fields.get().payments.setValue(newPayments);
    this.fields.get().paymentsToBeDeleted.setValue([])
  }

  @Action
  updatePayment = function(index, key, newVal) {
    const payments = this.fields.get().payments.value;
    payments[index][key] = newVal;
    this.fields.get().payments.setValue(payments)
  };

  @Action
  addPayment(payment) {
    const payments = [...this.fields.get().payments.value];
    payments.push(payment);
    this.fields.get().payments.setValue(payments)
  }

  @Action
  removePayment(paymentToBeRemoved, index) {
    const payments = this.fields.get().payments.value;
    const filteredPayments = payments.filter((item, current) =>
      current !== index
    );
    this.fields.get().payments.setValue(filteredPayments);
    if (paymentToBeRemoved.uuid) {
      const paymentsToBeDeleted = this.fields.get().paymentsToBeDeleted.value;
      paymentsToBeDeleted.push(paymentToBeRemoved);
      this.fields.get().paymentsToBeDeleted.setValue(paymentsToBeDeleted)
    }
  }

  @Action
  async submit() {
    const formStore = this;
    try {
      await Promise.all([formStore.createPayments(), formStore.updatePayments(), formStore.deletePayments()]);
      formHooksHandler('onSubmitSuccess', formStore, true)
    } catch (error) {
      formHooksHandler('onSubmitFailed', formStore, error);
      return Promise.reject(error)
    }
  }

  // Must have batch create update and delete payments
  @Action
  async createPayments() {
    const payments = this.fields.get().payments.value;
    let hasError = false;
    for (const index in payments) {
      const payment = payments[index];
      if (!payment.uuid) {
        try {
          await createExpensesPaymentApi(payment)
        } catch (error) {
          console.error(error);
          hasError = true
        }
      }
    }
    return hasError ? Promise.reject() : Promise.resolve()
  }

  @Action
  async updatePayments() {
    const payments = this.fields.get().payments.value;
    let hasError = false;
    for (const index in payments) {
      const payment = payments[index];
      if (payment.uuid) {
        try {
          await updateExpensesPaymentApi(payment)
        } catch (error) {
          console.error(error);
          hasError = true
        }
      }
    }
    return hasError ? Promise.reject() : Promise.resolve()
  }

  @Action
  async deletePayments() {
    const paymentsToBeDeleted = this.fields.get().paymentsToBeDeleted.value;
    let hasError = false;
    for (const index in paymentsToBeDeleted) {
      const payment = paymentsToBeDeleted[index];
      try {
        await deleteExpensesPaymentApi(payment)
      } catch (error) {
        console.error(error);
        hasError = true
      }
    }
    return hasError ? Promise.reject() : Promise.resolve()
  }
}

export const expensesPaymentForm = new ExpensesPaymentForm();
