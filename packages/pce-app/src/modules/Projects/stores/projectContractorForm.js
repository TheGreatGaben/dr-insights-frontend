import {isEmpty} from 'lodash';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {Getter, State} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
// import {apiErrorNotification} from '@/utils/request';
import {createProjectContractorApi, updateProjectContractorApi} from '../apis';

@Store('projectContractorForm')
class ProjectContractorForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        name: {
          value: '',
          label: 'Contractor',
          rules: 'required'
        },
        term: {
          value: 30,
          label: 'Term',
          format: 'int',
          rules: 'required|integer',
        },
        uuid: {
          value: '',
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
    })
  ];

  @Getter
  isUpdateForm() {
    return !isEmpty(this.fields.get().uuid.value);
  }

  @State
  formType = undefined; // create | update

  @State
  showFormModal = false;
}

@Store('createContractor')
class CreateContractor extends BaseStore {
  static mixins = [
    new ApiStore(createProjectContractorApi)
  ]
}

@Store('updateContractor')
class UpdateContractor extends BaseStore {
  static mixins = [
    new ApiStore(updateProjectContractorApi)
  ]
}

export const projectContractorForm = new ProjectContractorForm();
export const createContractor = new CreateContractor();
export const updateContractor = new UpdateContractor();
