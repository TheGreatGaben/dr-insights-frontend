import {addDays} from 'date-fns'
import {FormStore} from '@nixel/stores';
import {date} from '@nixel/base/filters/datetime'
import {apiErrorNotification} from '@/utils/request';

export class ExpensesForm extends FormStore {
  constructor({fields, options, hooks}) {
    super({
      fields: {
        invoice_date: {
          value: new Date().toISOString().substr(0, 10),
          label: 'Invoice Date',
          rules: 'date_format:Y-m-d',
        },
        date_received: {
          value: new Date().toISOString().substr(0, 10),
          label: 'Date Received',
          rules: 'date_format:Y-m-d',
        },
        contractor: {
          value: '',
          label: 'Contractor',
          rules: 'required'
        },
        contractor_type: {
          value: '',
          label: 'Contractor Type',
          rules: 'required'
        },
        ref_no: {
          value: 'Inv: ',
          label: 'Reference No.',
          rules: 'required'
        },
        payable_id: {
          value: 0,
        },
        payable_type: {
          value: '',
        },
        povo_no: {
          value: '',
          label: 'Contractor PO/VO'
        },
        acc_code_id: {
          value: undefined,
          label: 'Account Code',
          rules: 'required',
        },
        term: {
          value: 30,
          label: 'Term',
          format: 'int',
          rules: 'required|integer|min:0',
        },
        due_date: {
          // Calculated based on term days
          value: date(addDays(new Date(), 30)),
          label: 'Due Date',
          rules: 'date_format:Y-m-d',
        },
        description: {
          value: '',
          label: 'Description',
          type: 'textarea'
        },
        invoice_amt: {
          value: 0,
          format: 'float',
          label: 'Invoice Amount',
          rules: 'min:0',
        },
        dc_note: {
          value: 0,
          format: 'float',
          label: 'Debit Note',
          rules: 'min:0',
        },
        credit_note: {
          value: 0,
          format: 'float',
          label: 'Credit Note',
          rules: 'min:0',
        },
        tax_amt: {
          value: 0,
          format: 'float',
          label: 'Tax',
          rules: 'min:0',
        },
        tax_type: {
          value: 'sst',
        },
        exclude_tax: {
          value: true,
        },
        ...fields,
      },
      options: {
        resetFormOnSubmitSuccess: true,
        ...options,
      },
      hooks: {
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
        ...hooks,
      },
    })
  }
}
