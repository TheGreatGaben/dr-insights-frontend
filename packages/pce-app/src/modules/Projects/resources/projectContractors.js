import {date} from '@nixel/base/filters/datetime'

/**
 *
 * @type {TableColumn[]}
 */
export const projectContractorsTableHeaders = [
  {
    align: 'center',
    type: 'expand',
    style: {
      width: '10px',
      paddingLeft: '0px',
      paddingRight: '0px',
    }
  },
  {
    text: 'Contractor',
    align: 'left',
    sortable: true,
    value: 'name',
    minWidth: '175px',
    width: '15%',
    type: 'singleLine'
  },
  {
    text: 'Total PO',
    align: 'right',
    sortable: true,
    value: 'total_purchase_order',
    type: 'currency'
  },
  {
    text: 'Total VO',
    align: 'right',
    sortable: true,
    value: 'total_variation_order',
    type: 'currency'
  },
  {
    text: 'Total Amount',
    align: 'right',
    sortable: true,
    value: 'total_amount',
    type: 'currency'
  },
  {
    text: 'Total Expenses',
    align: 'right',
    sortable: true,
    value: 'total_expenses',
    type: 'currency'
  },
  {
    text: 'Total Paid',
    align: 'right',
    sortable: true,
    value: 'total_paid',
    type: 'currency'
  },
  {
    text: 'Balance',
    align: 'right',
    sortable: true,
    value: 'total_payable',
    type: 'currency'
  },
];

/**
 *
 * @type {TableColumn[]}
 */
export const projectContractorsPOTableHeaders = [
  {
    text: 'PO No.',
    align: 'left',
    sortable: false,
    value: 'po_no',
  },
  {
    text: 'Issued Date',
    align: 'left',
    sortable: false,
    value: 'issued_date',
    type: 'date'
  },
  {
    text: 'Total Value',
    align: 'right',
    sortable: false,
    value: 'total_value',
    type: 'currency'
  },
  {
    text: 'Budget',
    align: 'right',
    sortable: false,
    value: 'budget',
    type: 'currency'
  },
  {
    text: 'Total Expenses',
    align: 'right',
    sortable: false,
    value: 'total_expenses',
    type: 'currency'
  },
  {
    text: 'Total Paid',
    align: 'right',
    sortable: false,
    value: 'total_paid',
    type: 'currency'
  },
];

/**
 *
 * @type {TableColumn[]}
 */
export const projectContractorsVOTableHeaders = [
  {
    text: 'VO No.',
    align: 'left',
    sortable: false,
    value: 'vo_no',
  },
  {
    text: 'Issued Date',
    align: 'left',
    sortable: false,
    value: 'issued_date',
    type: 'date'
  },
  {
    text: 'Total Value',
    align: 'right',
    sortable: false,
    value: 'total_value',
    type: 'currency'
  },
  {
    text: 'Total Expenses',
    align: 'right',
    sortable: false,
    value: 'total_expenses',
    type: 'currency'
  },
  {
    text: 'Total Paid',
    align: 'right',
    sortable: false,
    value: 'total_paid',
    type: 'currency'
  },
];

export const defaultPOPayload = Object.freeze({
  budget: 0,
  po_no: '',
  issued_date: new Date().toISOString().substr(0, 10),
  total_value: 0
});

export const defaultVOPayload = Object.freeze({
  vo_no: '',
  issued_date: new Date().toISOString().substr(0, 10),
  total_value: 0
});

export function createContractorDataFormatter(response) {
  return response.data.data.contractor
}

export function projectPODataFormatter(response) {
  const pos = response.data.data ? response.data.data : response.data;
  return pos.map(po => {
    return {
      ...po,
      issued_date: date(po.issued_date)
    }
  })
}

export function projectVODataFormatter(response) {
  const vos = response.data.data ? response.data.data : response.data;
  return vos.map(vo => {
    return {
      ...vo,
      issued_date: date(vo.issued_date)
    }
  })
}
