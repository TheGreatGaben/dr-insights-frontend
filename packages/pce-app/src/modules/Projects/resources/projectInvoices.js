import {getTaxTypeByDate, taxTypes} from '@/consts/tax'
import {date} from '@nixel/base/filters/datetime'

export const accountCodeDefaultQuery = {
  code: '4-0000',
}

export const invoiceTableHeaders = [
  {
    text: 'Invoice Date',
    value: 'invoice_date',
    type: 'date',
    sortable: true,
  },
  {
    text: 'Cert No.',
    value: 'cert_no',
    type: 'singleLine',
    sortable: true,
  },
  {
    text: 'Work Done Cert.',
    value: 'work_done_certified',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Variation Order',
    value: 'vo',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Retention Released',
    value: 'retention_released',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Less Retention',
    value: 'retention',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Backcharge',
    value: 'debit_note',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Prev Certified',
    value: 'prev_certified',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Nett Certified',
    value: 'total_invoiced',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Tax',
    value: 'tax_amt',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Total Received',
    value: 'total_received_in_period',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Balance',
    value: 'total_receivable',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
  {
    text: 'Total Advance Received',
    value: 'total_advance_received',
    type: 'currency',
    align: 'right',
    minWidth: '175px',
    sortable: true,
  },
];

export const defaultPaymentPayload = Object.freeze({
  invoice_id: '',
  project_id: '',
  cash_flow: 'IN',
  payment_date: new Date().toISOString().substr(0, 10),
  cheque_no: '',
  payment_amt: 0,
});

export function projectInvoicesDataFormatter(response) {
  const invoices = response.data.data ? response.data.data : response.data;
  return invoices.map(invoice => {
    return {
      nett_certified: invoice.total_invoiced,
      ...invoice,
      due_date: date(invoice.due_date),
    }
  })
}

export function projectDataFormatter(project) {
  const contractSum = parseFloat(project.contract_sum);
  const retention = parseFloat(project.retention);
  const retentionPercent = (retention / contractSum) * 100;
  return {
    name: project.name,
    project_managers: project.project_managers,
    contract_sum: project.contract_sum,
    retention: project.retention,
    retention_percent: retentionPercent,
  }
}

export function invoiceDataFormatter(invoice) {
  const afterSSTDate = getTaxTypeByDate(invoice.invoice_date);
  const taxType = afterSSTDate ? taxTypes[1].value : taxTypes[0].value;
  const nettTotal = parseFloat(invoice.total_invoiced) + parseFloat(invoice.tax_amt);
  return {
    invoice_no: invoice.invoice_no,
    cert_no: invoice.cert_no,
    invoice_date: invoice.invoice_date,
    description: invoice.description,
    work_done_certified: invoice.work_done_certified,
    variation_order: invoice.vo,
    retention_released: invoice.retention_released,
    less_retention: invoice.retention,
    prev_certified: invoice.prev_certified,
    debit_note: invoice.debit_note,
    nett_certified: invoice.total_invoiced,
    tax_type: taxType,
    tax_amt: invoice.tax_amt,
    nett_total: nettTotal,
    balance: invoice.total_receivable
  }
}

export function clientDataFormatter(project) {
  // noinspection JSUnresolvedVariable
  return {
    name: project.client,
    address_1: project.client_address_1,
    city: project.client_city,
    postcode: project.client_postcode,
    state: project.client_state,
    country: project.client_country,
    fax_no: project.client_fax_no,
    tel_no: project.client_tel_no,
    end_customer: project.client_end_customer,
    person_in_charge: project.client_person_in_charge,
  }
}
