import {profitMarginFormula, progressFormula, projectedMarginFormula} from '@/consts/formulaLabels';
import {subMonths, startOfMonth} from 'date-fns';
import {monthAndDay} from '@nixel/base/filters/datetime';

export const cashflowStartMonth = monthAndDay(startOfMonth(subMonths(new Date(), 1)));

export const projectListDateFilters = [
  'invoice_date[start]',
  'invoice_date[end]'
];

export const projectTableHeaders = [
  {
    text: 'Project\'s Name',
    sortable: true,
    align: 'left',
    value: 'name',
    required: true,
  },
  {
    text: 'Contract Sum',
    sortable: true,
    align: 'left',
    type: 'currency',
    value: 'contract_sum'
  },
  {
    text: 'Nett Cash Flow',
    sortable: true,
    align: 'left',
    type: 'currency',
    value: 'cashflow_nett'
  },
  {
    text: `Cash Flow (starting ${cashflowStartMonth})`,
    sortable: false,
    align: 'left',
    value: 'cashflow', // Not in actual data payload
    formatter(project) {
      // formatter for export excel
      return `
      30 days : ${project.cashflow_30}\n
      60 days : ${project.cashflow_60}\n
      90 days : ${project.cashflow_90}\n
      `
    },
  },
  {
    text: 'Total Invoiced',
    sortable: true,
    align: 'left',
    type: 'currency',
    tag: 'p',
    value: 'total_invoiced'
  },
  {
    text: 'Total Expenses',
    sortable: true,
    align: 'left',
    type: 'currency',
    tag: 'p',
    value: 'total_expenses'
  },
  {
    text: 'Variation Order',
    sortable: true,
    align: 'left',
    type: 'currency',
    tag: 'p',
    value: 'variation_order'
  },
  {
    text: 'Omission Amount',
    sortable: true,
    align: 'left',
    type: 'currency',
    tag: 'p',
    value: 'omission_value'
  },
  {
    text: 'Contingency Amount',
    sortable: true,
    align: 'left',
    type: 'currency',
    tag: 'p',
    value: 'contingency_amt'
  },
  {
    text: 'Cashflow Margin',
    sortable: true,
    align: 'left',
    type: 'float',
    value: 'cashflow_margin'
  },
  {
    text: 'Profit Margin (%)',
    value: 'profit_margin',
    type: 'float',
    align: 'left',
    sortable: true,
  },
  {
    text: 'Projected Margin (%)',
    value: 'projected_margin',
    type: 'float',
    align: 'left',
    sortable: true,
  },
  {
    text: 'Duration Variance (%)',
    value: 'progress_diff',
    align: 'left',
    sortable: true,
  },
];

export const projectAllColumns = [
  'name',
  'contract_sum',
  'cashflow_nett',
  'cashflow',
  'total_invoiced',
  'total_expenses',
  'variation_order',
  'omission_value',
  'contingency_amt',
  'profit_margin',
  'progress_diff',
  'cashflow_margin',
];

export const projectDurationColumns = [
  'name',
  'contract_sum',
  'profit_margin',
  'progress_diff'
];

export const projectCashflowColumns = [
  'name',
  'contract_sum',
  'cashflow_nett',
  'cashflow',
  'total_invoiced',
  'total_expenses',
  'variation_order',
  'omission_value',
  'contingency_amt',
];

export const projectSalesTableHeaders = [
  {
    text: 'Invoice Date',
    value: 'invoice_date',
    align: 'left',
    type: 'date',
  },
  {
    text: 'Cert No.',
    value: 'cert_no',
    align: 'center',
  },
  {
    text: 'Work Done Certified',
    value: 'work_done_certified',
    align: 'right',
    type: 'currency',
  },
  {
    text: 'Nett Certified',
    value: 'total_invoiced',
    align: 'right',
    type: 'currency',
  },
];

export const projectPurchasesTableHeaders = [
  {
    text: 'Date Received',
    value: 'date_received',
    align: 'left',
    type: 'date',
  },
  {
    text: 'Contractor',
    value: 'contractor_name',
    align: 'left',
  },
  {
    text: 'Ref No.',
    value: 'ref_no',
    align: 'center',
  },
  {
    text: 'Due Date',
    value: 'due_date',
    align: 'center',
    type: 'date',
  },
  {
    text: 'Invoice Amount',
    value: 'total_expenses',
    align: 'right',
    type: 'currency',
  },
];

export function projectCashflowDataFormatter(response) {
  const data = response.data.data ? response.data.data : response.data;
  data.invoices = data.invoices.map(invoice => {
    return {
      ...invoice,
      total_invoiced: invoice.nett_certified,
    }
  });
  data.expenses = data.expenses.map(expense => {
    return {
      ...expense,
      contractor_name: expense.contractor,
      total_expenses: expense.invoice_amt,
    }
  });
  return data
}

export const projectExpectedSalesTableHeaders = [
  {
    text: 'Due Date',
    value: 'due_date',
    align: 'left',
    type: 'date',
  },
  {
    text: 'Cert No.',
    value: 'cert_no',
    align: 'center',
  },
  {
    text: 'Nett Certified',
    value: 'total_amount',
    align: 'right',
    type: 'currency',
  },
  {
    text: 'Balance',
    value: 'balance',
    align: 'right',
    type: 'currency',
  },
];

export const projectExpectedPurchasesTableHeaders = [
  {
    text: 'Due Date',
    value: 'due_date',
    align: 'left',
    type: 'date',
  },
  {
    text: 'Contractor',
    value: 'contractor',
    align: 'left',
  },
  {
    text: 'Ref No.',
    value: 'ref_no',
    align: 'center',
  },
  {
    text: 'Invoice Amount',
    value: 'total_amount',
    align: 'right',
    type: 'currency',
  },
  {
    text: 'Balance',
    value: 'balance',
    align: 'right',
    type: 'currency',
  },
];
