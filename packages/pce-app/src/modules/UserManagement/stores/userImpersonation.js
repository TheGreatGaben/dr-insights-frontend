import Vue from 'vue';
import {apiErrorNotification} from '@/utils/request';
import {setImpersonationSession} from '@/utils/auth';
import {getImpersonationTokenID, removeImpersonationToken} from '@/utils/auth';
import {BaseStore, State} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';

export function takeUserImpersonationApi(email) {
  return Vue.axios.get('/api/v2/impersonate-user/take', {
    params: {
      email
    }
  })
}

export function leaveUserImpersonationApi(tokenID) {
  return Vue.axios.delete('/api/v2/impersonate-user/leave', {
    params: {
      token_id: tokenID
    }
  })
}

@Store('userImpersonation')
class UserImpersonation extends BaseStore {
  @State
  impersonating = false;

  @Action
  async impersonateUser({email, name}) {
    try {
      this.impersonating.assign(true);

      const response = await takeUserImpersonationApi(email)
      const token = response.data.data.access_token
      const tokenID = response.data.data.token_id
      setImpersonationSession(token, tokenID)

      this._vm.$toast(`You are now impersonating ${name}`, {
        color: 'success',
        closeBtn: true,
      });

      // redirect to start impersonating
      window.location = '/';

    } catch (error) {
      apiErrorNotification(error);
    } finally {
      this.impersonating.assign(false);
    }
  }

  @Action
  async leaveImpersonation() {
    try {
      const tokenID = getImpersonationTokenID()
      await leaveUserImpersonationApi(tokenID)
      removeImpersonationToken()
      this.$router.push('/')
      this.$router.go() // Refresh

    } catch (error) {
      console.error(error)
      apiErrorNotification(error)
    }
  }
}

export const userImpersonation = new UserImpersonation();

