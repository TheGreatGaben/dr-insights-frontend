import Vue from 'vue';
import {BaseStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {FormStore} from '@nixel/stores';
import {roleTableList} from './roleList';

const updateRoleDetailsApi = (params) => Vue.axios.put(`/api/v2/roles/${params.id}`, params);

@Store('roleForm')
class UpdateRoleForm extends BaseStore {
  static mixins = [
    new ApiStore(updateRoleDetailsApi),
    new FormStore({
      fields: {
        name: {
          value: '',
          label: 'Role\'s Name',
          placeholder: 'Role\'s name',
          rules: 'required|max:200',
        },
        id: {},
      },
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update role.', {
            color: 'success'
          });

          // update list
          roleTableList.fetchData()
        },
        onSubmitFailed(formStore, error) {
          // attach validation errors to fields
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]

}

export const roleDetails = new UpdateRoleForm();

