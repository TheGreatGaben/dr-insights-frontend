import Vue from 'vue';
import {apiErrorNotification} from '@/utils/request';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';

const deleteUserApi = (params) => Vue.axios.delete(`/api/v2/users/${params.uuid}`);

@Store('userDelete')
class UserDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteUserApi, []),
  ];

  @Action
  async deleteUser(user, successCallback) {
    try {
      userDelete.fetchData({
        uuid: user.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${user.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      apiErrorNotification(error);
    }
  }
}

export const userDelete = new UserDelete();

