/**
 *
 * @type {TableColumn[]}
 */
export const userTableHeaders = [
  {
    text: '',
    type: 'expand',
    align: 'center',
    width: '80px',
  },
  {
    text: 'No.',
    type: 'index',
    align: 'left',
    width: '80px',
  },
  {
    text: 'User\'s Name',
    sortable: true,
    value: 'name',
    align: 'left',
  },
  {
    text: 'Roles',
    value: 'roles',
    align: 'left',
    width: '30%',
  },
  {
    text: 'Permissions',
    value: 'permissions',
    align: 'left',
    width: '30%',
  },
];
