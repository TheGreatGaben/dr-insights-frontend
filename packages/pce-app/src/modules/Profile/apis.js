// noinspection JSUnusedLocalSymbols
import Vue from 'vue';

export function changePasswordApi(params) {
  return Vue.axios.post('/api/password/change', params)
}
