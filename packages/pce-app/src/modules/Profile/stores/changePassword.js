import {BaseStore} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {app} from '@/main';
import {delay} from '@/utils/index';
import {changePasswordApi} from '../../Auth/apis';

@Store('changePassword')
class ChangePassword extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        password: {
          value: '',
          label: 'Password',
          placeholder: 'Set your new password here.',
          type: 'password',
          rules: 'required|minLength:6',
        },
        password_confirmation: {
          value: '',
          label: 'Password Confirmation',
          placeholder: '',
          type: 'password',
          rules: 'required|minLength:6|is:password',
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
      hooks: {
        async onSubmitSuccess() {
          app.$toast('Password has been updated.', {
            color: 'success',
            closeBtn: true,
          });

          await delay(2000);
          this.$router.push({path: '/login'})
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error, 'An error has occurred on the server.')
        },
      }
    }),
    new ApiStore(changePasswordApi)
  ]
}

export const changePassword = new ChangePassword();
