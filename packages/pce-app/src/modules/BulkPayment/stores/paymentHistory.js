import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Nested} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchBulkPaymentsApi = (params) => Vue.axios.get('/api/v2/payment_records', {params});

export const deleteBulkPaymentApi = (params) => Vue.axios.delete(`/api/v2/payment_records/${params.uuid}`);

@Store('paymentHistorySummary')
class PaymentHistorySummary extends BaseStore {
  static mixins = [
    new TableStore({
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      },
      tableColumns: [
        {
          text: 'Project',
          value: 'project_name',
          align: 'left',
          type: 'singleLine'
        },
        {
          text: 'Contractor',
          value: 'contractor',
          align: 'left',
          width: '12%',
          type: 'singleLine'
        },
        {
          text: 'Invoice Date',
          value: 'invoice_date',
          align: 'center',
          width: '10%',
          type: 'date'
        },
        {
          text: 'Reference No.',
          value: 'ref_no',
          align: 'left',
          width: '10%',
          type: 'singleLine'
        },
        {
          text: 'Invoice Amount',
          value: 'invoice_amt',
          align: 'right',
          type: 'currency'
        },
        {
          text: 'Cheque No.',
          value: 'cheque_no',
          align: 'center',
          width: '20%',
        },
        {
          text: 'Payment Amount',
          value: 'payment_amt',
          align: 'right',
          type: 'currency'
        },
      ]
    })
  ]
}

@Store('paymentHistory')
class PaymentHistory extends BaseStore {
  static mixins = [
    new ApiStore(fetchBulkPaymentsApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'Remark',
          value: 'remark',
          width: '20%',
          align: 'left',
          type: 'singleLine',
        },
        {
          text: 'Payment Date',
          value: 'payment_date',
          sortable: true,
          width: '20%',
          align: 'center',
          type: 'date',
        },
        {
          text: 'Payments',
          value: 'payment_count',
          align: 'center',
          width: '20%',
        },
        {
          text: 'Total Paid',
          sortable: true,
          value: 'total_paid',
          type: 'currency',
          align: 'center',
        },
      ]
    })
  ];

  @Nested
  summary = new PaymentHistorySummary()
}

export const paymentHistory = new PaymentHistory();
