import Vue from 'vue';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Getter} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';

export const createPaymentRecordApi = (payload) => Vue.axios.post('/api/v2/payment_records', payload);

@Store('pendingExpensesForm')
class PendingExpensesForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        bank: {
          value: '',
          label: 'Disbursement Bank',
          rules: 'required',
        },
        remark: {
          value: '',
          label: 'Remark',
          type: 'textarea',
        },
        payments: {
          value: {}
        },
      },
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Payments made successfully.', {
            color: 'success',
            closeBtn: true,
          })
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();
          apiErrorNotification(error, 'An error occurred on the server')
        },
      }
    }),
  ];

  @Getter
  totalPayments() {
    const payments = this.fields.get().payments.value;
    let total = 0;
    Object.keys(payments).forEach(key => {
      if (payments[key]) {
        total += payments[key].payment_amt
      }
    });
    return total
  }
}

export const pendingExpensesForm = new PendingExpensesForm();
