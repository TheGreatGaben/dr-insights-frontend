import {FormStore} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';

@Store('exampleNestedForm')
class ExampleNestedForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        currencies: {
          children: {
            budget: {
              value: 0,
              format: 'float',
              label: 'Budget',
              rules: 'min:0',
            },
            contingency_amt: {
              value: 0,
              format: 'float',
              label: 'Contingency Amount',
              rules: 'min:0',
            },
            omission_value: {
              value: 0,
              format: 'float',
              label: 'Omission Value',
              rules: 'min:0',
            },
          }
        },
        dates: {
          children: {
            start_date: {
              value: new Date().toISOString().substr(0, 10),
              label: 'Start Date',
              rules: 'required|before:end_date'
            },
            end_date: {
              value: new Date().toISOString().substr(0, 10),
              label: 'End Date',
              rules: 'required|after:start_date'
            },
          }
        },
        selections: {
          children: {
            client: {
              value: '',
              label: 'Client',
              rules: 'required'
            },
            project_managers: {
              value: [],
              label: 'Project Managers',
              rules: 'required',
              payloadFormatter(field) {
                const managers = field.value;
                return managers.map(pm => pm.id)
              }
            },
          }
        },
        contract_sum: {
          value: 0,
          format: 'float',
          label: 'Contract Sum',
          rules: 'required|min:0',
        },
        variation_order: {
          value: 0,
          format: 'float',
          label: 'Variation Order',
          rules: 'min:0',
        },
        end_customer: {
          value: '',
          label: 'End Customer',
        },
        retention_percent: {
          value: 10,
          format: 'float',
          label: 'Retention Percent',
          rules: 'required|numeric|max:100|min:0',
        },
        retention: {
          value: 0,
          format: 'float',
        },
        schedule: {
          value: '',
          label: 'Services',
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
      hooks: {
        onSubmitSuccess(formStore, response) {
          console.log(formStore, response);
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error, 'An error occurred on the server')
        },
      },
    })
  ];
}

export const exampleNestedForm = new ExampleNestedForm();
