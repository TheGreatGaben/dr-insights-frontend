interface TableColumn {
  text: string;
  value: string;
  align?: 'left' | 'right' | 'center';
  class?: string[] | string;
  sortable?: boolean;
  width?: string;
  tooltip?: string;
}

// TODO: not completed yet
// eslint-disable-next-line
interface TableStore {
  selectedColumns: string[];
  tableColumns: TableColumn[];
}

// eslint-disable-next-line
interface TableStoreConfig {
  tableColumns: TableColumn[];
  defaultSelectedColumns: string[];
}

// eslint-disable-next-line
interface TableStoreOptions {
  filterTableColumns?: boolean;
  serverSitePagination?: boolean;
}

// eslint-disable-next-line
interface TableProp {
  sortBy: string;
  descending: boolean;
  limit: number;
  total: string;
}
