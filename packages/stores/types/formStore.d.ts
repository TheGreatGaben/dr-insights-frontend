interface FormStoreHooks {
  onSubmitSuccess: (formStore: FormStore, response: any) => void;
  onSubmitFailed: (formStore: FormStore, error: Error) => void;
  onValidationFailed: (formStore: FormStore, fieldName: string) => void;
  onValidationSuccess: (formStore: FormStore, fieldName: string) => void;
  onFieldUpdate: (formStore: FormStore, fieldName: string) => void;
}
type HooksKeys = keyof FormStoreHooks;

interface FormStore {
  getFormDataFromFields: () => any;
  isValid: (state: any) => boolean;
  formData: () => any;
  getFields: () => any;
  getErrorMessages: () => string[];
  resetForm: () => void;
  addField: (fieldName: string) => void;
  submit: () => any;
  validateForm: () => any;
  resetValidation: () => any;
  formErrors: () => any;
  fieldConfig: () => any;
  fields: any;
  hooks: () => any;
  isPristine: () => any;
  isDirty: () => any;
}

interface FormStoreOptions {
  resetFormOnSubmitSuccess?: boolean;
  validationDebounce?: number;
  validateOnSubmit?: boolean;
  validateOnChange?: boolean;
  validateOnBlur?: boolean;
  validateOnInit?: boolean;
}

interface FieldConfig {
  readonly?: boolean;
  rules?: string;
  type?: string;
  label?: string;
  name?: string;
  options?: any;
  checked?: boolean;
  id?: number;
  placeholder?: string;
  disabled?: boolean;
  value?: any;
  defaultValue?: any;

  // field settings
  validateOnBlur?: boolean;
}
type FieldsConfig = Record<string, FieldConfig>

// noinspection JSUnusedGlobalSymbols
interface Field {
  readonly formStore: FormStore;
  readonly dirty: boolean;
  readonly pristine: boolean;
  readonly validated: boolean;
  readonly sync: {
    get: () => any;
    set: () => void;
  };
  readonly $rules: () => any;
  readonly $listeners: () => any;

  get: (key) => any;
  set: (key, value) => any;
  setValue: (value) => any;
  updateFieldOnValueChange: () => any;
  validateField: () => any;
  fieldEventsBinder: () => any;

  name: string;
  placeholder: string;
  label: string;
  value: any;
  valid: boolean;
  error: boolean;
  pending: boolean;
  changed: boolean;
  errorMessages: string[];
}
