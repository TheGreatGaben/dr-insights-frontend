/* eslint-disable */
// noinspection ES6UnusedImports
import helpers from '../src/helpers';

declare module '../src/helpers' {
  // noinspection JSUnusedGlobalSymbols
  export type CurriedState<T> = (moduleName: string) => StateInstance<T>;
  // noinspection JSUnusedGlobalSymbols
  export interface CurriedState<T> {
    default: T;
    isState: true;
    updateDefaultValue: (value) => void;
  }

  type StateInstance<T> = StateGeneralProperties<T> & StateArrayProperties<T> & StateObjectProperties

  // state's general actions
  interface StateGeneralProperties<T> {
    prefix: string;
    default: T;
    sync: Sync<T>;
    assign: (value: any) => void;
    reset: () => void;
    toggle: () => void;
  }

  // state's object actions
  interface StateObjectProperties<T> {
    get: (path?: string) => T;
    set: (path: string, value: any) => void;
    update: (values) => void;
  }

  // state's array actions
  interface StateArrayProperties {
    add: (item: any) => void;
    push: (...items: any) => void;
    unshift: (item: any) => void;
    delete: (predicate: Predicate) => void;
    update: (arg: any) => void;
    find: (predicate: Predicate) => void;
    filter: (predicate: Predicate) => void;
    findIndex: (predicate: Predicate) => void;
    indexOf: (item: any) => void;
    includes: (item: any) => void;
  }

  interface Sync<T> {
    set: (fieldName: string, value: any) => T;
    get: (fieldName: string, value: any) => void;
  }

  type Predicate = (item: any) => boolean
}





