import {FormStore} from './mixins/FormStore';
import {BaseStore} from './BaseStore';
import {ApiStore} from './mixins/ApiStore';
import {Store} from './helpers/Store';
import {Nested} from './helpers/Nested';

/**
 * Create Store
 */
@Store('create')
class CreateStore extends BaseStore {
  static mixins = [new FormStore({}), new ApiStore()];
}

/**
 * Update Store
 */
@Store('update')
class UpdateStore extends BaseStore {
  static mixins = [new FormStore({}), new ApiStore()];
}

/**
 * just planning, don't use it yet!!
 */
export class CRUDStore {
  @Nested
  create = new CreateStore();

  @Nested
  update = new UpdateStore();

  constructor(apis, dataStructure) {
    // update apiRequests
    console.log(apis);
    console.log(this.create.apiRequest);
    console.log(this.update.apiRequest);

    // dataStructure to construct fields for create/update form
    console.log(dataStructure);
    console.log(this.create.addField);
    console.log(this.update.addField);
  }
}
