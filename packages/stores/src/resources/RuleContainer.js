import {RULES} from './rules'

export class RuleContainer {
  static add(name, {validate, options, paramNames}) {
    RULES[name] = {
      validate,
      options,
      paramNames
    };
  }

  static get rules() {
    return RULES;
  }

  static has(name) {
    return !!RULES[name];
  }

  static remove(ruleName) {
    delete RULES[ruleName];
  }

  /**
   * find and trigger rule's validate method
   * @param ruleName
   * @param value
   * @param params
   * @param field
   * @param fields
   * @param formStore
   * @return {boolean}
   */
  static triggerValidatorMethod({ruleName, value, params, field, fields, formStore}) {
    const rule = RULES[ruleName];
    if (!rule) {
      throw Error(`"${ruleName}" rule in "${field.name}" field not available in "${formStore.moduleName}" store`)
    }

    if (!rule.validate) {
      throw Error(`"${ruleName}" does not have validate method. Please add.`)
    }

    // ignore all validators if value is empty
    if (ruleName !== 'required' && ['', undefined, null].includes(value)) {
      return true
    }

    return rule.validate({value, params, formStore, field, fields});
  }

  static getValidatorErrorMessage({ruleName, params, field, formStore}) {
    return RULES[ruleName] ? RULES[ruleName].getMessage(field, ...params) : undefined;
  }
}
