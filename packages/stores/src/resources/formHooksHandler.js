/**
 *
 * @param {HooksKeys} hookName
 * @param formStore
 * @param {object|string|boolean|number} args
 * @return {void}
 */
export function formHooksHandler(hookName, formStore, args) {
  const hook = formStore.hooks[hookName];

  if (typeof hook === 'function') {
    hook(formStore, args)
  }
}
