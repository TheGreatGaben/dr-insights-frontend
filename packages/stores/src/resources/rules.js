import {isEmpty, isNull, isNaN, clone} from 'lodash'
import {dateFormatValid} from '@nixel/base/filters/datetime'
import validator from 'validator'

function defaultGetMessage(field) {
  return `${field.label || field.name} is not valid.`
}

export const RULES = {
  email: {
    validate({value}) {
      return validator.isEmail(value)
    },
    getMessage(field) {
      // TODO: fix validation error
      console.log(field);
      return `${field.label || field.name} is not a valid email.`
    },
  },
  is: {
    validate({value, params, fields}) {
      const anotherFieldName = params[0];
      const anotherField = fields[anotherFieldName];

      return value === anotherField.value
    },
    getMessage: defaultGetMessage,
  },
  required: {
    validate({value}) {
      const cloneValue = clone(value);
      // Array is also an Object !?!!?!?!?
      // yup it is, everything is object in JS
      if (cloneValue instanceof Object) {
        return !isEmpty(cloneValue)
      }
      return !isNull(cloneValue) && !!cloneValue
    },
    getMessage(field) {
      return `${field.label || field.name} is required.`
    },
  },
  minLength: {
    validate({value, params}) {
      const minSize = parseInt(params[0]);

      return value.length >= minSize
    },
    getMessage(field, minSize) {
      return `${field.label || field.name} field must be at least or equals to ${minSize} characters.`
    },
  },
  min: {
    validate({value, params}) {
      const minSize = parseInt(params[0]);
      const floatValue = parseFloat(value);
      const intValue = parseInt(value);

      if (!isNaN(floatValue) || !isNaN(intValue)) {
        return floatValue >= minSize || intValue >= minSize
      }
      return value.length >= minSize
    },
    getMessage(field, minSize) {
      const floatValue = parseFloat(field.value);
      const intValue = parseInt(field.value);
      if (!isNaN(floatValue) || !isNaN(intValue)) {
        return `${field.label || field.name} field must not be negative.`
      }
      return `${field.label || field.name} field must be at least or equals to ${minSize} characters.`
    },
  },
  max: {
    validate({value, params}) {
      const cloneValue = clone(value);
      const maxSize = params[0];
      const floatValue = parseFloat(cloneValue);
      const intValue = parseInt(cloneValue);

      if (!isNaN(floatValue) || !isNaN(intValue)) {
        return floatValue <= maxSize || intValue <= maxSize
      }
      return cloneValue.length <= maxSize
    },
    getMessage(field, maxSize) {
      const floatValue = parseFloat(field.value);
      const intValue = parseInt(field.value);

      if (!isNaN(floatValue) || !isNaN(intValue)) {
        return `${field.label || field.name} field must be less than or equals to ${maxSize}.`
      }

      return `${field.label || field.name} field must be less than or equals to ${maxSize} characters.`
    },
  },
  tel: {
    validate({value}) {
      const telephoneRegExPattern = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{3,7}$/im;
      return telephoneRegExPattern.test(value)
    },
    getMessage(field) {
      return `${field.label || field.name} field must be a valid telephone number.`
    },
  },
  before: {
    validate({value, params, fields}) {
      const anotherDateFieldName = params[0];
      const anotherDateField = fields[anotherDateFieldName];

      if (!anotherDateField) {
        return `Field "${anotherDateFieldName}" is not defined`;
      } else if(!anotherDateField.value) {
        // ignore validation if another field is `undefined`
        return true;
      }

      const dateA = new Date(value);
      const dateB = new Date(anotherDateField.value);
      return dateA < dateB
    },
    getMessage: defaultGetMessage,
  },
  after: {
    validate({value, params, fields}) {
      const anotherDateFieldName = params[0];
      const anotherDateField = fields[anotherDateFieldName];

      // ignore validation if another field is `undefined`
      if (!anotherDateField) {
        return `Field "${anotherDateFieldName}" is not defined`;
      } else if(!anotherDateField.value) {
        // ignore validation if another field is `undefined`
        return true;
      }

      const dateA = new Date(value);
      const dateB = new Date(anotherDateField.value);
      return dateA > dateB
    },
    getMessage: defaultGetMessage,
  },
  integer: {
    validate({value}) {
      return !isNaN(parseInt(value))
    },
    getMessage(field) {
      return `${field.label || field.name} must only consists of numbers.`
    },
  },
  numeric: {
    validate({value}) {
      const testValue = clone(value);
      return !isNaN(parseInt(testValue)) || !isNaN(parseFloat(testValue))
    },
    getMessage(field) {
      return `${field.label || field.name} must only consists of numbers.`
    },
  },
  date_format: {
    validate({value, params}) {
      return dateFormatValid(value);
    },
    getMessage(field) {
      return `${field.label || field.name} must be in YYYY-MM-DD format.`
    },
  },
  gte: {
    validate({value, params}) {
      const cloneValue = clone(value);
      const valueCheck = params[0];
      const floatValue = parseFloat(cloneValue);
      const intValue = parseInt(cloneValue);

      if (!isNaN(floatValue) || !isNaN(intValue)) {
        return floatValue >= valueCheck || intValue >= valueCheck;
      }
      return cloneValue.length >= valueCheck;
    },
    getMessage(field, valueCheck) {
      const floatValue = parseFloat(field.value);
      const intValue = parseInt(field.value);

      if (!isNaN(floatValue) || !isNaN(intValue)) {
        return `${field.label || field.name} field must be less than or equals to ${valueCheck}.`
      }

      return `${field.label || field.name} field must be less than or equals to ${valueCheck} characters.`
    },
  }
};
