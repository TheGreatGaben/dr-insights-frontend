import {clone, reduce, debounce} from 'lodash';
import {RuleContainer} from './RuleContainer';
import {normalizeRules} from '../../utils';
import {formHooksHandler} from './formHooksHandler';

export class Field {
  /**
   * pass in new value and fields
   * @param {*} value // could be from field object could be value to be updated
   * @param {Field} field
   * @param {FormStore} formStore
   * @return Partial<Field>
   */
  static updateFieldOnValueChange(value, field, formStore) {
    const errorMessage = Field.validateField(value, field, formStore.getFields(), formStore);

    let formattedDefaultValue;
    if (field.defaultValue) {
      formattedDefaultValue = field.formatter({...field, value: field.defaultValue})
    }
    const formattedValue = field.formatter({...field, value});

    if (errorMessage) {
      return {
        value: formattedValue,
        error: true,
        valid: false,
        errorMessages: [errorMessage],
        dirty: formattedDefaultValue !== formattedValue,
        pristine: formattedDefaultValue === formattedValue,
        validated: true,
      }
    } else {
      return {
        value: formattedValue,
        error: false,
        valid: true,
        errorMessages: null,
        dirty: formattedDefaultValue !== formattedValue,
        pristine: formattedDefaultValue === formattedValue,
        validated: true,
      }
    }
  }

  /**
   * Validate field using field.rules, validators are defined in `utils/rules`
   * @param {*} value // could be from field object could be value to be updated
   * @param {Field} field
   * @param {Record<string, Field>} allFields
   * @param {FormStore} formStore
   * @return {string} errorMessage
   */
  static validateField(value, field, allFields, formStore) {
    return reduce(field.$rules, (result, params, key) => {
      if (result) {
        // make sure only trigger 1 validator, and skip the rest
        return result
      }

      const payloadForValidator = {
        ruleName: key,
        params,
        field,
        value,
        fields: allFields,
        formStore
      };

      // call validate method from rule
      const isValid = RuleContainer.triggerValidatorMethod(payloadForValidator);

      // if validator return false, return rule's error message
      if (!isValid) {
        return RuleContainer.getValidatorErrorMessage(payloadForValidator)
      }
    }, undefined)
  }

  _inputFn;
  /**
   * Field events binder
   * pass in field object, it will return what input field events component needs
   * @param {string} fieldName
   * @param {Field} field
   */
  static fieldEventsBinder(fieldName, field) {
    let valueCache = field.value;

    return function() {
      const formStore = this;

      if (!field._inputFn) {
        field._inputFn = debounce(function(value) {
          // check has value changed, don't update if didn't change
          const hasValueChanged = value !== valueCache;
          // update cache
          valueCache = value;

          if (!field.validateOnBlur && hasValueChanged) {
            Field.fieldOnInput(value, field, formStore, fieldName);
          }
        }, (formStore.formOptions.validationDebounce || 300));
      }

      // noinspection JSUnusedGlobalSymbols
      return {
        change(value) {
          Field.fieldOnChange(value, field, formStore, fieldName);
        },
        input: field._inputFn,
      }
    }
  }

  static fieldOnInput(value, field, formStore, fieldName) {
    const dataToChange = Field.updateFieldOnValueChange(value, field, formStore);
    formStore.fields.set(fieldName, dataToChange);

    if (dataToChange.error) {
      // trigger onValidationFailed hook
      formHooksHandler('onValidationFailed', formStore, fieldName);
    } else {
      // trigger onValidationSuccess hook
      formHooksHandler('onValidationSuccess', formStore, fieldName);
    }
  }

  static fieldOnChange(value, field, formStore, fieldName) {
    const dataToChange = Field.updateFieldOnValueChange(value, field, formStore);
    formStore.fields.set(fieldName, dataToChange);

    if (dataToChange.error) {
      // trigger onValidationFailed hook
      formHooksHandler('onValidationFailed', formStore, fieldName);
    } else {
      // trigger onValidationSuccess hook
      formHooksHandler('onValidationSuccess', formStore, fieldName);
    }
  }

  name = undefined;
  formStore = undefined;
  placeholder = undefined;
  label = undefined;
  value = '';
  type = 'text';
  defaultValue = '';

  format = undefined; // primary type - int | float
  payloadFormatter;

  // noinspection JSMethodCanBeStatic
  formatter(field) {
    const value = field.value;
    if (field.format === 'float') {
      const float = parseFloat(value);
      return !isNaN(float) ? float : 0
    } else if (field.format === 'int' || field.format === 'integer') {
      const int = parseInt(value);
      return !isNaN(int) ? int : 0
    }

    return value
  }

  // default field states
  dirty = false;
  pristine = true;
  valid = false;
  error = false;
  validated = false;
  loading = false;
  changed = false;
  errorMessages = [];

  $rules = {};
  $listeners;

  // settings
  validateOnBlur = false;

  /**
   * @param {FieldConfig} field
   * @param formStore
   * @return {Field}
   */
  constructor(field, formStore) {
    if (!field) {
      throw Error('Field config is not provided.')
    }

    this.formStore = formStore;
    this.validateOnBlur = field.validateOnBlur || false;

    // init field states from FieldConfig
    this.name = field.name;
    this.readonly = field.readonly;
    this.rules = field.rules;
    this.type = field.type;
    this.label = (field.rules && field.rules.includes('required')) ? `* ${field.label}`: field.label;
    this.name = field.name;
    this.options = field.options;
    this.checked = typeof field.defaultValue === 'boolean' ? field.defaultValue : false;
    this.id = field.id;
    this.placeholder = field.placeholder;
    this.disabled = field.disabled;
    this.value = field.value;
    this.defaultValue = clone(field.defaultValue || field.value || field.checked);
    this.format = field.format;
    this.formatter = field.formatter || this.formatter;
    this.payloadFormatter = field.payloadFormatter;

    // get any value from Field
    this.get = function(key) {
      // the scope in the function is the scope of the formStore
      // due to BaseStore.alterStateScope
      const formStore = this;

      return formStore.fields.get(`${field.name}.${key}`)
    };

    // set any value from Field
    this.set = formStore.makeSetField(field.name);

    // set value triggers field validation at the same time
    this.setValue = formStore.makeSetFieldValue(field.name);

    // additional states
    this.$rules = normalizeRules(field.rules);
    this.$listeners = Field.fieldEventsBinder(field.name, this)
  }
}
