import {debounce} from 'lodash';
import {ADD_REGISTERED_STORES, REMOVE_REGISTERED_STORES, STORE_NAME} from '../consts/stores';

let registerQueueMessages = [];
let disposeQueueMessages = [];

const storeLogger = (actionLabel, messages, backgroundColor, textColor) => {
  console.log(
    `%c ${actionLabel}: %c\n${messages.join('\n')} %c `,
    'background:#35495e ; padding: 1px; border-radius: 3px;  color: #fff',
    `background: ${backgroundColor}; padding: 2px; border-radius: 3px;  color: ${textColor}`,
    'background:transparent'
  );
};

const registerLogger = debounce(() => {
  const backgroundColor = '#ddeec9';
  const textColor = '#141414';
  const actionLabel = 'Register';
  const messages = registerQueueMessages;

  storeLogger(actionLabel, messages, backgroundColor, textColor);

  registerQueueMessages = []
}, 500);

const disposeLogger = debounce(() => {
  const backgroundColor = '#ef9a9a';
  const textColor = '#fff';
  const actionLabel = 'Dispose';
  const messages = disposeQueueMessages;

  storeLogger(actionLabel, messages, backgroundColor, textColor);

  disposeQueueMessages = []
}, 500);

export class StoreManager {
  store;

  static toast(message, isRegister) {
    if (isRegister) {
      registerQueueMessages.push(message);
      registerLogger();
    } else {
      disposeQueueMessages.push(message);
      disposeLogger();
    }
  }

  constructor(store) {
    this.store = store
  }

  /**
   * Register store module
   * @param {BaseStore} storeConstructor
   * @param {ModuleOptions?} registerOptions
   * @return {function} storeDisposer
   */
  registerStore(storeConstructor, registerOptions) {
    const moduleName = storeConstructor.moduleName;
    const storeName = storeConstructor.storeName;

    if (this.storeExist(moduleName)) {
      return
    }

    this.store.commit(ADD_REGISTERED_STORES, moduleName);

    // initialize store
    const initializedStore = storeConstructor.build();
    storeConstructor._isRegistered = true;

    // inject app instance
    storeConstructor._vm = this.store._vm;
    this.store.registerModule(moduleName, initializedStore, registerOptions);

    // trigger `onStoreRegistered` event
    storeConstructor.triggerStoreHooks('onStoreRegistered');

    // initialize nested store
    this.registerNestedModules(storeConstructor);

    if (!['prod', 'production', 'uat', 'testing'].includes(process.env.NODE_ENV)) {
      StoreManager.toast(storeName, true);

      if (!storeConstructor.isNested) {
        window[storeName] = storeConstructor;
      }
    }

    const manager = this;
    return async function disposer() {
      if (!manager.store.state[moduleName]) {
        return
      }

      // reset store before unregister it
      await storeConstructor.resetAll(true);

      // unregisterStore
      manager.unregisterStore(storeConstructor, moduleName);

      // dispose nested store
      storeConstructor._nestedDisposer.forEach(nestedDisposer => nestedDisposer && nestedDisposer());

      // empty the nested list
      storeConstructor._nestedDisposer = [];
      storeConstructor._nestedModules = []
    };
  }

  /**
   * remove store module by passing in moduleName
   * @param storeConstructor
   * @param moduleName
   */
  unregisterStore(storeConstructor, moduleName) {
    if (!['prod', 'production', 'uat', 'testing'].includes(process.env.NODE_ENV)) {
      StoreManager.toast(storeConstructor.storeName);

      delete window[moduleName];
    }

    this.store.commit(REMOVE_REGISTERED_STORES, moduleName);

    // set `_isRegister` as false
    storeConstructor._isRegistered = false;
    this.store.unregisterModule(moduleName);
  }

  /**
   * check if store registered?
   * @param storeName
   * @return {boolean}
   */
  storeExist(storeName) {
    return (this.store.state[STORE_NAME].registeredStores || []).includes(storeName);
  }

  /**
   * register nested module use by `registerModule`
   * @param parentStoreConstructor
   */
  registerNestedModules(parentStoreConstructor) {
    parentStoreConstructor._nestedModules.forEach(moduleName => {
      const storeDisposer = this.registerStore(parentStoreConstructor[moduleName]);

      // parentStoreConstructor.
      parentStoreConstructor._nestedDisposer.push(storeDisposer);
    });
  }

}
