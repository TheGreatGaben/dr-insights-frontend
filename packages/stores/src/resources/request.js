import {get} from 'lodash';
import {STORE_REMOVED_MESSAGE} from '../consts/stores';


/**
 * handle errors created by Store that are not related to the api response
 * return true if is store error
 * @param {*} error
 * @return {boolean}
 */
export function isStoreError(error) {
  const errorResponse = get(error, 'response')
  if (!error) {
    return false;
  }

  // validation error from FormStore
  if (error.validationErrors) {
    return true;
  }

  // is loading from FormStore
  if (error.isLoading) {
    return true;
  }

  // ignore if error messages
  return [STORE_REMOVED_MESSAGE].includes(errorResponse || error);
}
