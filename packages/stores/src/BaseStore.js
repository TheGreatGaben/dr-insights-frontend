import shortid from 'shortid';
import {each, keys, transform} from 'lodash';
// TODO: remove vuex-dry
import {$commit, Module} from 'vuex-dry';
import {asyncForEach, makePrivateProperty} from '../utils';

const initStores = [];

const OMITTED_PROPERTIES = [
  'storeName',
  'moduleName',
  'mixins',
  'isNested',
  'resetAll',
  '_isRegistered',
  '_hasAttached',
  '_target',
  '_properties',
  '_states',
  '_getters',
  '_actions',
  '_nestedModules',
  '_nestedDisposer',
  'build',
  'constructor',
  '_vm',
  '__ob__',
  '__obj__',
  '__proto__',
  '__defineGetter__',
  '__defineSetter__',
  'hasOwnProperty',
  '__lookupGetter__',
  '__lookupSetter__',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toString',
  'valueOf',
  'toLocaleString',
];

const PRIVATE_PROPERTIES = [
  '_vm',
  '_isRegistered',
  '_hasAttached',
  '_target',
  '_properties',
  '_states',
  '_getters',
  '_actions',
  '_nestedModules',
  '_nestedDisposer',
];

// Caveats: don't add States or Actions here, use mixins instead
// To avoid inheritance madness
export class BaseStore {
  static mixins = [];

  static updateState(store, args) {
    // update any states from TableStore
    each(args, (values, key) => {
      if (store[key] && store[key].updateDefaultValue) {
        store[key].updateDefaultValue(values);
      }
    });
  }

  /**
   * to detect of state is sync state
   * duck test checks if it's sync
   * @param state
   * @return {boolean}
   */
  static isSyncState(state) {
    try {
      const hasTwoProperties = Object.keys(state).length === 2;

      return hasTwoProperties && typeof state.set === 'function' && typeof state.get === 'function';
    } catch (e) {
      return false;
    }
  }

  static moduleName;
  storeName;
  moduleName;
  storeHooks;
  resetAll() {}
  _vm; // Vue instance
  isNested = false;
  _isRegistered = false; // to keep track on the store whether is attached to global store
  _hasAttached = false; // to keep track on nested store is attached to base store
  _target;
  _properties = new Map();
  _states = {};
  _getters = {};
  _actions = {};
  _nestedModules = [];
  _nestedDisposer = [];
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    this._target = new.target;
    this.isNested = this._target.isNested;
    this.storeName = this._target.moduleName;
    this.moduleName = this._target.moduleName + '_' + shortid.generate();

    this.makePrivateProperties();

    // register mixin first
    this.registerMixinsProperties();
  }

  build(buildConfig = {}) {
    const buildConfigState = buildConfig && buildConfig.state ? buildConfig.state() : {};

    // register states, actions and getters
    this.registerPrivateProperties()
      .registerStates()
      .registerActions()
      .registerGetters()
      .registerModules();

    const initialStates = {
      ...this._states,
      ...buildConfigState,
    };

    // @event onInit
    this.triggerStoreHooks('onInit');

    return Module.build({
      ...buildConfig,
      config: {
        ...buildConfig.config,
        /**
         * register to `vuex-dry` to make object state less strict
         * @ref: https://github.com/eunjae-lee/vuex-dry/blob/master/DOCUMENT.md#non-strict-object
         */
        nonStrictObject: keys(initialStates),
      },
      getters: {
        ...this._getters,
        ...buildConfig.getters,
      },
      mutations: buildConfig.mutations,
      state() {
        return {
          ...initialStates,
        };
      },
      actions: {
        ...this._actions,
        ...buildConfig.actions,
        $resetAll: this.resetAll,
      },
    });
  }

  registerPrivateProperties() {
    each(
      this.filterProperties((item, key) => PRIVATE_PROPERTIES.includes(key)),
      async (item, key) => {
        // add useful private properties into store
        this[key] = item;
      }
    );

    return this;
  }

  registerStates() {
    each(this.filterProperties(item => item.isState), async (item, key) => {
      // set curried to true, to throw error if State is called before curried
      item.curried = true;

      // overwrite existing property with state APIs
      // turn curriedState to exact state
      this[key] = item(this.moduleName);

      this._states[key] = this.alterStateScope(this[key].default);
    });

    return this;
  }

  registerGetters() {
    each(this.filterProperties(item => item.isGetter), (item, key) => {
      // set curried to true, to throw error if Getter is called before curried
      item.curried = true;

      // overwrite existing property with state APIs
      // turn curriedState to getter function
      this[key] = item(this.moduleName);
      this._getters[key] = this[key].getter.bind(this);
    });

    return this;
  }

  registerActions() {
    const context = this;
    const moduleName = this.moduleName;

    each(this.filterProperties(item => item.isAction), (item, key) => {
      // set curried to true, to throw error if Action is called before curried
      item.curried = true;

      // overwrite existing property with exact action function
      // turn curriedAction to action
      this[key] = item(moduleName);
      // console.log(key, this, this)
      this._actions[key] = this[key].bind(this);
    });

    // register module action resetAll
    this.resetAll = async function(recursive = false) {
      $commit(`${moduleName}/$resetAll`);

      if (recursive) {
        await asyncForEach(context._nestedModules, nestedModuleName => {
          $commit(`${context[nestedModuleName].moduleName}/$resetAll`);
        });

        return Promise.resolve();
      } else {
        return Promise.resolve();
      }
    };

    return this;
  }

  registerModules() {
    const parentModuleName = this.moduleName;
    each(this.filterProperties(property => property.isNested), (item, key) => {
      if (!item._hasAttached) {
        item.moduleName = `${parentModuleName}/${item.moduleName}`;
        item._hasAttached = true;
      }

      // add into _nestedModules, `registerStore` function will loop thru the list and register the nested stores
      this._nestedModules.push(key);
    });

    return this;
  }

  filterProperties(predicate = () => true, fetchNewList = false) {
    const returnList = {};

    if (this._properties.size === 0 || fetchNewList) {
      this._properties.clear();
      let storeObject = this;

      do {
        Object.getOwnPropertyNames(storeObject).map(key => {
          if (!OMITTED_PROPERTIES.includes(key)) {
            // register existing properties to store
            this._properties.set(key, storeObject[key]);
          }
        });
      } while ((storeObject = Object.getPrototypeOf(storeObject)));
    }

    this._properties.forEach((item, key) => {
      if (item && predicate(item, key)) {
        returnList[key] = item;
      }
    });

    return returnList;
  }

  registerMixinsProperties() {
    this._target.mixins.forEach(extendedStore => {
      let mixinExtendStore = extendedStore;

      do {
        Object.getOwnPropertyNames(mixinExtendStore).map(key => {
          if (!OMITTED_PROPERTIES.includes(key)) {
            // register mixins properties to store
            // it will replace existing states from base store
            this[key] = extendedStore[key];
          }
        });
      } while ((mixinExtendStore = Object.getPrototypeOf(mixinExtendStore)));
    });
  }

  /**
   * trigger store's hook, even for all nested and mixin store hooks
   * @param {string} hookName
   * @param args
   */
  triggerStoreHooks(hookName, ...args) {
    if (this.storeHooks && this.storeHooks[hookName]) {
      this.storeHooks[hookName].call(this, ...args);
    }

    this._target.mixins &&
      this._target.mixins.map(module => {
        const constructor = module.constructor;

        if (constructor.storeHooks && constructor.storeHooks[hookName]) {
          constructor.storeHooks[hookName].call(this, ...args);
        }
      });

    this._nestedModules.map(module => {
      const constructor = module.constructor;

      if (constructor.storeHooks && constructor.storeHooks[hookName]) {
        constructor.storeHooks[hookName].call(this, ...args);
      }
    });
  }

  /**
   * make sure state's functions are bind to base store scope
   * @param state
   * @return {*}
   */
  alterStateScope(state) {
    // avoid sync states, due to hard bind functions won't work in Vue component
    if (typeof state === 'object' && !BaseStore.isSyncState(state)) {
      return transform(
        state,
        (returnState, value, key) => {
          if (typeof value === 'function') {
            returnState[key] = returnState[key].bind(this);
          }

          if (typeof value === 'object') {
            returnState[key] = this.alterStateScope(value);
          }
        },
        state
      );
    }

    return state;
  }

  makePrivateProperties() {
    // noinspection JSMismatchedCollectionQueryUpdate
    const privateProperties = [
      '_hasAttached',
      '_target',
      '_properties',
      '_states',
      '_getters',
      '_actions',
      '_nestedModules',
      '_nestedDisposer',
    ];

    privateProperties.map(propName => {
      makePrivateProperty(this, propName);
    });
  }
}
