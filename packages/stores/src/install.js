import {isBoolean, each} from 'lodash';
import Vue from 'vue';
import {plugin, $get, $action, $commit} from 'vuex-dry';

import {updateEnv} from './consts/env';
import {
  defaultFilterQuery,
  defaultPaginationQuery,
  ADD_REGISTERED_STORES,
  REMOVE_REGISTERED_STORES,
  STORE_NAME,
} from './consts/stores';
import StoreInjector from './components/StoreInjector';
import {StoreManager} from './resources/StoreManager';
import {isDevelopment} from 'pce-admin/src/utils';

/**
 * add `StoreInjector` component and update store's default options
 * @param defaultPaginationOptions?
 * @param defaultFilterOptions?
 * @param isProduction?
 * @param apiStore - API store default options
 */
export const storesInstaller = ({defaultPaginationOptions, defaultFilterOptions, isProduction}) => {
  // install components
  Vue.component('StoreInjector', StoreInjector);

  if (defaultFilterOptions) {
    defaultFilterQuery = defaultFilterOptions;
  }

  if (defaultPaginationOptions) {
    defaultPaginationQuery = defaultPaginationOptions;
  }

  // stores comes with environment check, update if it's different from it.
  if (isBoolean(isProduction)) {
    updateEnv(isProduction);
  }
};

/**
 *
 * @param {Store<any>} store
 */
export const vuexPlugin = (store) => {
  // add `registeredStores` states and it's mutations
  store.registerModule(STORE_NAME, {
    state() {
      return {
        registeredStores: []
      }
    },
    mutations: {
      [ADD_REGISTERED_STORES]: (state, storeName) => {
        state.registeredStores.push(storeName);
      },
      [REMOVE_REGISTERED_STORES]: (state, storeName) => {
        const index = state.registeredStores.indexOf(storeName);
        if (index > -1) {
          state.registeredStores.splice(index, 1);
        }
      }
    }
  })

  // init vuex-dry plugin
  plugin(store);

  // init storeManager
  const storeManager = new StoreManager(store);
  const modules = store._modules.root._rawModule.modules;

  // register all initial `BaseStore` using storeManager
  each(modules, (module, key) => {
    // if it's module from `BaseStore`
    if (module.moduleName && module.storeName) {
      // remove existing module
      delete store.state[key];

      // register it with storeManager
      storeManager.registerStore(module);
    }
  });

  // add in Vue instances
  // noinspection JSUnusedGlobalSymbols
  Vue.prototype.$storeManager = storeManager;

  if (isDevelopment) {
    window.store = store;
    window.$get = $get;
    window.$action = $action;
    window.$commit = $commit;
  }
};

