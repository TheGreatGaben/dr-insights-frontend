import {get, isEmpty, omit} from 'lodash';
import {Action} from '../helpers/Action';
import {Getter} from '../helpers/Getter';
import {State} from '../helpers/State';
import {STORE_REMOVED_MESSAGE} from '../consts/stores';
import {isDevelopment} from '../consts/env';

function defaultDataFormatter(response) {
  return response.data.data ? response.data.data : response.data
}

export class ApiStore {
  constructor(apiRequest, defaultValue, dataFormatter = defaultDataFormatter) {
    this.apiRequest = apiRequest;
    this.dataFormatter = dataFormatter;
    this.data.updateDefaultValue(defaultValue)
  }

  dataFormatter;

  apiRequest;

  @State
  apiResponse = undefined;

  @State
  isLoading = false;

  @State
  isRequested = false;

  @State
  requestError = {};

  @State
  requestStatus = undefined;

  @State
  data = undefined;

  @State
  query = {};

  @Getter
  pagination(state) {
    return omit(state.apiResponse, ['data'])
  }

  @Getter
  isPaginated(state) {
    return Array.isArray(state.data) && state.query.page;
  }

  @Getter
  size(state) {
    if (Array.isArray(state.data)) {
      return state.data.length
    }

    return 0
  };

  @Getter
  hasError(state) {
    return !isEmpty(state.requestError)
  };

  /**
   * A function to delete an item in data (mostly use for list / Array data)
   * @param predicate - use it like using filter
   */
  @Action
  deleteItems(predicate) {
    if (Array.isArray(this.data.get())) {
      this.data.delete(predicate)
    } else {
      throw Error('deleteItems supports array only at the moment.');
    }
  }

  updateSortingFromResponse(response) {
    const currentSorting = get(response, 'data.current_sorting');
    if (currentSorting) {
      const currentQuery = this.query.get() || {};
      const newQuery = {};

      if (currentSorting.sort !== currentQuery.sort) {
        newQuery.sort = currentSorting.sort;
      }

      if (currentSorting.order !== currentQuery.order) {
        newQuery.order = currentSorting.order;
      }

      this.query.update(newQuery);
    }
  }

  async fetchDataWithoutUpdateStore(payload = this.query.get()) {
    if (!this.apiRequest) {
      console.log('API not provided.');
      return Promise.reject();
    }

    try {
      // get API
      const api = this.apiRequest;
      const dataFormatter = this.dataFormatter;

      // call API
      const response = await api(payload);

      return Promise.resolve(dataFormatter(response))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  @Action
  async fetchData(payload) {
    if (this.apiRequest) {
      try {
        this.isLoading.assign(true);
        this.isRequested.assign(true);

        if (payload) {
          this.query.assign(payload)
        }

        // get API
        const api = this.apiRequest;
        const dataFormatter = this.dataFormatter;

        // call API
        const response = await api(this.query.get());

        // update current API sorting
        this.updateSortingFromResponse(response);

        // noinspection JSUnresolvedVariable
        if (!this._isRegistered) {

          if (isDevelopment) {
            console.log(STORE_REMOVED_MESSAGE);
          }
          return Promise.reject(STORE_REMOVED_MESSAGE)
        }

        // assign API response to states
        this.requestStatus.assign(response.status);
        this.apiResponse.assign(response.data);
        const formattedResponse = dataFormatter(response);
        this.data.assign(formattedResponse);

        return Promise.resolve(response)
      } catch (e) {
        this.requestError.assign(e ? e.response : undefined);
        return Promise.reject(e)
      } finally {
        // noinspection JSUnresolvedVariable
        if (this._isRegistered) {
          this.isLoading.assign(false)
        }
      }
    } else {
      console.log('API not provided.');
      return Promise.resolve();
    }
  }
}

