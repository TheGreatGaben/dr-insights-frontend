import {clone, get, transform, mapValues, reduce} from 'lodash';
import {State} from '../helpers/State';
import {Action} from '../helpers/Action';
import {Getter} from '../helpers/Getter';
import {Field} from '../resources/Field';
import {formHooksHandler} from '../resources/formHooksHandler';
import {BaseStore} from '../BaseStore';
import {delay} from '../../utils';
/**
 * FormStore
 * Usually use it with ApiStore, so it will automatically calls API
 */
export class FormStore {
  /**
   * Get form data from fields object or array
   * @param fields
   * @return {*}
   */
  static getFormDataFromFields(fields) {
    return transform(fields, (formData, field, key) => {
      if (field.payloadFormatter) {
        formData[field.name || key] = field.payloadFormatter(field)
      } else {
        formData[field.name || key] = field.value
      }
    }, {})
  }

  /**
   * @param {Record<string, FieldConfig>} fields
   * @param {Partial<FormStoreHooks>?} hooks
   * @param {Partial<FormStoreOptions>?} options
   * @param args
   */
  constructor({fields, hooks, options, ...args}) {
    this.fieldConfig = fields;

    // update field list
    this.fields.updateDefaultValue(
      mapValues(fields, (field, name) => {
        field.name = field.name || name;

        return new Field(field, this)
      })
    );

    // add hooks in the store
    this.hooks = hooks || {};

    // merge options with default options in the store
    this.formOptions = {
      ...this.formOptions,
      ...options
    };

    // update states' default value from FormStore's constructor
    BaseStore.updateState(this, args)
  }

  /**
   * Field config on FormStore initialise
   * @type {Record<string, FieldsConfig>}
   */
  fieldConfig = {};

  /**
   * @type {FormStoreOptions}
   */
  formOptions = {
    resetFormOnSubmitSuccess: false,
    validationDebounce: 300,
    validateOnSubmit: true,
    validateOnChange: true,
    validateOnBlur: true,
    validateOnInit: false,
  };

  /**
   * @return {StateInstance<Record<string, Field>>}
   */
  @State
  fields = {};

  hooks = {};

  @Getter
  isPristine(state) {
    return reduce(state.fields, (result, field) => {
      if (field.dirty) {
        return false
      }

      return result
    }, true)
  }

  @Getter
  isDirty(state) {
    return reduce(state.fields, (result, field) => {
      if (field.dirty) {
        return true
      }

      return result
    }, false)

  }

  /**
   * @return {Record<string, string[]>}
   */
  @Getter
  formErrors(state) {
    return transform(state.fields, (result, field, key) => {
      if (field.error) {
        result[key] = field.errorMessages
      }

    }, {})
  };

  @Getter
  isValid(state) {
    return reduce(state.fields, (result, field) => {
      if (field.error) {
        return false
      }

      return result
    }, true)
  }

  /**
   * FormData is form values that are ready to submit to backend
   * all values will be formatted by Field's `payloadFormatter`
   * @return {*}
   */
  @Getter
  formData() {
    return FormStore.getFormDataFromFields(this.fields.get())
  };

  @Getter
  getFields(state) {
    return state.fields
  };

  @Getter
  getErrorMessages(state) {
    return reduce(state.fields, (errorFields, field) => {
      if (field.errorMessages && field.errorMessages.length > 0) {
        errorFields[field.name] = field.errorMessages
      }

      return errorFields;
    }, {})
  };

  /**
   * If resetAll is enabled, `defaultValue` will be reset
   * @param {boolean} resetAll
   */
  @Action
  resetForm(resetAll = false) {
    const fields = this.getFields();

    const updatedFields = mapValues(fields, field => {
      let defaultValue = field.defaultValue

      // if resetAll is enabled, resetAll `defaultValue`
      if (resetAll) {
        defaultValue = this.fieldConfig[field.name].value
      }

      const newField = {
        ...field,
        checked: typeof defaultValue === 'boolean' ? defaultValue : false,
        defaultValue,
        value: clone(defaultValue) || this.fieldConfig[field.name].value,
        errorMessages: [],
        error: undefined,
        hasError: false,
        pristine: true,
        dirty: false,
        isValid: true,
        disabled: false,
        readonly: false,
      };

      // update value using formatter
      newField.value = field.formatter(newField);
      return newField
    });

    this.fields.assign(updatedFields)
  }

  /**
   * add a field
   * @param {FieldConfig} fieldConfig
   */
  @Action
  addField(fieldConfig) {
    const newField = new Field(fieldConfig, this);

    this.fields.set(newField.name, newField)
  }

  /**
   * Submit form
   * @return {Promise<Promise<any>|undefined>}
   */
  @Action
  async submit(payload = {}) {
    if (this.isLoading && this.isLoading.get()) {
      return Promise.reject({
        isLoading: true,
        message: 'Is loading.'
      });
    }
    // start change loading state first
    this.isLoading && this.isLoading.assign(true);

    // put delays due to input validation might have delay on update formData
    await delay(this.formOptions.validationDebounce || 300);

    const formStore = this;
    const formData = formStore.formData();
    const validateOnFormSubmit = formStore.formOptions.validateOnSubmit;

    if (validateOnFormSubmit) {
      const formIsValid = await formStore.validateForm();

      if (!formIsValid) {
        // turn off isLoading state
        this.isLoading && this.isLoading.assign(false);

        return Promise.reject({
          validationErrors: this.getErrorMessages()
        })
      }
    }

    if (formStore.fetchData) {
      try {
        const response = await formStore.fetchData({...formData, ...payload});

        // trigger onSubmitSuccess hook
        formHooksHandler('onSubmitSuccess', formStore, response);
        // reset form
        if (formStore.formOptions.resetFormOnSubmitSuccess) {
          this.resetForm()
        }

        return Promise.resolve(response)
      } catch (error) {
        // trigger onSubmitFailed hook
        formHooksHandler('onSubmitFailed', formStore, error);
        return Promise.reject(error)
      }
    } else {
      // trigger onSubmitSuccess hook
      formHooksHandler('onSubmitSuccess', formStore, true);

      // turn off isLoading state
      this.isLoading && this.isLoading.assign(false);
    }
  }

  /**
   * Validate all fields
   * @return {Promise<void>}
   */
  @Action
  async validateForm() {
    const formStore = this;
    return new Promise((resolve) => {
      const allFields = formStore.getFields();
      let isValid = true;

      // loop all fields and validate each and one of them
      const validatedFields = mapValues(allFields, field => {
        const dataToUpdate = Field.updateFieldOnValueChange(field.value, field, formStore);
        if (dataToUpdate.error) {
          isValid = false
        }

        return {
          ...field,
          ...dataToUpdate
        }
      });

      formStore.fields.assign(validatedFields);

      if (isValid) {
        // trigger onValidationSuccess hook
        formHooksHandler('onValidationSuccess', formStore);
      } else {
        // trigger onValidationFailed hook
        formHooksHandler('onValidationFailed', formStore);
      }

      return resolve(isValid)
    });
  }

  /**
   * Reset fields validations
   */
  @Action
  resetValidation() {
    const allFields = this.getFields();

    // loop all fields and remove validation messages
    const validatedFields = mapValues(allFields, field => {
      return {
        ...field,
        errorMessages: [],
        error: false,
        valid: true,
        validated: false,
      }
    });

    this.fields.assign(validatedFields)
  }

  /**
   * update field is not similar to assign, because it only update the states that is provided in the `fieldConfig`
   * @param {Partial<Field>} fieldConfig
   */
  @Action
  updateFields(fieldConfig) {
    const allFields = this.getFields();

    // loop all fields and update states
    const updatedField = mapValues(allFields, field => {
      const statesToBeUpdated = fieldConfig[field.name];
      if (statesToBeUpdated) {
        const newField = {
          ...field,
          ...statesToBeUpdated,
        };
        if (statesToBeUpdated.hasOwnProperty('value')) {
          const formatter = statesToBeUpdated.formatter ? statesToBeUpdated.formatter : field.formatter;
          newField.value = formatter(newField)
        }
        return newField
      }

      return field
    });

    this.fields.assign(updatedField)
  }

  /**
   * update fields' `values`
   *
   * @example
   * formStore.updateValues({
   *   fieldName: value,
   *   fieldName2: ['value', 'value2'],
   * })
   *
   * @param {Record<string, *>} values
   */
  @Action
  updateValues(values) {
    this.updateFields(mapValues(values, value => ({
      value,
    })));
  }

  /**
   * update fields' `values` and `defaultValues`
   * works exactly like `updateValues`
   * @alias updateValues
   * @param {Record<string, *>} values
   */
  @Action
  updateInitials(values) {
    this.updateFields(mapValues(values, value => ({
      defaultValue: value,
      value,
    })));
  }

  makeSetField(fieldName) {
    return function(key, value) {

      // I know this is dangerous
      // noinspection JSPotentiallyInvalidUsageOfClassThis
      this.updateFields({
        [fieldName]: {
          [key]: value,
        }
      });
    }
  }

  makeSetFieldValue(fieldName) {
    return function(value, withValidation = true) {
      // noinspection JSPotentiallyInvalidUsageOfClassThis
      const field = this.fields.get(fieldName);
      let newField = {};

      if (withValidation) {
        newField = Field.updateFieldOnValueChange(value, field, this)
      } else {
        newField.value = value
      }

      // I know this is dangerous
      // noinspection JSPotentiallyInvalidUsageOfClassThis
      this.updateFields({
        [fieldName]: newField
      });
    }
  }

  /**
   * Map error messages from argument or ApiStore `requestError` to fields
   * If `customErrors` didn't pass in, it will use `requestError`
   * @param {Record<string, string>?} customErrors
   */
  @Action
  mapBackendErrorsToFields(customErrors) {
    // get requestError from `ApiStore`
    // noinspection JSUnresolvedVariable
    const validationErrors = customErrors ||
      get(this.requestError ? this.requestError.get() : undefined, 'data.data.errors') ||
      get(this, 'data.errors');

    if (validationErrors) {
      // if there's validation errors, map it to the fields
      const errorMessageList = mapValues(validationErrors, (errorMessages) => ({
        error: true,
        valid: false,
        errorMessages,
        validated: true,
      }));

      this.updateFields(errorMessageList)
    }
  }

}
