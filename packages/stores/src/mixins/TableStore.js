import {reduce, map, pick, pickBy, mapKeys, transform, get} from 'lodash';
import {State} from '../helpers/State';
import {Getter} from '../helpers/Getter';
import {Action} from '../helpers/Action';
import {defaultFilterQuery, defaultPaginationQuery} from '../consts/stores';
import {BaseStore} from '../BaseStore';
import {isDevelopment} from '../consts/env';

/**
 * @class TableStore
 * @constructor
 * @param {TableColumn[]} tableColumns
 * @param {TableStoreOptions[]} options
 * @param {string[]} defaultSelectedColumns
 */

export class TableStore {
  
  constructor({tableColumns, defaultSelectedColumns, options, ...args}) {
    this.tableColumns.updateDefaultValue(tableColumns);    
    let defaultComputedSelectedColumns = [];    
    if (defaultSelectedColumns) {
      this.defaultSelectedColumns = defaultSelectedColumns;
      if (typeof defaultSelectedColumns === 'function') {
        defaultComputedSelectedColumns = defaultSelectedColumns();
      } else {
        defaultComputedSelectedColumns = defaultSelectedColumns
      }
    } else {
      // default to all columns
      defaultComputedSelectedColumns = tableColumns.map(column => column.value);
    }

    this.selectedColumns.updateDefaultValue(defaultComputedSelectedColumns);

    this.tableOptions = {
      ...this.tableOptions,
      ...options,
    };

    // update states' default value from TableStore's constructor
    BaseStore.updateState(this, args)
  }

  defaultSelectedColumns;

  /**
   * @type {TableStoreOptions}
   */
  tableOptions = {
    filterTableColumns: false, // filter collection's keys according to `filteredColumns`
    listMapWithTableColumns: false, // if set to true, the item's label will follow `tableColumns[].text`, use in `listMappedWithColumnLabel`
    serverSitePagination: true, // if set to true, tableProps.pagination = -1
    // expandAllByDefault: false, // if set to true, all rows will expand by default
    selectableRows: false
  };

  /**
   * Store event hooks: events `onInit`, `onRegister`
   * onInit: on store register
   * @type {Record<string, Function>}
   */
  storeHooks = {
    onStoreRegistered() {
      if (typeof this.defaultSelectedColumns === 'function') {
        let defaultComputedSelectedColumns = this.defaultSelectedColumns() || [];

        if (defaultComputedSelectedColumns.length === 0) {
          // default to all columns
          defaultComputedSelectedColumns = this.tableColumns.get().map(column => column.value);
        }

        this.selectedColumns.assign(defaultComputedSelectedColumns);
      }
    },
  };

  /**
   * Store Data
   * @type {any[]}
   */
  @State
  data = [];

  /**
   * Table columns
   * @type {TableColumn[]}
   */
  @State
  tableColumns = [];

  /**
   * Selected columns
   * @type {string[]}
   */
  @State
  selectedColumns = [];

  /**
   * Replace Query States from ApiStore
   * @type {Object}
   */
  @State
  query = {
    ...defaultPaginationQuery,
    ...defaultFilterQuery,
  };

  /**
   * An object of item keys that are selected from the table
   * @type {Object}
   */
  @State
  selectedRows = {};

  @State
  itemKey = 'uuid';

  /**
   * Table props
   * @type {ReadonlyArray<TableProp>}
   */
  @Getter
  tableProp() {
    try {
      const query = this.query.get();
      const rowsPerPage = this.tableOptions.serverSitePagination ? -1 : query.limit;
      const pagination = this.pagination ? this.pagination() : {};

      return {
        sortBy: [query.sort || ''],
        sortDesc: [query.order === 'DESC'],
        totalItems: pagination.total,
        page: query.page,
        rowsPerPage,
        from: pagination.from,
        to: pagination.to,
      }
    } catch (e) {
      throw new Error(e)
    }
  }

  /**
   * @return {string[]}
   */
  @Getter
  allColumnKeys(state) {
    return map(state.tableColumns, (column) => column.value)
  }

  /**
   * table column list in object
   * well easier to get column by object's key
   * @return {Record<string, TableColumn>}
   */
  @Getter
  tableColumnsObject(state) {
    return reduce(state.tableColumns, (columnsObject, column) => {
      columnsObject[column.value] = column;

      return columnsObject
    }, {})
  }

  /**
   * Sort by
   * @return {string}
   */
  @Getter
  sortBy() {
    return this.tableProp().sortBy
  }

  /**
   * is descending
   * @return {boolean}
   */
  @Getter
  isDescending() {
    return this.tableProp().descending
  }

  PROTECTED_COLUMNS = ['uuid', 'id'];

  /**
   * Format list item according to selectedColumns
   * @return {any[]}
   */
  @Getter
  filteredList() {
    const selectedColumns = this.selectedColumns.get();
    const columnsObject = this.filteredColumnsObject();

    return map(this.data.get(), (item) => {
      if (this.tableOptions.filterTableColumns) {
        return pickBy(item, (value, key) => {
          if (columnsObject[key] && columnsObject[key].formatter) {
            return columnsObject[key].formatter(item)
          }

          return selectedColumns.includes(key) || this.PROTECTED_COLUMNS.includes(key);
        });
      }

      return item
    })
  }

  /**
   * Filter table columns according to selectedColumns
   * @return {any[]}
   */
  filteredColumnsObject() {
    const selectedColumns = this.selectedColumns.get();

    return pick(this.tableColumnsObject(), selectedColumns)
  }

  /**
   * Filter table rows according to selectedRows
   * @return {any[]}
   */
  filteredRows(list = this.data.get()) {
    const key = this.itemKey.get();
    const selectedRows = this.selectedRows.get();
    let filtered = list
    // filter selected rows
    if (Array.isArray(list) && this.tableOptions.selectableRows) {
      const selected = list.filter(item => selectedRows[item[key]]);

      // Get all rows in table by default
      if (selected.length > 0) {
        filtered = selected
      }
    }
    return filtered
  }

  /**
   * Map list's key with column's label
   * @return {any[]}
   */
  async listMappedWithColumnLabel(list) {
    if (!list) {
      // No pagination
      const query = {
        ...this.query.get(),
        page: undefined,
        limit: undefined,
      }
      const response = await this.apiRequest(query)
      list = this.dataFormatter(response)
    }

    list = this.filteredRows(list)

    const selectedColumns = this.filteredColumnsObject();
    // use it when there's custom column in `tableColumns`
    // list's item will shape according to `tableColumns[].text`
    if (this.tableOptions.listMapWithTableColumns) {
      return map(list, (item) => {
        return transform(selectedColumns, (newItem, column, key) => {
          if (column.type === 'expand') {
            //
          } else if (column.type === 'index') {
            //
          } else if (column.formatter) {
            newItem[column.text] = column.formatter(item);
          } else {
            newItem[column.text] = get(item, key);
          }
        }, {})
      })
    }

    return map(list, (item) => {
      return mapKeys(item, (value, key) => {
        // replace existing key with label
        // mainly for excel purposes
        return selectedColumns[key] ? selectedColumns[key].text : key;
      })
    })
  }

  /**
   * Check if column is part of `selectedColumns` list
   * @param columnName
   * @return {boolean}
   */
  @Action
  hasColumn(columnName) {
    return this.selectedColumns.get().includes(columnName)
  }

  /**
   * Check if column exist
   * @param columnName
   * @return {boolean}
   */
  @Action
  doesColumnExist(columnName) {
    return this.allColumnKeys().includes(columnName)
  }

  /**
   * Must be called if table columns are set dynamically (or export excel would not work)
   * @return undefined
   */
  @Action
  updateSelectedColumns() {
    const computedSelectedColumns = this.tableColumns.get().map(column => column.value);
    this.selectedColumns.assign(computedSelectedColumns);
  }
}

