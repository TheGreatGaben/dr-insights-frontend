import {statePrefix} from '../../utils';

/**
 * decorator to inject action into store
 * TODO: add hooks to chain actions?
 * ====================================
 * example:
 * ...store.js
 * class StoreName {
 *   @Action
 *   actionName({ state, dispatch, commit, getters, rootGetters }, payload) {
 *     // caveat: try not to use arrow function, array function not supported at this moment
 *     // caveat: don't use `this` in the function, when you use `this`, it's an instance of vuex store
 *   }
 * }
 *
 * ====================================
 *
 * @param target
 * @param name
 * @param descriptor
 * @return {*}
 * @constructor
 */
export function Action(target, name, descriptor) {
  const initialFunction = typeof descriptor.initializer === 'function' ? descriptor.initializer() : descriptor.value;

  descriptor.initializer = () => {
    const curriedAction = function(moduleName) {
      if (!curriedAction.curried) {
        throw Error(`Action is not registered yet. Register "${target.constructor.name}" in StoreInjector`);
      }

      const prefix = statePrefix(moduleName, name);
      const returnAction = initialFunction;

      returnAction.fn = curriedAction.defaultFunction;
      returnAction.actionName = prefix;
      return returnAction;
    };

    curriedAction.defaultFunction = initialFunction;
    curriedAction.isAction = true;
    curriedAction.curried = false;
    curriedAction.updateDefaultFunction = function(fn) {
      if (typeof fn !== 'function') {
        throw Error('Please pass in Function.');
      }
      curriedAction.defaultFunction = fn;
    };
    return curriedAction;
  };

  return descriptor;
}
