import {statePrefix} from '../../utils';
import {get} from 'vuex-dry';

/**
 * decorator to inject getter into store
 * ====================================
 * example:
 * ...store.js
 * class StoreName {
 *   @Getter
 *   getterName({ state }) {
 *     return state.a + state.b
 *   }
 * }
 *
 * ====================================
 *
 * @param target
 * @param name
 * @param descriptor
 * @return {*}
 * @constructor
 */
export function Getter(target, name, descriptor) {
  const getterFunction = typeof descriptor.initializer === 'function' ? descriptor.initializer() : descriptor.value;

  descriptor.initializer = () => {
    const curriedState = function(moduleName) {
      if (!curriedState.curried) {
        throw Error(`Getter is not registered yet. Register "${moduleName || target.constructor.name}" in StoreInjector`);
      }

      const prefix = statePrefix(moduleName, name);
      const returnGetter = get(prefix);

      returnGetter.getter = getterFunction;
      returnGetter.stateName = prefix;
      return returnGetter;
    };

    curriedState.isGetter = true;
    curriedState.curried = false;
    return curriedState;
  };

  return descriptor;
}
