import {get, map, clone} from 'lodash';
import {$commit, $action, $get, action, sync} from 'vuex-dry';
import {statePrefix} from '../../utils';

/**
 * @description
 * State decorator to inject ORM like APIs into states
 * APIs are proxying from `vuex-dry` features,
 * if you familiar with `vuex-dry` it works as it is.
 * if you don't, it just works like standard primitive types (not really, but i wanna achieve that!!) TODO: do what primitives can do!!!
 *
 * Why?
 * why not `vuex-pathify`, still using string to construct get states and to call actions, 😅 might have better APIs than `vuex-dry`
 * Will revisit here
 * these decorators are created to avoid using vuex-dry APIs (but you still can use it la in some cases)
 * better IntelliSense, lesser confusions (I hope.), easier to understand
 *
 * Vuex-dry cons:
 * - a little rigid where the actions/getters defined depends on state's initial type,
 *   as the state's type changed, the actions/getters did not change accordingly
 * @ref https://github.com/eunjae-lee/vuex-dry/blob/master/DOCUMENT.md#adding-your-own-getters-mutations-and-actions
 *
 * @docs
 *
 * General APIs:
 * - state.assign(val)
 * - state.reset()
 * - state.get(arg?) TODO: utilises `getter`, so a get state does not requires to call .get()
 * - state.sync(customGet?, customSet?)
 *
 * Array APIs:
 * - state.add(value)
 * - state.push(...values)
 * - state.unshift(value)
 * - state.delete(fn)
 * - state.update(arg)
 * - state.find(predicate)
 * - state.filter(predicate)
 * - state.findIndex(predicate)
 * - state.indexOf(predicate)
 * - state.indexOf(searchElement)
 * - state.includes(searchElement)
 *
 * Object:
 * - state.get(path?)
 * - state.set(key, value)
 *
 * ====================================
 * @example
 *
 * ...store.js
 * class StoreName {
 *   @State
 *   stateName = []
 * }
 *
 * const storeName = new StoreName() // import it to your global store
 *
 * ...Component.vue
 * <div>
 *   // if the state default value is an object,
 *   // pass in optional argument to get nested value
 *   {{ getNestedValueStoreName('user.name') }}
 * </div>
 *
 * ...
 * ...
 * computed: {
 *   storeName: storeName.get,
 *   getNestedValueStoreName: storeName.get
 *   storeNameSync: storeName.sync, // two way bind computed property (TODO: not working at the moment)
 * },
 * methods: {
 *   addStoreName: storeName.add, // Caveat: only 1 item at a time, is not like push
 *   changeStoreName: storeName.assign,
 *   resetStoreName: storeName.reset,
 * }
 *
 * ====================================
 *
 * @return {Function}
 * @constructor
 * @param target
 * @param name
 * @param descriptor
 */
export function State(target, name, descriptor) {
  // carry defaultValue till curriedState initialized
  const initialValue = typeof descriptor.initializer === 'function' ? descriptor.initializer() : undefined;

  descriptor.initializer = function() {
    /**
     * @description
     * Curried State to get latest moduleName on store registered
     * latest moduleName? well, stores that are nested,
     * their `moduleName` will be prefixed with parent store's name
     * to avoid store module name clashing
     *
     * @typedef CurriedState
     * @param moduleName
     * @return {StateInstance<any>}
     */
    const curriedState = function(moduleName) {
      if (!curriedState.curried) {
        throw Error(`State is not registered yet. Register "${moduleName || target.constructor.name}" in StoreInjector`);
      }

      const prefix = statePrefix(moduleName, name);

      let returnState = {
        prefix,
        default: curriedState.default,
        assign: action(`${prefix}$assign`),
        reset: action(`${prefix}$reset`),
        sync: sync(`${prefix}`),
        toggle: (bool) => {
          if (typeof bool === 'boolean') {
            $action(`${prefix}$assign`, bool)
          } else {
            $action(`${prefix}$assign`, !$get(`${prefix}`))
          }
        },
      };

      // array API
      returnState = constructArrayStateAPI(returnState, prefix);

      // object API
      returnState = constructObjectStateAPI(returnState, prefix);

      returnState.stateName = prefix;

      // noinspection JSValidateTypes
      return returnState;
    };

    /** @memberOf CurriedState */
    curriedState.default = initialValue;
    /** @memberOf CurriedState */
    curriedState.isState = true;

    curriedState.curried = false;
    curriedState.assign = function() {
      throw Error(`Store not available. Please register "${target.constructor.name}" in StoreInjector`);
    };
    curriedState.get = function() {
      throw Error(`Store not available. Please register "${target.constructor.name}" in StoreInjector`);
    };

    /** @memberOf CurriedState */
    curriedState.updateDefaultValue = function(value) {
      curriedState.default = value;
    };

    return curriedState;
  };

  return descriptor;
}

/**
 * @description
 * Add Array's APIs into state
 * example: add, push, unshift, delete, update, find, filter, findIndex, indexOf, includes
 * @param returnState
 * @param prefix
 * @return {StateArrayProperties}
 */
function constructArrayStateAPI(returnState, prefix) {
  returnState.add = value => $commit(`${prefix}$add`, value); // Caveat: only add 1 item at a time
  returnState.push = (...values) => {
    return $commit(`${prefix}$assign`, [...returnState.get(), ...values]);
  };
  returnState.unshift = value => $commit(`${prefix}$unshift`, value);
  returnState.delete = fn => $commit(`${prefix}$delete`, fn);
  // returnState.update = arg => $commit(`${prefix}$assign`, arg);
  returnState.find = predicate => {
    return $get(`${prefix}$find`, predicate);
  };
  returnState.filter = predicate => {
    return $get(`${prefix}$filter`, predicate);
  };
  returnState.findIndex = predicate => {
    return $get(`${prefix}`).findIndex(predicate);
  };
  returnState.indexOf = searchElement => {
    return $get(`${prefix}`).indexOf(searchElement);
  };
  returnState.includes = searchElement => {
    return $get(`${prefix}`).includes(searchElement);
  };

  return returnState;
}

/**
 * Add Object's APIs into state
 * example: get, set
 * @param returnState
 * @param prefix
 * @return {StateObjectProperties}
 */
function constructObjectStateAPI(returnState, prefix) {
  returnState.set = (key, value) => $commit(`${prefix}$set`, {key, value});
  returnState.update = (value) => {
    // supported array, object data type as well update
    const currentState = $get(`${prefix}`);
    let updatedState = undefined;

    if (Array.isArray(currentState)) {
      updatedState = map(currentState, (state, index) => {
        const valueTobeUpdated = value[index];
        const isObject = typeof state === 'object';

        // check if the item is an object or not
        if (valueTobeUpdated) {
          return isObject ? {...state, ...valueTobeUpdated} : valueTobeUpdated;
        } else {
          return state;
        }
      });
    } else if (typeof currentState === 'object') {
      updatedState = {
        ...currentState,
        ...value,
      };
    } else {
      updatedState = value;
    }

    return $commit(`${prefix}$assign`, updatedState);
  };
  returnState.get = (path, defaultValue) => {
    if (path) {
      return get($get(`${prefix}`), path, defaultValue);
    }

    const value = $get(`${prefix}`);

    if (['object', 'number', 'string'].includes(typeof value)) {
      return clone(value)
    }

    return value;
  };

  return returnState;
}

