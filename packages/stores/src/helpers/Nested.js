/**
 * to decorate nested store
 * Trying to make it as elegant as possible
 * @return {*}
 * @constructor
 */
export function Nested(target, name, descriptor) {
  const initialValue = typeof descriptor.initializer === 'function' ? descriptor.initializer() : undefined;

  descriptor.initializer = function() {
    // inject isNested state in the store
    initialValue.isNested = true;
    // console.log('here: ', {target, name, descriptor, initialValue})

    return initialValue;
  };

  return descriptor
}

