/**
 * to decorate store with moduleName and propagate to parent constructors
 * Trying to make it as elegant as possible
 * @param moduleName
 * @return {*}
 * @constructor
 */
export function Store(moduleName) {
  if (typeof moduleName !== 'string') {
    throw Error('moduleName not provided.');
  }

  return function StoreDecorator(DeclaredStore) {
    DeclaredStore.moduleName = moduleName;

    Object.preventExtensions(DeclaredStore);
    return DeclaredStore
  };
}
