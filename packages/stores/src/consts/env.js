// default env checking
export let isProduction = ['prod', 'production', 'uat', 'testing'].includes(process.env.NODE_ENV);

export let isDevelopment = !isProduction;

export const updateEnv = (isProd) => {
  isProduction = isProd
  isDevelopment = !isProd
}
