export const STORE_NAME = 'vuexStoreManager';
export const ADD_REGISTERED_STORES = 'ADD_REGISTERED_STORES';
export const REMOVE_REGISTERED_STORES = 'REMOVE_REGISTERED_STORES';

export const STORE_REMOVED_MESSAGE = 'Store has been removed.';

export let defaultFilterQuery = {
  filter: '',
  sort: undefined,
  order: 'DESC',
}

export let defaultPaginationQuery = {
  limit: 50,
  page: 1,
}
