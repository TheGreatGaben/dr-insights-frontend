import {includes, isObject} from 'lodash';

/**
 * Parses a rule string expression.
 */
export const parseRule = rule => {
  let params = [];
  const name = rule.split(':')[0];

  if (includes(rule, ':')) {
    params = rule
      .split(':')
      .slice(1)
      .join(':')
      .split(',');
  }

  return {name, params};
};

/**
 * Normalizes the given rules expression.
 */
export const normalizeRules = rules => {
  if (!rules) {
    return {};
  }

  if (isObject(rules)) {
    return Object.keys(rules).reduce((prev, curr) => {
      let params = [];
      if (rules[curr] === true) {
        params = [];
      } else if (Array.isArray(rules[curr])) {
        params = rules[curr];
      } else if (isObject(rules[curr])) {
        params = rules[curr];
      } else {
        params = [rules[curr]];
      }

      if (rules[curr] !== false) {
        prev[curr] = params;
      }

      return prev;
    }, {});
  }

  if (typeof rules !== 'string') {
    return {};
  }

  return rules.split('|').reduce((prev, rule) => {
    const parsedRule = parseRule(rule);
    if (!parsedRule.name) {
      return prev;
    }

    prev[parsedRule.name] = parsedRule.params;
    return prev;
  }, {});
};

/**
 * Appends a rule definition to a list of rules.
 */
export const appendRule = (rule, rules) => {
  if (!rules) {
    return normalizeRules(rule);
  }

  if (!rule) {
    return normalizeRules(rules);
  }

  if (typeof rules === 'string') {
    rules = normalizeRules(rules);
  }

  return {
    ...rules,
    ...normalizeRules(rule),
  };
};

export const asyncForEach = async (collection, callback) => {
  const list = [];

  if (Array.isArray(collection)) {
    for (let index = 0; index < collection.length; index++) {
      list[index] = await callback(collection[index], index, collection);
    }
  } else if (typeof collection === 'object') {
    const keys = keys(collection);

    for (let index = 0; index < keys.length; index++) {
      list[index] = await callback(collection[keys[index]], index, collection);
    }
  }

  return Promise.resolve(list);
};

/**
 * State name generator following vuex-dry format `moduleName/state`
 * @param moduleName
 * @param name
 * @return {string}
 */
export function statePrefix(moduleName, name) {
  return `${moduleName}/${name}`;
}

export function makePrivateProperty(object, propName) {
  Object.defineProperty(object, propName, {
    value: object[propName],
    enumerable: false,
    configurable: false,
  });
}

export const delay = milliseconds =>
  new Promise(resolve => setTimeout(() => resolve(), milliseconds));
