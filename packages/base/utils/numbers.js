export const isCurrency = (value) => {
  const currencyRegex = /^-?[0-9,]+(\.\d{1,6})?$/;
  return currencyRegex.test(value);
}
