/**
 * State name generator following vuex-dry format `moduleName/state`
 * @param moduleName
 * @param name
 * @return {string}
 */
export function statePrefix(moduleName, name) {
  return `${moduleName}/${name}`;
}

export function makePrivateProperty(object, propName) {
  Object.defineProperty(object, propName, {
    value: object[propName],
    enumerable: false,
    configurable: false,
  });
}
