import {map, mapValues, isEmpty} from 'lodash';
import {isCurrency} from './numbers';
import {utils, writeFile} from 'xlsx';

// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
export function exportJSONToExcel({data, filename, autoWidth = true} = {}) {
  const validatedData = map(data, item => {
    return mapValues(item, value => {
      if (isCurrency(value)) {
        return Math.round(parseFloat(value) *100) / 100;
      }

      return value;
    });
  });

  const xlsxName = filename;
  const sheetName = 'sheet1';
  const workSheet = utils.json_to_sheet(validatedData);
  const workBook = utils.book_new();

  if (autoWidth) {
    /* calculate col width */
    const colWidth = validatedData.map(row => {
      return map(row, (val, headerTitle) => {
        let valToCheck = val;

        if (val === null) {
          return {
            wch: 10,
          };
        }

        /**
         * if val have break lines, need to find the longest line in the string
         */
        if (!isEmpty(val) && val.indexOf('\n') > -1) {
          valToCheck = val
            .split('\n')
            .reduce((selected, next) => (selected.length > next.length ? selected : next));
        }

        return {
          wch: Math.max(headerTitle.length, (valToCheck || '').toString().length) + 1,
        };
      });
    });

    const result = colWidth[0];

    /**
     * loop thru list of columns to calculate column width
     */
    for (let i = 1; i < colWidth.length; i++) {
      for (let j = 0; j < colWidth[i].length; j++) {
        if (result[j]['wch'] < colWidth[i][j]['wch']) {
          result[j]['wch'] = colWidth[i][j]['wch'];
        }
      }
    }

    workSheet['!cols'] = result;
  }

  /* add worksheet to workbook */
  utils.book_append_sheet(workBook, workSheet, sheetName);

  /* export xlsx file */
  writeFile(workBook, xlsxName);
}
