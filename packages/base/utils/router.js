import {reduce, template} from 'lodash';

/**
 * Change from `/pathname/:param/url` to `/pathname/${params}/url`
 * convert to make string _.template ready
 * return: TemplateExecutor
 */
export const convertToTemplate = url => {
  const replacementRegex = /:[0-9a-zA-Z_]*/g;
  const replacementList = url.match(replacementRegex);

  const replacedUrl = reduce(
    replacementList,
    (currentUrl, replacementWord) => {
      const strippedWordToReplace = replacementWord.substr(1);

      return currentUrl.replace(replacementWord, `\${${strippedWordToReplace}}`);
    },
    url
  );

  return template(replacedUrl);
};

/**
 * Lazy-loads view components, but with better UX. A loading view
 * will be used if the component takes a while to load, falling
 * back to a timeout view in case the page fails to load. You can
 * use this component to lazy-load a route with:
 * component: () => lazyLoadView(import('@views/my-view'))
 *
 * NOTE: Components loaded with this strategy DO NOT have access
 * to in-component guards, such as beforeRouteEnter,
 * beforeRouteUpdate, and beforeRouteLeave. You must either use
 * route-level guards instead or lazy-load the component directly:
 * component: () => import('@views/my-view')
 *
 * @param AsyncView
 * @return {Promise<{functional: boolean, render(*, {data?: *, children?: *}): *}>|*}
 */
export function lazyLoadView(AsyncView) {
  const AsyncHandler = () => ({
    component: AsyncView,
    // A component to use while the component is loading.
    // loading: require('@views/_loading').default,
    // Delay before showing the loading component.
    // Default: 200 (milliseconds).
    delay: 400,
    // A fallback component in case the timeout is exceeded
    // when loading the component.
    // error: require('@views/_timeout').default,
    // Time before giving up trying to load the component.
    // Default: Infinity (milliseconds).
    timeout: 10000,
  });

  return Promise.resolve({
    functional: true,
    render(h, {data, children}) {
      // Transparently pass any props or children
      // to the view component.
      return h(AsyncHandler, data, children);
    },
  });
}

export function getRouteTitle(route) {
  if (route.meta && (route.meta.title || route.meta.name)) {
    return route.meta.title || route.meta.name;
  }

  return route.name;
}
