import * as numeral from 'numeral';
import {reduce} from 'lodash';

export const V_MONEY_SETTING = {
  decimal: '.',
  thousands: ',',
  prefix: 'RM ',
  precision: 2,
};

export function currencyFormatterWithoutPrefix(num) {
  if (parseFloat(num) > 0 && parseFloat(num) < 100000000) {
    // noinspection JSUnresolvedFunction
    return numeral(num).format('(0,0.00)');
  }

  // noinspection JSUnresolvedFunction
  return numeral(num).format('(0,0.00)');
}

export function currencyFormatter(num) {
  if (parseFloat(num) > 0 && parseFloat(num) < 100000000) {
    // noinspection JSUnresolvedFunction
    return `RM ${numeral(num).format('(0,0.00)')}`;
  }

  // noinspection JSUnresolvedFunction
  return `RM ${numeral(num).format('(0,0.00)')}`;
}

/**
 * Aggregate/ sum up value from a list using the `key` provided
 * @param list
 * @param key
 * @return {number}
 */
export function aggregator(list, key) {
  return reduce(
    list,
    (result, invoice) => {
      result += parseFloat(invoice[key]) || 0;

      return result;
    },
    0
  );
}
