import Vue from 'vue';
import {show} from './toast';

export function toastError(message, options = Vue.prototype.$toastError.options) {
  return show(message, {
    color: 'error',
    closeBtn: true,
    ...options
  });
}

export function installApiErrorToast(Vue, options) {
  Vue.prototype.$toastError = toastError;
  Vue.prototype.$toastError.options = options || {};
  Vue.$toastError = toastError;
  Vue.$toastError.options = options || {};
}
