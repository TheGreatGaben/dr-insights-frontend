import Vue from 'vue';
import SnackBar from '../components/SnackBar';

function createToastComponent(options) {
  return new Promise(resolve => {
    const app = document.querySelector('.v-application') || document.body;

    const component = new Vue(
      Object.assign({}, SnackBar, {
        propsData: Object.assign({}, Vue.prototype.$toast.options, options),
        destroyed: () => {
          app.removeChild(component.$el);
          resolve(component.value);
        },
      })
    );
    app.appendChild(component.$mount().$el);
  });
}

export function show(message, options = {}) {
  options.message = message;
  return createToastComponent(options);
}

export function installToast(Vue, options) {
  Vue.prototype.$toast = show;
  Vue.prototype.$toast.options = options || {};
  Vue.$toast = show;
  Vue.$toast.options = options || {};
}
