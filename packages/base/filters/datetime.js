import {isNull, isNaN} from 'lodash';
import {isDate, isValid, format} from 'date-fns';

function validateIsDate(value) {
  if (!isDate(value)) {
    if (isNull(value)) {
      return false;
    }

    const newDate = new Date(value);

    return isValid(newDate);
  }

  return true;
}

export function dateFormatValid(value, config = {
  minYear: 1900,
  maxYear: undefined,
}) {
  if (!value) {
    return false;
  }

  // We support more formats in future, but now only
  // validates Y-m-d format (or specifically: YYYY-MM-DD)
  const tokens = value.split('-');
  if (tokens.length !== 3) {
    return false;
  }
  const year = tokens[0];
  const month = tokens[1];
  const day = tokens[2];
  if (year.length !== 4 || month.length !== 2 || day.length !== 2) {
    return false;
  }
  const yearInt = parseInt(year);
  let yearValid = !isNaN(yearInt);
  if (config.minYear) {
    yearValid = yearValid && yearInt >= config.minYear;
  }
  if (config.maxYear) {
    yearValid = yearValid && yearInt <= config.maxYear;
  }

  const monthInt = parseInt(month);
  const monthValid = !isNaN(monthInt) && monthInt > 0 && monthInt < 13;

  const dayInt = parseInt(day);
  const dayValid = !isNaN(dayInt) && dayInt > 0 && dayInt < 32;

  if (!yearValid || !monthValid || !dayValid) {
    return false;
  }

  return true;
}

export function datetimeShort(date) {
  return validateIsDate(date) ? format(date, 'YYYYMMDDHHmm') : date;
}

export function datetime(date) {
  return validateIsDate(date) ? format(date, 'YYYY-MM-DD HH:mm:ss') : date;
}

export function monthName(date) {
  if (!isDate(date)) {
    return date;
  }
  return `${format(date, 'MMMM')}`;
}

export function monthAndDay(date) {
  if (!isDate(date)) {
    return date;
  }
  const day = format(date, 'DD');
  const lastDayLetter = day.substring(day.length - 1, day.length);
  let daySuffix = 'th';
  switch (lastDayLetter) {
    case '1':
      daySuffix = 'st';
      break;
    case '2':
      daySuffix = 'nd';
      break;
    case '3':
      daySuffix = 'rd';
      break;
  }
  return `${format(date, 'MMMM')} ${format(date, 'D')}${daySuffix}`;
}

export function date(date) {
  // console.log(validateIsDate(date), date);
  return validateIsDate(date) ? format(date, 'YYYY-MM-DD') : date;
}

function pluralize(time, label) {
  if (time === 1) {
    return time + label;
  }
  return time + label + 's';
}

export function timeAgo(time) {
  const between = Date.now() / 1000 - Number(time);
  if (between < 3600) {
    return pluralize(~~(between / 60), ' minute');
  } else if (between < 86400) {
    return pluralize(~~(between / 3600), ' hour');
  } else {
    return pluralize(~~(between / 86400), ' day');
  }
}
