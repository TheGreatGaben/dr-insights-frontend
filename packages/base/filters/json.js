export function json(value) {
  return JSON.stringify(JSON.parse(value), null, 2);
}
