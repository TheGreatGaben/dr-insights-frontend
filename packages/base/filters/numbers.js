import * as numeral from 'numeral';
import {isNaN} from 'lodash';
import {currencyFormatter, currencyFormatterWithoutPrefix} from '../utils/math';

export function toThousandsFilter(num) {
  if (num >= 0) {
    return (+num || 0).toString().replace(/^-?\d+/g, m => m.replace(/(?=(?!\b)(\d{3})+$)/g, ','));
  } else {
    return (
      '(' +
      (-num || 0).toString().replace(/^-?\d+/g, m => m.replace(/(?=(?!\b)(\d{3})+$)/g, ',')) +
      ')'
    );
  }
}

export function toCurrencyValue(num) {
  return currencyFormatter(num);
}

export function toCurrencyValueWithoutPrefix(num) {
  return currencyFormatterWithoutPrefix(num);
}

export function toPercent(num) {
  if (!isNaN(parseFloat(num))) {
    // noinspection JSUnresolvedFunction
    return numeral(num).format('0.00') + '%';
  }

  return num;
}

export function toThousand(num) {
  if (!isNaN(parseFloat(num))) {
    // noinspection JSUnresolvedFunction
    return numeral(num).format('0,0.00');
  }

  return num;
}
