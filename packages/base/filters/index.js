import Vue from 'vue';
import {each, isEmpty, transform} from 'lodash';

/**
 * Caveat: don't add filters here!!
 */

const requireModule = require.context('.', false, /\.js$/);

/**
 * auto import filters
 * @type {Dictionary<Function>}
 */
const filters = transform(requireModule.keys(), (list, fileName) => {
  if (fileName !== './index.js') {
    const module = requireModule(fileName);

    if (!isEmpty(module)) {
      each(module, (fnc, key) => {
        list[key] = fnc;
      });
    }
  }
});

export function installFilters() {
  Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key]);
  });
}
