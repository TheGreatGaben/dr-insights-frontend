export const monthDefinitions = [
  {
    name: 'Jan',
    value: 1,
    fullName: 'January'
  },
  {
    name: 'Feb',
    value: 2,
    fullName: 'February'
  },
  {
    name: 'Mar',
    value: 3,
    fullName: 'March'
  },
  {
    name: 'Apr',
    value: 4,
    fullName: 'April'
  },
  {
    name: 'May',
    value: 5,
    fullName: 'May'
  },
  {
    name: 'Jun',
    value: 6,
    fullName: 'June'
  },
  {
    name: 'Jul',
    value: 7,
    fullName: 'July'
  },
  {
    name: 'Aug',
    value: 8,
    fullName: 'August'
  },
  {
    name: 'Sep',
    value: 9,
    fullName: 'September'
  },
  {
    name: 'Oct',
    value: 10,
    fullName: 'October'
  },
  {
    name: 'Nov',
    value: 11,
    fullName: 'November'
  },
  {
    name: 'Dec',
    value: 12,
    fullName: 'December'
  },
]
