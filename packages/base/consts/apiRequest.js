export const pageLimits = [
  {
    text: '15/page',
    value: 15,
  },
  {
    text: '30/page',
    value: 30,
  },
  {
    text: '50/page',
    value: 50,
  },
  {
    text: '100/page',
    value: 100,
  },
];
