import Vue from 'vue';

import CoreButton from './CoreButton';
import CoreTooltip from './CoreTooltip';
import ActionMenu from './ActionMenu';
import AnyTag from './AnyTag';
import ColumnSelect from './ColumnSelect';
import Combobox from './Combobox';
import CurrencyDisplay from './CurrencyDisplay';
import DatePicker from './DatePicker';
import DateRangePicker from './DateRangePicker';
import FakeCheckbox from './FakeCheckbox';
import Money from './Money';
import MoneyInput from './MoneyInput';
import Pagination from './Pagination';
import QueryPagination from './QueryPagination';
import RoundedCard from './RoundedCard';
import RouteLink from './RouteLink';
import SearchBar from './SearchBar';
import SearchToolbar from './SearchToolbar';
import Selects from './Selects';
import SnackBar from './SnackBar';
import TableColumn from './TableColumn';
import TableExpandColumn from './TableExpandColumn';
import TableExportExcelButton from './TableExportExcelButton';
import TableHeader from './TableHeader';
import TableList from './TableList';
import TextField from './TextField';
import TextGlow from './TextGlow';
import ColumnButtonSelection from './ColumnButtonSelection';
import InvoiceDateFilter from './InvoiceDateFilter';
import AppSearchToolbar from './AppSearchToolbar';

export function installComponents() {
  Vue.component('ActionMenu', ActionMenu);
  Vue.component('AnyTag', AnyTag);
  Vue.component('ColumnSelect', ColumnSelect);
  Vue.component('Combobox', Combobox);
  Vue.component('CoreButton', CoreButton);
  Vue.component('CoreTooltip', CoreTooltip);
  Vue.component('CurrencyDisplay', CurrencyDisplay);
  Vue.component('DatePicker', DatePicker);
  Vue.component('DateRangePicker', DateRangePicker);
  Vue.component('FakeCheckbox', FakeCheckbox);
  Vue.component('Money', Money);
  Vue.component('MoneyInput', MoneyInput);
  Vue.component('Pagination', Pagination);
  Vue.component('QueryPagination', QueryPagination);
  Vue.component('RoundedCard', RoundedCard);
  Vue.component('RouteLink', RouteLink);
  Vue.component('SearchBar', SearchBar);
  Vue.component('SearchToolbar', SearchToolbar);
  Vue.component('Selects', Selects);
  Vue.component('SnackBar', SnackBar);
  Vue.component('TableColumn', TableColumn);
  Vue.component('TableExpandColumn', TableExpandColumn);
  Vue.component('TableExportExcelButton', TableExportExcelButton);
  Vue.component('TableHeader', TableHeader);
  Vue.component('TableList', TableList);
  Vue.component('TextField', TextField);
  Vue.component('TextGlow', TextGlow);
  Vue.component('ColumnButtonSelection', ColumnButtonSelection);
  Vue.component('InvoiceDateFilter', InvoiceDateFilter);
  Vue.component('AppSearchToolbar', AppSearchToolbar)

}
