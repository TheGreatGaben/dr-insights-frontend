declare module 'vue/types/vue' {
  interface ToastOptions {
    y: String;
    x: String;
    timeout: Number;
    mode: String;
    color: String;
    closeBtn: Boolean;
  }
  interface Vue {
    $toast(message: string, options?: Partial<ToastOptions>)
    $toastError(message: string, options?: Partial<ToastOptions>)
  }
}
