declare module '@nixel/base/components' {
  // @ts-ignore
  import { Component } from 'vue'

  const ActionMenu: Component
  const AnyTag: Component
  const ColumnSelect: Component
  const Combobox: Component
  const CoreButton: Component
  const CurrencyDisplay: Component
  const DatePicker: Component
  const DateRangePicker: Component
  const FakeCheckbox: Component
  const Money: Component
  const MoneyInput: Component
  const Pagination: Component
  const QueryPagination: Component
  const RoundedCard: Component
  const RouteLink: Component
  const SearchBar: Component
  const SearchToolbar: Component
  const Selects: Component
  const SnackBar: Component
  const TableColumn: Component
  const TableExpandColumn: Component
  const TableExportExcelButton: Component
  const TableHeader: Component
  const TableList: Component
  const TextField: Component

  export {
    ActionMenu,
    AnyTag,
    ColumnSelect,
    Combobox,
    CoreButton,
    CurrencyDisplay,
    DatePicker,
    DateRangePicker,
    FakeCheckbox,
    Money,
    MoneyInput,
    Pagination,
    QueryPagination,
    RoundedCard,
    RouteLink,
    SearchBar,
    SearchToolbar,
    Selects,
    SnackBar,
    TableColumn,
    TableExpandColumn,
    TableExportExcelButton,
    TableHeader,
    TableList,
    TextField,
  }
}
