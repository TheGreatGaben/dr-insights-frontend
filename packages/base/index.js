import Vue from 'vue';
import {installComponents} from './components';
import {installFilters} from './filters';
import {installToast} from './plugins/toast';
import {installApiErrorToast} from './plugins/apiErrorToast';

export function baseInstaller() {
  // install plugins
  Vue.use(installToast);
  Vue.use(installApiErrorToast);

  // install components
  installComponents();

  // install filters
  installFilters();
}
