import Vue from 'vue'
import {baseInstaller} from '@nixel/base'
import {storesInstaller} from '@nixel/stores'
import App from './App.vue'
import {router} from './router'
import {store} from './store'
import filters from './filters';
import {isDevelopment, isProduction} from './utils';

// import style
import './styles/index.scss';

// install base
Vue.use(baseInstaller);

// install stores
Vue.use(storesInstaller, {
  // defaultPaginationOptions: {},
  // defaultFilterOptions: {},
  isProduction,
});

import './registerServiceWorker';
import './checkVersion';

import './plugins';
import vuetify from './plugins/vuetify'

import './components';

Vue.config.productionTip = isDevelopment;

export const app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

/**
 * register global utility filters.
 */
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

/**
 * register global directives
 */
import {permissionDirectiveInstall} from './directives/permission';
Vue.use(permissionDirectiveInstall);

if (isDevelopment) {
  window.Vue = Vue;
  window.app = app;

  window.dateFns = require('date-fns');
}
