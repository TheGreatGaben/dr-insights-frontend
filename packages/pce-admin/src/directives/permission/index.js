import {permission} from './permission'

export const permissionDirectiveInstall = function(Vue) {
  Vue.directive('permission', permission)
};
