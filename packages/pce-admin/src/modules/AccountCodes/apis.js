import Vue from 'vue';

export const fetchAccountCodeDetailsApi = (params) => Vue.axios.get(`/api/v2/finance_account_codes/${params.uuid}`, {
  params
});
