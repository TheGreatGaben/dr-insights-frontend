/**
 *
 * @type {TableColumn[]}
 */
export const accountCodesTableHeaders = [
  {
    align: 'center',
    type: 'expand',
    value: '_expand',
    width: '10px',
  },
  {
    text: 'No.',
    type: 'index',
    value: '_index',
    align: 'left',
    width: '80px',
  },
  {
    text: 'Code',
    sortable: true,
    value: 'code',
    width: '100px',
    align: 'left',
  },
  {
    text: 'Account Code',
    sortable: true,
    value: 'name',
    align: 'left',
  },
];
