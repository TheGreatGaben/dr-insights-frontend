import Vue from 'vue'
import {ApiStore, State} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Getter} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {accountCodesTableHeaders} from '../resources/accountCodes';
import {defaultFilterQuery} from '@nixel/stores/src/consts/stores';

const fetchAccountCodeListApi = (params) => Vue.axios.get('/api/v2/finance_account_codes', {
  params: {
    ...params,
    to_tree: 1,
  },
});

@Store('accountCodeTableList')
class AccountCodeTableList extends BaseStore {
  static mixins = [
    new ApiStore(fetchAccountCodeListApi, []),
    new TableStore({
      tableColumns: accountCodesTableHeaders,
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]

  @State
  query = {
    ...defaultFilterQuery,
    sort: 'code',
    order: 'ASC',
  };
}

@Store('accountCodes')
class AccountCodes extends BaseStore {
  static mixins = [
    new ApiStore(fetchAccountCodeListApi, [])
  ]

  @Getter
  accountCodeNames() {
    const accountCodes = this.data.get()
    return accountCodes.map(accountCode => accountCode.name);
  }
}

export const accountCodes = new AccountCodes();

export const accountCodeTableList = new AccountCodeTableList();

