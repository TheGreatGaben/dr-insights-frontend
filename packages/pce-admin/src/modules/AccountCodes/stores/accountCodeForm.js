import Vue from 'vue';
import {BaseStore, FormStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';

const createAccountCodeDetailsApi = (params) => Vue.axios.post('/api/v2/finance_account_codes', params);

const updateAccountCodeDetailsApi = (params) => Vue.axios.put(`/api/v2/finance_account_codes/${params.uuid}`, params);

const accountCodeFields = {
  name: {
    value: '',
    label: 'Name',
    placeholder: 'Name',
    rules: 'required',
  },
  code: {
    value: '',
    label: 'Code',
    placeholder: 'Code',
    rules: 'required',
  },
  parent_id: {
    value: '',
    label: 'Group',
    placeholder: 'Group',
  },
};

@Store('createAccountCodeForm')
class CreateAccountCodeForm extends BaseStore {
  static mixins = [
    new ApiStore(createAccountCodeDetailsApi),
    new FormStore({
      fields: accountCodeFields,
      hooks: {
        onSubmitSuccess(formStore) {
          console.log(formStore);
          formStore._vm.$toast('Successfully created account code.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}
@Store('updateAccountCodeForm')
class UpdateAccountCodeForm extends BaseStore {
  static mixins = [
    new ApiStore(updateAccountCodeDetailsApi),
    new FormStore({
      fields: accountCodeFields,
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update account code.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}

export const createAccountCodeForm = new CreateAccountCodeForm();

export const updateAccountCodeForm = new UpdateAccountCodeForm();
