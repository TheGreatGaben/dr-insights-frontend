import Vue from 'vue';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchRoleListApi = (params) => Vue.axios.get('/api/v2/roles', {
  params
});

@Store('roleTableList')
class RoleTableList extends BaseStore {
  static mixins = [
    new ApiStore(fetchRoleListApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'No.',
          type: 'index',
          align: 'left',
          width: '80px',
        },
        {
          text: 'Role\'s Name',
          sortable: true,
          value: 'name',
          align: 'left',
        },
      ],
    })
  ]
}

@Store('roleList')
class RoleList extends BaseStore {
  static mixins = [
    new ApiStore(fetchRoleListApi, []),
  ]
}

export const roleList = new RoleList();

export const roleTableList = new RoleTableList();

