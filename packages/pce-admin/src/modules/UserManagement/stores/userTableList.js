import Vue from 'vue';
import {map, find, transform} from 'lodash';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {userTableHeaders} from '../resources/userList';
import {apiErrorNotification} from '@/utils/request';
import {Action, Nested} from '@nixel/stores';
import {FormStore} from '@nixel/stores';

const fetchUserListApi = (params) => Vue.axios.get('/api/v2/users', {
  params
});

const fetchProjectPermissionsApi = () => Vue.axios.get('/api/v2/projects/permissions');

export const batchUpdateUserRoles = (formData) => Vue.axios.put('/api/v2/batch-update/users/roles', {
  data: formData.users,
});

@Store('projectPermissions')
class ProjectPermissions extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectPermissionsApi, []),
  ]
}

@Store('batchUpdateForm')
class BatchUpdateUserDetails extends BaseStore {
  static mixins = [
    new ApiStore(batchUpdateUserRoles),
    new FormStore({
      fields: {
        users: {
          value: [],
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update users.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          // attach validation errors to fields
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ];

  /**
   * Update user field, give him any roles and permission, it will sync old and new data together
   * this is one messy function
   * @param {roles, permissions}
   * @param userData
   */
  @Action
  updateUsersField({roles, permissions}, userData) {
    const usersField = this.fields.get().users;
    const values = usersField.value;
    const userToBeUpdated = find(values, (value) => {
      return value.id === userData.id;
    });
    const isNew = !userToBeUpdated;
    let result = [...values];

    if (isNew) {
      // if user is new, add into the list
      result.push({
        id: userData.id,
        roles: roles || BatchUpdateUserDetails.getUserRoles(userData),
        permissions: permissions || BatchUpdateUserDetails.getUserPermissions(userData),
      })
    } else {
      // if user has been updated, just sync the value
      result = transform(values, (newResult, value, index) => {
        // update the user
        if (value.id === userData.id) {
          // if roles not provided, get roles from current field value, if also not provided, grab from user's data
          const newRoles = roles || userToBeUpdated.roles || BatchUpdateUserDetails.getUserRoles(userData);

          // same like `newRoles`, but for permissions
          const newPermissions = permissions || userToBeUpdated.permissions || BatchUpdateUserDetails.getUserPermissions(userData);

          newResult[index] = {
            ...userToBeUpdated,
            roles: newRoles,
            permissions: newPermissions,
          }
        }
      }, values)
    }

    usersField.setValue(result)
  }

  static getUserRoles(userData) {
    return map(userData.roles, role => role.name);
  }

  static getUserPermissions(userData) {
    return map((userData.permissions || userData.all_permissions), permission => permission.name)
  }
}

@Store('userTableList')
class UserTableList extends BaseStore {
  static mixins = [
    new ApiStore(fetchUserListApi, []),
    new TableStore({
      tableColumns: userTableHeaders,
    })
  ];

  @Nested
  batchUpdateForm = new BatchUpdateUserDetails()
}

export const projectPermissions = new ProjectPermissions();
export const userTableList = new UserTableList();

