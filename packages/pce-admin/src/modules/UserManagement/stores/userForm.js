import {FormStore} from '@nixel/stores';

export class UserForm extends FormStore {
  constructor({fields, ...args}) {
    super({
      fields: {
        name: {
          value: '',
          label: '* User\'s Name',
          placeholder: 'User\'s name',
          rules: 'required|max:200',
        },
        email: {
          value: '',
          label: '* Email',
          rules: 'required|email',
        },
        ...fields,
      },
      ...args,
    });
  }

}
