import Vue from 'vue';
import {BaseStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Getter, Nested, Store} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {UserForm} from './userForm';

const createUserApi = (params) => Vue.axios.post('/api/v2/users', params);

const updateUserDetailsApi = (params) => Vue.axios.put(`/api/v2/users/${params.id}`, params);

const fetchUserDetailsApi = (params) => Vue.axios.get(`/api/v2/users/${params.id}`, {
  params
});

@Store('createUserForm')
class CreateUserForm extends BaseStore {
  static mixins = [
    new ApiStore(createUserApi),
    new UserForm({
      fields: {
        password: {
          value: '',
          label: '* Password',
          placeholder: 'Set password here.',
          type: 'password',
          rules: 'required|minLength:6',
        },
        password_confirmation: {
          value: '',
        },
        roles: {
          value: [],
          label: 'Roles',
          formatter(field) {
            return field.value.map(role => {
              if (typeof role === 'object') {
                return role.id;
              }

              return role
            })
          }
        },
      },
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully created user.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          // attach validation errors to fields
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}
@Store('updateUserForm')
class UpdateUserForm extends BaseStore {
  static mixins = [
    new ApiStore(updateUserDetailsApi),
    new UserForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update user.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          // attach validation errors to fields
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}

@Store('userDetails')
class UserDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchUserDetailsApi, {}),
  ];

  @Nested
  createUserForm = new CreateUserForm();

  @Nested
  updateUserForm = new UpdateUserForm();

  @Getter
  name(state) {
    return state.data && state.data.name
  }

}

export const userDetails = new UserDetails();

