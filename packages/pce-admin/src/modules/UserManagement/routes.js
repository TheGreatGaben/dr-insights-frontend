/**
 *
 * @type {RouterOptions[]}
 */
import UserManagement from './UserManagement';

export const usersRoutes = [
  {
    path: '/users',
    name: 'Users',
    meta: {
      authRequired: true,
      permissions: ['member'], // hide users page if user don't have permission to use
      title: 'Users',
      icon: 'mdi-account-group'
    },
    redirect: '/users/list',
    component: UserManagement,
    children: [
      {
        path: 'list',
        name: 'UserList',
        meta: {
          authRequired: true,
          title: 'User List',
        },
        component: () => import(/* webpackChunkName: "userList" */ './UserList.vue'),
      },
      {
        path: 'roles',
        name: 'RoleManagement',
        meta: {
          authRequired: true,
          title: 'Role Management',
        },
        component: () => import(/* webpackChunkName: "userList" */ './RoleManagement.vue'),
      },
      {
        path: '*',
        redirect: '/users/list',
      }
    ]
  }
];
