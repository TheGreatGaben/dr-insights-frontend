import Layout from '@/views/layout/Layout';
import ExamplePage from '@/modules/Examples/ExamplePage';

/**
 *
 * @type {RouterOptions[]}
 */
export const exampleRoutes = [
  {
    path: '/examples',
    name: 'examples',
    meta: {
      authRequired: true,
      title: 'Examples',
      icon: 'mdi-file-document'
    },
    component: ExamplePage,
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('./Dashboard.vue')
      },
      {
        path: 'user-profile',
        name: 'User Profile',
        component: () => import('./UserProfile.vue')
      },
      {
        path: 'table-list',
        name: 'Table List',
        component: () => import('./TableList.vue')
      },
      {
        path: 'form',
        name: 'Form',
        redirect: 'form/basic',
        component: Layout,
        children: [
          {
            path: 'basic',
            name: 'Basic Form',
            component: () => import('./ExampleForm.vue')
          },
          {
            path: 'nested',
            name: 'Nested Form',
            component: () => import('./ExampleNestedForm.vue')
          },
        ]
      },
      {
        path: 'typography',
        name: 'Type',
        component: () => import('./Typography.vue')
      },
      {
        path: 'icons',
        name: 'Icons',
        component: () => import('./Icons.vue')
      },
      {
        path: 'maps',
        name: 'Maps',
        component: () => import('./Maps.vue')
      },
      {
        path: 'notifications',
        name: 'Notifications',
        component: () => import('./Notifications.vue')
      },
    ]
  }
];
