import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {State} from '@nixel/stores';
import {fetchProjectExpensesApi, createProjectExpensesApi, updateProjectExpensesApi} from '../apis';
import {TableStore} from '@nixel/stores';
import {expensesTableHeaders, projectExpensesDataFormatter} from '../resources/projectExpenses'
import {ExpensesForm} from './projectExpensesForm';
import {getSelectedColumns, PROJECT_DETAILS_EXPENSES_COLUMNS} from '@/consts/selectedColumns';

@Store('createExpensesForm')
class CreateExpensesForm extends BaseStore {
  static mixins = [
    new ExpensesForm({
      hooks: {
        onSubmitSuccess() {
          this._vm.$toast('Expenses created successfully.', {
            color: 'success',
            closeBtn: true,
          })
        },
      }
    }),
    new ApiStore(createProjectExpensesApi)
  ]
}

@Store('updateExpensesForm')
class UpdateExpensesForm extends BaseStore {
  static mixins = [
    new ExpensesForm({
      hooks: {
        onSubmitSuccess() {
          this._vm.$toast('Expenses updated successfully.', {
            color: 'success',
            closeBtn: true,
          })
        },
      },
    }),
    new ApiStore(updateProjectExpensesApi)
  ];

  @State
  expenseID = undefined
}

@Store('projectExpensesTable')
class ProjectExpensesTable extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectExpensesApi, [], projectExpensesDataFormatter),
    new TableStore({
      tableColumns: expensesTableHeaders,
      defaultSelectedColumns: getSelectedColumns(PROJECT_DETAILS_EXPENSES_COLUMNS),
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

@Store('projectExpenses')
class ProjectExpenses extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectExpensesApi)
  ];

  @State
  showCreateForm = false;

  @State
  showUpdateForm = false;

  @State
  showPaymentForm = false
}

export const projectExpenses = new ProjectExpenses();
export const createExpensesForm = new CreateExpensesForm();
export const projectExpensesTable = new ProjectExpensesTable();
export const updateExpensesForm = new UpdateExpensesForm();
