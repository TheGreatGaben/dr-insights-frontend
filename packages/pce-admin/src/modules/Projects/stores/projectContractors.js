import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {State, Action} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {
  fetchProjectContractorsApi,
  deleteProjectContractorsApi,
  fetchProjectContractorDetailsApi,
  fetchPurchaseOrderApi,
  fetchVariationOrderApi,
  fetchContractorsApi,
} from '../apis';
import {
  projectContractorsTableHeaders,
  projectContractorsPOTableHeaders,
  projectContractorsVOTableHeaders,
  projectPODataFormatter,
  projectVODataFormatter
} from '../resources/projectContractors';
import {getSelectedColumns, PROJECT_DETAILS_CONTRACTOR_COLUMNS} from '@/consts/selectedColumns';

@Store('projectContractors')
class ProjectContractors extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectContractorsApi, []),
    new TableStore({
      tableColumns: projectContractorsTableHeaders,
      defaultSelectedColumns: getSelectedColumns(PROJECT_DETAILS_CONTRACTOR_COLUMNS),
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ];

  @State
  showCreateForm = false;

  @State
  showUpdateForm = false
}

@Store('autocompleteContractorList')
class AutoCompleteContractorList extends BaseStore {
  static mixins = [
    new ApiStore(fetchContractorsApi, []),
  ];
}

@Store('projectContractorsDelete')
class ProjectContractorsDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteProjectContractorsApi, []),
  ];

  @Action
  async deleteProjectContractors(contractor, successCallback) {
    try {
      await projectContractorsDelete.fetchData({
        uuid: contractor.uuid,
        project_id: contractor.project_id,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${contractor.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

@Store('ProjectContractorsDetails')
class ProjectContractorsDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectContractorDetailsApi, {}),
  ]
}

@Store('projectContractorsPO')
class ProjectContractorsPO extends BaseStore {
  static mixins = [
    new ApiStore(fetchPurchaseOrderApi, [], projectPODataFormatter),
    new TableStore({
      tableColumns: projectContractorsPOTableHeaders,
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    }),
  ];

  @State
  poListIsLoading = {};

  @State
  poListCache = {};

  @Action
  async fetchPOList(params) {
    const contractorID = params.contractor_id;
    const poListCache = this.poListCache.get();
    if (poListCache[contractorID]) {
      return poListCache[contractorID]
    } else {
      await this.fetchLatestPOList(params)
    }
  }

  @Action
  async fetchLatestPOList(params) {
    const contractorID = params.contractor_id;
    this.poListIsLoading.update({[contractorID]: true});
    await this.fetchData(params);
    const data = this.data.get();
    this.poListCache.update({[contractorID]: data});
    this.poListIsLoading.update({[contractorID]: false})
  }
}

@Store('projectContractorsVO')
class ProjectContractorsVO extends BaseStore {
  static mixins = [
    new ApiStore(fetchVariationOrderApi, [], projectVODataFormatter),
    new TableStore({
      tableColumns: projectContractorsVOTableHeaders,
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    }),
  ];

  @State
  voListIsLoading = {};

  @State
  voListCache = {};

  @Action
  async fetchVOList(params) {
    const contractorID = params.contractor_id;
    const voListCache = this.voListCache.get();
    if (voListCache[contractorID]) {
      return voListCache[contractorID]
    } else {
      await this.fetchLatestVOList(params)
    }
  }

  @Action
  async fetchLatestVOList(params) {
    const contractorID = params.contractor_id;
    this.voListIsLoading.update({[contractorID]: true});
    await this.fetchData(params);
    const data = this.data.get();
    this.voListCache.update({[contractorID]: data});
    this.voListIsLoading.update({[contractorID]: false})
  }
}

export const projectContractorsDelete = new ProjectContractorsDelete();
export const projectContractorsDetails = new ProjectContractorsDetails();
export const autoCompleteContractorList = new AutoCompleteContractorList();
export const projectContractors = new ProjectContractors();
export const projectContractorsPO = new ProjectContractorsPO();
export const projectContractorsVO = new ProjectContractorsVO();
