import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Action} from '@nixel/stores';
import {deleteExpensesApi} from '../apis';
import {apiErrorNotification} from '@/utils/request';

@Store('projectExpensesDelete')
class ProjectExpensesDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteExpensesApi, []),
  ];

  @Action
  async delete(expense, successCallback) {
    try {
      await projectExpensesDelete.fetchData({
        uuid: expense.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${expense.ref_no}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

export const projectExpensesDelete = new ProjectExpensesDelete();
