import Vue from 'vue'

import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {State} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
// import {defaultPaginationQuery} from '@/consts/apiRequest'
import {
  projectTableHeaders,
  projectSalesTableHeaders,
  projectPurchasesTableHeaders,
  projectCashflowDataFormatter
} from '../resources/projectList'
import {createProject, fetchProjectInvoicesApi, fetchProjectExpensesApi, fetchProjectCashflowProjectionApi} from '../apis'
import {ProjectForm} from './projectForm'
import {getProjectListSelectedColumns} from '@/consts/selectedColumns';
import {inProgressProjectStatus} from '../resources/projectStatuses';

const fetchProjectListApi = (params) => Vue.axios.get('/api/v2/projects', {
  params
});

@Store('createProjectForm')
class CreateProjectForm extends BaseStore {
  static mixins = [
    new ProjectForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Project created successfully.', {
            color: 'success',
            closeBtn: true,
          })
        }
      }
    }),
    new ApiStore(createProject),
  ]
}

@Store('projectSales')
class ProjectSales extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectInvoicesApi, []),
    new TableStore({
      tableColumns: projectSalesTableHeaders,
    })
  ];

  @State
  query = {};

  @State
  showSalesModal = false;
}

@Store('projectPurchases')
class ProjectPurchases extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectExpensesApi, []),
    new TableStore({
      tableColumns: projectPurchasesTableHeaders,
    })
  ];

  // @State
  // query = defaultPaginationQuery;

  @State
  showPurchasesModal = false;
}

@Store('projectCashflow')
class ProjectCashflow extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectCashflowProjectionApi, [], projectCashflowDataFormatter),
  ];

  @State
  showCashflowModal = false;
}

@Store('projectList')
class ProjectList extends BaseStore {
  static mixins = [
    new ApiStore(fetchProjectListApi, []),
    new TableStore({
      tableColumns: projectTableHeaders,
      defaultSelectedColumns: getProjectListSelectedColumns,
      options: {
        serverSitePagination: false,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    }),
  ];

  // Remove this if server-side pagination is to be used
  // (default pagination params from TableStore
  // would be used)
  @State
  query = {
    status: inProgressProjectStatus
  };
}

export const projectList = new ProjectList();
export const createProjectForm = new CreateProjectForm();
export const projectSales = new ProjectSales();
export const projectPurchases = new ProjectPurchases();
export const projectCashflow = new ProjectCashflow();
