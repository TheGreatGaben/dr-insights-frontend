import {get, compact} from 'lodash';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';
import {State} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {formHooksHandler} from '@nixel/stores';
import {date} from '@nixel/base/filters/datetime';
import {
  deletePurchaseOrderApi,
  updatePurchaseOrderApi,
  createPurchaseOrderApi} from '../apis';
import {apiErrorNotification} from '@/utils/request';

@Store('projectContractorPOForm')
class ProjectContractorPOForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        pos: {
          value: []
        },
        posToBeDeleted: {
          value: []
        },
      },
      hooks: {
        onSubmitFailed(formStore, error) {
          // update responseError to update nested fields
          this.responseError.assign(get(error, 'response.data.data.errors'));

          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error, 'An error occurred on the server')
        },
      }
    }),
  ];

  @State
  project_id = '';

  @State
  responseError = {};

  @State
  contractor_id = '';

  @Action
  setPOs(newPOs) {
    this.fields.get().pos.setValue(newPOs);
    this.fields.get().posToBeDeleted.setValue([])
  }

  @Action
  updatePO(index, key, newVal) {
    const pos = this.fields.get().pos.value;
    pos[index][key] = newVal;
    this.fields.get().pos.setValue(pos)
  }

  @Action
  addPO(po) {
    const pos = [...this.fields.get().pos.value];
    pos.push(po);
    this.fields.get().pos.setValue(pos)
  }

  @Action
  removePO(poToBeRemoved, index) {
    const pos = this.fields.get().pos.value;
    const filteredPOs = pos.filter((item, current) =>
      current !== index
    );
    this.fields.get().pos.setValue(filteredPOs);
    if (poToBeRemoved.uuid) {
      const posToBeDeleted = this.fields.get().posToBeDeleted.value;
      posToBeDeleted.push(poToBeRemoved);
      this.fields.get().posToBeDeleted.setValue(posToBeDeleted)
    }
  }

  @Action
  async submit(params) {
    this.project_id.assign(params.project_id);
    this.contractor_id.assign(params.contractor_id);
    const formStore = this;
    try {
      await Promise.all([formStore.createPOs(), formStore.updatePOs(), formStore.deletePOs()]);
      formHooksHandler('onSubmitSuccess', formStore, true)
    } catch (error) {
      formHooksHandler('onSubmitFailed', formStore, error);
      return Promise.reject(error)
    }
  }

  @Action
  async createPOs() {
    const pos = this.fields.get().pos.value;
    return new Promise(async (resolve, reject) => {
      for (const index in pos) {
        // noinspection JSUnfilteredForInLoop
        const po = pos[index];
        if (!po.uuid) {
          try {
            const newPO = {...po};
            newPO.project_id = this.project_id.get();
            newPO.contractor_id = this.contractor_id.get();
            await createPurchaseOrderApi(newPO)
          } catch (error) {
            // reject promise
            return reject(error)
          }
        }
      }

      // resolve promise
      resolve()
    })
  }

  @Action
  async updatePOs() {
    const pos = this.fields.get().pos.value;
    return new Promise(async (resolve, reject) => {
      for (const index in pos) {
        // noinspection JSUnfilteredForInLoop
        const po = pos[index];
        if (po.uuid) {
          const newPO = {...po};
          newPO.issued_date = date(po.issued_date); // reformat date string to Y-m-d
          try {
            await updatePurchaseOrderApi(newPO)
          } catch (error) {
            // reject promise
            return reject(error)
          }
        }
      }

      // resolve promise
      resolve()
    })
  }

  @Action
  deletePOs() {
    const posToBeDeleted = this.fields.get().posToBeDeleted.value;
    return new Promise(async (resolve, reject) => {
      for (const index in posToBeDeleted) {
        // noinspection JSUnfilteredForInLoop
        const po = posToBeDeleted[index];
        try {
          await deletePurchaseOrderApi(po);

          // noinspection JSUnfilteredForInLoop
          delete posToBeDeleted[index];

          this.updateValues({
            posToBeDeleted: compact(posToBeDeleted),
          });
        } catch (error) {
          // reject promise
          return reject(error)
        }
      }

      return resolve()

    });
  }
}

export const projectContractorPOForm = new ProjectContractorPOForm();
