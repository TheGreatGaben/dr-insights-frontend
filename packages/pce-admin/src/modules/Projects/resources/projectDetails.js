export const projectDetailsPaths = {
  projectDetails: {
    label: 'Project Details',
    path: (projectUuid) => `/projects/${projectUuid}`,
  },
  invoicesToCustomer: {
    label: 'Invoices to Customer',
    path: (projectUuid) => `/projects/${projectUuid}/invoices`,
  },
  contractors: {
    label: 'Purchase Orders',
    path: (projectUuid) => `/projects/${projectUuid}/purchase-orders`,
  },
  suppliers: {
    label: 'Suppliers / Sub-cons',
    path: (projectUuid) => `/projects/${projectUuid}/suppliers`,
  },
};
