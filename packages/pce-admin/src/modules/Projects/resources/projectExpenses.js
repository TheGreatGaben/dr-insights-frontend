import {date} from '@nixel/base/filters/datetime'
import {getTaxTypeByDate} from '@/consts/tax';

export const accountCodeDefaultQuery = {
  code: ['5-0000', '6-0000'],
}

export const expensesTableHeaders = [
  {
    text: 'Invoice Date',
    value: 'invoice_date',
    type: 'date',
    minWidth: '130px',
    align: 'left',
    sortable: true,
  },
  {
    text: 'Date Received',
    value: 'date_received',
    type: 'date',
    minWidth: '130px',
    align: 'left',
    sortable: true,
  },
  {
    text: 'Due Date',
    value: 'due_date',
    type: 'date',
    minWidth: '130px',
    align: 'left',
    sortable: true,
  },
  {
    text: 'Contractor',
    value: 'contractor_name',
    align: 'left',
    minWidth: '350px',
    required: true,
    sortable: true,
  },
  {
    text: 'Ref No.',
    value: 'ref_no',
    align: 'left',
    minWidth: '170px',
    sortable: true,
  },
  {
    text: 'Description',
    value: 'description',
    align: 'left',
    width: '350px',
    type: 'singleLine',
  },
  {
    text: 'Invoice Amount',
    value: 'total_expenses',
    type: 'currency',
    align: 'right',
    minWidth: '130px',
    sortable: true,
  },
  {
    text: 'Total Paid',
    value: 'total_paid_in_period',
    type: 'currency',
    align: 'right',
    minWidth: '130px',
    sortable: true,
  },
  {
    text: 'Balance',
    value: 'total_payable_in_period',
    type: 'currency',
    align: 'right',
    minWidth: '130px',
    sortable: true,
  },
  {
    text: 'Total Overpaid',
    value: 'total_overpaid_in_period',
    type: 'currency',
    align: 'right',
    minWidth: '130px',
    sortable: true,
  },
];

export const defaultExpensesPaymentPayload = Object.freeze({
  expenses_id: '',
  project_id: '',
  cash_flow: 'OUT',
  payment_date: new Date().toISOString().substr(0, 10),
  cheque_no: '',
  payment_amt: 0,
});

export function projectExpensesDataFormatter(response) {
  const expenses = response.data.data ? response.data.data : response.data;
  return expenses.map(expense => {
    return {
      povo_no: expense.po_no || expense.vo_no,
      ...expense,
      contractor: expense.contractor_name,
      invoice_date: date(expense.invoice_date),
      due_date: date(expense.due_date),
      date_received: date(expense.date_received),
      exclude_tax: !!(expense.exclude_tax),
      tax_type: getTaxTypeByDate(new Date(expense.invoice_date)),
      dc_note: (expense.dc_note >= 0) ? expense.dc_note : expense.dc_note * -1
    }
  })
}
