/**
 *
 * @type {RouterOptions[]}
 */
import {projectDetails} from '@/modules/Projects/stores/projectDetails';
import Projects from '@/modules/Projects/Projects';

export const PROJECT_LIST_PATH = '/projects/list';
export const PROJECT_CREATE_PATH = '/projects/list/create';
export const projectContractorDetailsPath = ({uuid, contractorUuid}) => `/projects/${uuid}/purchase-orders/${contractorUuid}`;

export const projectsRoutes = [
  {
    path: '/projects',
    name: 'Projects',
    meta: {
      authRequired: true,
      permissions: ['project.*'], // hide project page if user don't have permission to use
      title: 'Projects',
    },
    redirect: '/projects/list',
    component: Projects,
    children: [
      {
        path: 'list',
        name: 'Project List',
        meta: {
          authRequired: true,
          title: 'Projects List',
          icon: 'mdi-domain'
        },
        component: () => import(/* webpackChunkName: "projectList" */ './ProjectListStoreInjector.vue'),
        children: [
          {
            path: 'create',
            name: 'projectCreate',
            meta: {
              authRequired: true,
              title: 'Create Project',
            },
          },
          {
            path: ':uuid/sales',
            name: 'projectSales',
            meta: {
              authRequired: true,
              title: 'Project Sales',
            },
          },
          {
            path: ':uuid/purchases',
            name: 'projectPurchases',
            meta: {
              authRequired: true,
              title: 'Project Purchases',
            },
          },
          {
            path: ':uuid/cashflow/:days',
            name: 'projectCashflow',
            meta: {
              authRequired: true,
              title: 'Cashflow Projection',
            },
          },
        ],
      },
      {
        path: ':uuid',
        hidden: true,
        meta: {
          authRequired: true,
          title: 'Projects Details',
          asyncTitle() {
            return projectDetails.name.stateName;
          },
          // asyncTitle: 'projectDetails/name',
        },
        component: () => import(/* webpackChunkName: "projectDetails" */ './ProjectDetailsStoreInjector.vue'),
        children: [
          {
            path: '',
            name: 'projectDetailsUpdate',
            meta: {
              authRequired: true,
              title: 'Details',
            },
            component: () => import(/* webpackChunkName: "ProjectDetailsUpdate" */ './components/ProjectUpdateForm.vue'),
          },
          {
            path: 'invoices',
            name: 'projectInvoices',
            meta: {
              authRequired: true,
              title: 'Invoices',
            },
            component: () => import(/* webpackChunkName: "ProjectDetailsInvoice" */ './ProjectDetailsInvoicesStoreInjector.vue'),
          },
          {
            path: 'purchase-orders',
            name: 'projectContractors',
            meta: {
              authRequired: true,
              title: 'Purchase Orders',
            },
            component: () => import(/* webpackChunkName: "ProjectDetailsContractors" */ './ProjectDetailsContractorsStoreInjector.vue'),
            children: [
              {
                path: 'create',
                name: 'createProjectContractors',
                meta: {
                  authRequired: true,
                  title: 'Create Purchase Orders',
                },
              },
              {
                path: ':contractorUuid',
                name: 'updateProjectContractors',
                meta: {
                  authRequired: true,
                  title: 'Update Purchase Orders',
                },
              },
            ]
          },
          {
            path: 'suppliers',
            name: 'projectSuppliers',
            meta: {
              authRequired: true,
              title: 'Suppliers',
            },
            component: () => import(/* webpackChunkName: "ProjectDetailsSuppliers" */ './components/projectDetails/expenses/ProjectDetailsExpensesStoreInjector.vue'),
          },
        ]
      },
      {
        path: '*',
        redirect: '/projects/list',
      }
    ]
  }
];
