import Vue from 'vue'
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {Getter} from '@nixel/stores';
import {State} from '@nixel/stores';
import {aggregator} from '@nixel/base/utils/math';

const fetchPendingInvoicesApi = (params) => Vue.axios.get('/api/v2/invoices?status=pending', {params});
export const createPaymentApi = (params) => Vue.axios.post('/api/v2/payments', params);
export const writeOffInvoiceApi = (params) => Vue.axios.put(`/api/v2/write-off/invoices/${params.uuid}`, params);

@Store('pendingInvoicesForm')
class PendingInvoicesForm extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        invoices: {
          value: []
        },
      },
    })
  ];

  @Getter
  totalReceived() {
    return aggregator(this.fields.get().invoices.value, 'payment_amt')
  }
}

@Store('pendingInvoices')
class PendingInvoices extends BaseStore {

  @State
  invoiceToWriteOff = {};

  static mixins = [
    new ApiStore(fetchPendingInvoicesApi, []),
    new FormStore({
      fields: {
        reason: {
          value: '',
          placeholder: 'If yes, please give a reason for writing it off.',
          label: 'Reason',
          rules: 'required|max:50',
        },
      },
    }),
    new TableStore({
      // TODO: handle if column values are undefined
      // to eliminate input columns
      defaultSelectedColumns: [
        'project_name',
        'invoice_date',
        'cert_no',
        'client',
        'due_date',
        'total_invoiced',
        'total_receivable',
      ],
      tableColumns: [
        {
          text: 'Project',
          sortable: true,
          value: 'project_name',
          width: '10%',
          align: 'left',
          type: 'singleLine',
        },
        {
          text: 'Invoice Date',
          sortable: true,
          value: 'invoice_date',
          type: 'date',
          align: 'left',
        },
        {
          text: 'Cert No.',
          sortable: true,
          value: 'cert_no',
          width: '100px',
          align: 'left',
        },
        {
          text: 'Client',
          sortable: true,
          value: 'client_name',
          width: '10%',
          align: 'left',
          type: 'singleLine',
        },
        {
          text: 'Due Date',
          sortable: true,
          value: 'due_date',
          type: 'date',
          align: 'right',
        },
        {
          text: 'Overdue (days)',
          sortable: true,
          value: 'overdue',
          align: 'right',
        },
        {
          text: 'Invoice Amount',
          sortable: true,
          value: 'total_invoiced',
          tag: 'p',
          type: 'currency',
          align: 'right',
        },
        {
          text: 'Receivable Amount',
          sortable: true,
          value: 'total_receivable',
          tag: 'p',
          type: 'currency',
          align: 'right',
        },

        // Inputs
        {
          text: 'Received',
          value: 'received',
          class: 'pl-2',
          headerClass: 'pl-2',
        },
        {
          text: 'Payment Date',
          value: 'payment_date',
          minWidth: '120px',
          class: 'px-2',
          headerClass: 'px-2',
        },
        {
          text: 'Cheque No.',
          value: 'cheque_no',
          minWidth: '100px',
          class: 'px-2',
          headerClass: 'px-2',
        },

        {
          text: 'Write Off',
          value: 'write_off',
        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

export const pendingInvoices = new PendingInvoices();
export const pendingInvoicesForm = new PendingInvoicesForm();

