import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchWrittenOffInvoicesApi = (params) => Vue.axios.get('/api/v2/invoices?status=writeOff', {params});
export const undoWrittenOffInvoicesApi = (params) => Vue.axios.delete(`/api/v2/write-off/invoices/${params.uuid}`);

@Store('writtenOffInvoices')
class WrittenOffInvoices extends BaseStore {
  static mixins = [
    new ApiStore(fetchWrittenOffInvoicesApi, []),
    new TableStore({
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      },
      tableColumns: [
        {
          text: 'Project',
          sortable: true,
          value: 'project_name',
          width: '10%',
          align: 'left',
          type: 'singleLine',
        },
        {
          text: 'Invoice Date',
          sortable: true,
          value: 'invoice_date',
          type: 'date',
          align: 'left',
        },
        {
          text: 'Cert No.',
          sortable: true,
          value: 'cert_no',
          width: '100px',
          align: 'left',
        },
        {
          text: 'Client',
          sortable: true,
          value: 'client_name',
          width: '10%',
          align: 'left',
          type: 'singleLine',
        },
        {
          text: 'Receivable Written Off',
          sortable: true,
          value: 'write_off_amt',
          tag: 'p',
          type: 'currency',
          align: 'right',
        },
        {
          text: 'Write Off Date',
          sortable: true,
          value: 'write_off_date',
          type: 'date',
          align: 'right',
        },
        {
          text: 'Write Off Reason',
          value: 'write_off_reason',
          align: 'right',
        },
      ]
    })
  ]
}

export const writtenOffInvoices = new WrittenOffInvoices();
