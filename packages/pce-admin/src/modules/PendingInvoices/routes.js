/**
 *
 * @type {RouterOptions[]}
 */
export const pendingInvoicesRoutes = [
  {
    path: '/pending-invoices',
    name: 'PendingInvoices',
    meta: {
      authRequired: true,
      permissions: ['project.invoice'], // hide reports page if user don't have permission to use
      title: 'Pending Invoices',
      icon: 'mdi-playlist-check',
    },
    redirect: '/pending-invoices/list',
    component: () => import(/* webpackChunkName: "pendingInvoices" */ './PendingInvoices.vue'),
    children: [
      {
        path: 'list',
        name: 'PendingInvoicesList',
        meta: {
          authRequired: true,
          title: 'Pending Invoices List',
        },
        component: () => import(/* webpackChunkName: "pendingInvoicesList" */ './PendingInvoicesList.vue'),
        children: [
          {
            path: ':uuid/write-off',
            name: 'invoiceWriteOff',
            meta: {
              authRequired: true,
              title: 'Invoice Write Off',
            },
          },
        ],
      },
      {
        path: 'write-off-invoices',
        name: 'WrittenOffInvoices',
        meta: {
          authRequired: true,
          title: 'Write Offs',
        },
        component: () => import(/* webpackChunkName: "writtenOffInvoicesList" */ './WriteOffList.vue'),
      },
    ]
  },
];
