/**
 *
 * @type {RouterOptions[]}
 */
import Contractors from './Contractors';

export const contractorsRoutes = [
  {
    path: '/contractors',
    name: 'Contractors',
    meta: {
      authRequired: true,
      permissions: ['contractor.*'], // hide contractors page if user don't have permission to use
      title: 'Contractors',
    },
    redirect: '/contractors/list',
    component: Contractors,
    children: [
      {
        path: 'list',
        name: 'ContractorList',
        meta: {
          authRequired: true,
          title: 'Contractor List',
          icon: 'mdi-worker'
        },
        component: () => import(/* webpackChunkName: "contractorList" */ './ContractorList.vue'),
      },
      {
        path: '*',
        redirect: '/contractors/list',
      }
    ]
  }
];
