import Vue from 'vue'
import {isArray, get, map} from 'lodash';
import {apiErrorNotification} from '@/utils/request';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';
import {confirmOnce} from '@/plugins/confirmOnce';

const deleteContractorApi = (params) => Vue.axios.delete(`/api/v2/contractors/${params.uuid}`);

@Store('contractorDelete')
class ContractorDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteContractorApi, []),
  ];

  @Action
  async deleteContractor(contractor, successCallback) {
    try {
      await contractorDelete.fetchData({
        uuid: contractor.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${contractor.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      const contractorIdError = get(error, 'response.data.data.errors.contractor_id[0]');
      const projectNames = get(error, 'response.data.data.errors.projects');

      if (contractorIdError && isArray(projectNames)) {
        await confirmOnce(`The contractor appears in ${map(projectNames, (project) => project[0].name).join(', ')}. You need to delete them from these projects to proceed.`, {
          buttonTrueText: 'Got it',
          buttonFalseText: '',
          title: 'Delete Contractor Error.',
          color: 'error',
        });
      }

      apiErrorNotification(contractorIdError || error);
    }
  }
}

export const contractorDelete = new ContractorDelete();

