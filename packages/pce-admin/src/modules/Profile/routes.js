import {lazyLoadView} from '../../utils/router';
import Layout from '../../views/layout/Layout';

export const profileRoutes = [
  {
    path: '/profile',
    name: 'profile',
    hidden: true,
    meta: {
      title: 'Profile',
      icon: 'mdi-account-circle-outline',
    },
    redirect: '/profile/change-password',
    component: Layout,
    children: [
      {
        path: 'change-password',
        name: 'changePassword',
        meta: {
          title: 'Change Password',
          icon: 'mdi-lock'
        },
        component: () => lazyLoadView(import('./ChangePassword.vue')),
      },
    ]
  },
];
