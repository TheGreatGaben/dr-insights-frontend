/** When your routing table is too long, you can split it into small modules**/

import PrintLayout from './PrintLayout'

export const documentPrintingRoutes = {
  path: '/document-printing',
  component: PrintLayout,
  redirect: '/document-printing/invoice',
  name: 'DocumentPrinting',
  hidden: true,
  children: [
    {
      path: 'invoice',
      component: () => import('@/modules/DocumentPrinting/ProjectInvoice'),
      name: 'ProjectInvoicePrint',
    },
    /*
    {
      path: 'po',
      component: () => import('@/modules/DocumentPrinting/PurchaseOrder'),
      name: 'PurchaseOrderPrint',
    },*/
  ]
};
