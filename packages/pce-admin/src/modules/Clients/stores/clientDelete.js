import Vue from 'vue'
import {apiErrorNotification} from '@/utils/request';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';

const deleteClientApi = (params) => Vue.axios.delete(`/api/v2/clients/${params.uuid}`);

@Store('clientDelete')
class ClientDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteClientApi, []),
  ];

  @Action
  async deleteClient(client, successCallback) {
    try {
      clientDelete.fetchData({
        uuid: client.uuid,
      });

      successCallback();

      this._vm.$toast(`Successfully deleted "${client.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

export const clientDelete = new ClientDelete();

