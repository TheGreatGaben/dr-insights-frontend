import Vue from 'vue';
import {BaseStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Getter, Nested, Store} from '@nixel/stores';
import {ClientForm} from './clientForm';
import {apiErrorNotification} from '@/utils/request';

const createClientDetailsApi = (params) => Vue.axios.post('/api/v2/clients', params);

const updateClientDetailsApi = (params) => Vue.axios.put(`/api/v2/clients/${params.uuid}`, params);

const fetchClientDetailsApi = (params) => Vue.axios.get(`/api/v2/clients/${params.uuid}`, {
  params
});

@Store('createClientForm')
class CreateClientForm extends BaseStore {
  static mixins = [
    new ApiStore(createClientDetailsApi),
    new ClientForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully created client.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}
@Store('updateClientForm')
class UpdateClientForm extends BaseStore {
  static mixins = [
    new ApiStore(updateClientDetailsApi),
    new ClientForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update client.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}

@Store('clientDetails')
class ClientDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchClientDetailsApi, {}),
  ];

  @Nested
  createClientForm = new CreateClientForm();

  @Nested
  updateClientForm = new UpdateClientForm();

  @Getter
  name(state) {
    return state.data && state.data.name
  }

}

export const clientDetails = new ClientDetails();

