import Vue from 'vue';

export const fetchClientsMYOBApi = (params) => Vue.axios.get('/api/v2/clients/myob', {
  params
});
