import Vue from 'vue';

export function loginApi(formFields) {
  return Vue.axios.post('/oauth/token', {
    ...formFields,
    grant_type: 'password',
    client_id: process.env.VUE_APP_CLIENT_ID,
    client_secret: process.env.VUE_APP_CLIENT_SECRET,
  })
}

export function resetPasswordApi(payload) {
  return Vue.axios.post('/api/password/reset', payload)
}

export function changePasswordApi(payload) {
  return Vue.axios.post('/api/password/change', payload)
}
