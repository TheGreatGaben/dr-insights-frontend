import {BaseStore} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {apiErrorNotification} from '@/utils/request';
import {resetPasswordApi} from '../apis';

@Store('resetPassword')
class ResetPasswordStore extends BaseStore {
  static mixins = [
    new FormStore({
      fields: {
        password: {
          value: '',
          label: 'Password',
          placeholder: 'Set your new password here.',
          type: 'password',
          rules: 'required|minLength:6',
        },
        password_confirmation: {
          value: '',
          label: 'Password Confirmation',
          placeholder: '',
          type: 'password',
          rules: 'required|minLength:6|is:password',
        },
        email: {
          value: '',
          type: 'email',
        },
        token: {
          value: '',
          type: 'text',
        },
      },
      options: {
        resetFormOnSubmitSuccess: true,
      },
      hooks: {
        onSubmitFailed(formStore, response) {
          apiErrorNotification(response)
        },
      }
    }),
    new ApiStore(resetPasswordApi)
  ]
}

export const resetPasswordStore = new ResetPasswordStore();
