import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {organisationTableHeaders} from '../resources/organisationList';

const fetchOrganisationListApi = (params) => Vue.axios.get('/api/v2/organisations', {
  params
});

@Store('organisationTableList')
class OrganisationTableList extends BaseStore {
  static mixins = [
    new ApiStore(fetchOrganisationListApi, []),
    new TableStore({
      tableColumns: organisationTableHeaders,
      options: {
        selectableRows: true,
      }
    })
  ]
}

export const organisationTableList = new OrganisationTableList();

