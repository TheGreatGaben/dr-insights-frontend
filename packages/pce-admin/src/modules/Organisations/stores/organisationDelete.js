import Vue from 'vue'
import {apiErrorNotification} from '@/utils/request';
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {Action} from '@nixel/stores';

const deleteOrganisationApi = (params) => Vue.axios.delete(`/api/v2/organisations/${params.uuid}`);

@Store('organisationDelete')
class OrganisationDelete extends BaseStore {
  static mixins = [
    new ApiStore(deleteOrganisationApi, []),
  ];

  @Action
  async deleteOrganisation(organisation, successCallback) {
    try {
      organisationDelete.fetchData({
        uuid: organisation.uuid,
      });

      successCallback();

      // noinspection ES6MissingAwait
      this._vm.$toast(`Successfully deleted "${organisation.name}"`, {
        color: 'success',
        closeBtn: true,
      })
    } catch (error) {
      console.log(error);
      apiErrorNotification(error);
    }
  }
}

export const organisationDelete = new OrganisationDelete();

