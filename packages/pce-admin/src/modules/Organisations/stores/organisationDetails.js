import Vue from 'vue';
import {BaseStore} from '@nixel/stores';
import {ApiStore} from '@nixel/stores';
import {Getter, Nested, Store} from '@nixel/stores';
import {OrganisationForm} from './organisationForm';
import {apiErrorNotification} from '@/utils/request';

const createOrganisationDetailsApi = (params) => Vue.axios.post('/api/v2/organisations', params);

const updateOrganisationDetailsApi = (params) => Vue.axios.put(`/api/v2/organisations/${params.uuid}`, params);

const fetchOrganisationDetailsApi = (params) => Vue.axios.get(`/api/v2/organisations/${params.uuid}`, {
  params
});

@Store('createOrganisationForm')
class CreateOrganisationForm extends BaseStore {
  static mixins = [
    new ApiStore(createOrganisationDetailsApi),
    new OrganisationForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully created organisation.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}
@Store('updateOrganisationForm')
class UpdateOrganisationForm extends BaseStore {
  static mixins = [
    new ApiStore(updateOrganisationDetailsApi),
    new OrganisationForm({
      hooks: {
        onSubmitSuccess(formStore) {
          formStore._vm.$toast('Successfully update organisation.', {
            color: 'success'
          });
        },
        onSubmitFailed(formStore, error) {
          formStore.mapBackendErrorsToFields();

          // throw api errors
          apiErrorNotification(error)
        },
      }
    }),
  ]
}

@Store('organisationDetails')
class OrganisationDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchOrganisationDetailsApi, {}),
  ];

  @Nested
  createOrganisationForm = new CreateOrganisationForm();

  @Nested
  updateOrganisationForm = new UpdateOrganisationForm();

  @Getter
  name(state) {
    return state.data && state.data.name
  }

}

export const organisationDetails = new OrganisationDetails();

