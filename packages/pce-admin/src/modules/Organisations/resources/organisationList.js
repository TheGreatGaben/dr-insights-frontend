/**
 *
 * @type {TableColumn[]}
 */
export const organisationTableHeaders = [
  {
    align: 'center',
    type: 'expand',
    value: '_expand',
    width: '10px',
  },
  {
    text: 'No.',
    type: 'index',
    align: 'left',
    width: '80px',
  },
  {
    text: 'Organisation\'s Name',
    sortable: true,
    value: 'name',
    align: 'left',
  },
];
