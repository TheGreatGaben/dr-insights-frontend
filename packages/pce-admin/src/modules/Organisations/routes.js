/**
 *
 * @type {RouterOptions[]}
 */
import Organisations from './Organisations';

export const organisationsRoutes = [
  {
    path: '/organisations',
    name: 'Organisations',
    meta: {
      authRequired: true,
      // permissions: ['organisation.*'], // hide organisations page if user don't have permission to use
      title: 'Organisations',
    },
    redirect: '/organisations/list',
    component: Organisations,
    children: [
      {
        path: 'list',
        name: 'OrganisationList',
        meta: {
          authRequired: true,
          title: 'Organisation List',
          icon: 'mdi-worker'
        },
        component: () => import(/* webpackChunkName: "organisationList" */ './OrganisationList.vue'),
      },
      {
        path: '*',
        redirect: '/organisations/list',
      }
    ]
  }
];
