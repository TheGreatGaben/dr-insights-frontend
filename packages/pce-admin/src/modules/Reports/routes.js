import Reports from './Reports';

/**
 *
 * @type {RouterOptions[]}
 */
export const reportsRoutes = [
  {
    path: '/reports',
    name: 'Reports',
    redirect: '/reports/invoices',
    meta: {
      authRequired: true,
      permissions: ['client.*'], // hide reports page if user don't have permission to use
      title: 'Reports',
      icon: 'mdi-file-chart'
    },
    component: Reports,
    children: [
      {
        path: 'invoices',
        name: 'Report Invoice List',
        meta: {
          authRequired: true,
          title: 'Invoice List',
        },
        component: () => import(/* webpackChunkName: "invoiceReportList" */ './InvoiceReportList.vue'),
      },
      {
        path: 'expenses',
        name: 'Report Expense List',
        meta: {
          authRequired: true,
          title: 'Expense List',
        },
        component: () => import(/* webpackChunkName: "expenseReportList" */ './ExpenseReportList.vue'),
      },
      {
        path: 'purchase_orders',
        name: 'Report Purchase Order List',
        meta: {
          authRequired: true,
          title: 'Purchase Order List',
        },
        component: () => import(/* webpackChunkName: "purchaseOrderReportList" */ './PurchaseOrderReportList.vue'),
      },
      {
        path: 'payments',
        name: 'Report Payment List',
        meta: {
          authRequired: true,
          title: 'Payment List',
        },
        component: () => import(/* webpackChunkName: "paymentReportList" */ './PaymentReportList.vue'),
      },
      {
        path: '*',
        redirect: '/reports/clients',
      }
    ]
  }
];
