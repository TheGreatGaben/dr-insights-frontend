import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchExpenseReportListApi = (params) => Vue.axios.get('/api/v2/expenses', {params});

@Store('expenseReports')
class ExpenseReports extends BaseStore {
  static mixins = [
    new ApiStore(fetchExpenseReportListApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'No.',
          type: 'index',
          align: 'left',
          width: '80px',
          sortable: true,
        },
        {
          text: 'Project',
          value: 'project_name',
          align: 'left',
          type: 'singleLine',
          width: '15%',
          sortable: true,
        },
        {
          text: 'Contractor',
          value: 'contractor_name',
          width: '15%',
          type: 'singleLine',
          align: 'left',
          sortable: true,
        },
        {
          text: 'Ref No.',
          value: 'ref_no',
          align: 'center',
          width: '80px',
          sortable: true,
        },
        {
          text: 'Total Expenses',
          value: 'total_expenses',
          type: 'currency',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Total Paid',
          value: 'total_paid_in_period',
          type: 'currency',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Total Payable',
          value: 'total_payable',
          type: 'currency',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Invoice Date',
          value: 'invoice_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Date Received',
          value: 'date_received',
          type: 'date',
          align: 'right',
          sortable: true,
        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

export const expenseReports = new ExpenseReports();

