import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchPurchaseOrderReportListApi = (params) => Vue.axios.get('/api/v2/purchaseOrders', {params});

@Store('purchaseOrderReports')
class PurchaseOrderReports extends BaseStore {
  static mixins = [
    new ApiStore(fetchPurchaseOrderReportListApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'Purchase Order',
          value: 'po_no',
          width: '60px',
          align: 'left',
          sortable: true,
        },
        {
          text: 'Project',
          value: 'project_name',
          width: '10%',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Contractor',
          value: 'contractor_name',
          width: '10%',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Budget',
          value: 'budget',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'DC Note',
          value: 'dc_note',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'Tax Amount', // could be sst or gst or could be excluded `exclude_tax`
          value: 'tax',
          align: 'right',
          formatter(po) {
            return po.sst || po.gst;
          },
          sortable: true,
        },
        {
          text: 'Total Value',
          value: 'total_value',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'Issued Date',
          value: 'issued_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

export const purchaseOrderReports = new PurchaseOrderReports();

