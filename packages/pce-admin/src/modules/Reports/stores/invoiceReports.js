import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

const fetchInvoiceListApi = (params) => Vue.axios.get('/api/v2/invoices', {params});

@Store('invoiceReports')
class InvoiceReports extends BaseStore {
  static mixins = [
    new ApiStore(fetchInvoiceListApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'No.',
          type: 'index',
          align: 'left',
          width: '80px',
        },
        {
          text: 'Client',
          sortable: true,
          value: 'client_name',
          align: 'left',
          type: 'singleLine',
          width: '10%',
        },
        {
          text: 'Project',
          value: 'project_name',
          align: 'left',
          type: 'singleLine',
          width: '15%',
          sortable: true,
        },
        // cert_no: "PC#4"
        // client: "Malaysian Resources Corporation Berhad"
        // debit_note: "0.00"
        // description: null
        // due_date: "2016-06-29 00:00:00"
        // exclude_tax: 0
        // id: "126"
        // invoice_date: "2016-04-30 00:00:00"
        // invoice_no: "0516-094"
        // name: "DESARU WESTIN"
        // payment_details: [{uuid: "509c58db-f93f-11e8-af56-00163e001aed", cheque_no: "", payment_date: "2016-06-14",…}]
        // prev_certified: "-651156.59"
        // retention: "-125612.64"
        // retention_released: "0.00"
        // tax_amt: "28761.43"
        // total_advance_received: "0.00"
        // total_invoiced: "479357.20"
        // total_receivable: "0.00"
        // total_received: "479357.20"
        // total_received_in_period: "479357.20"
        // total_received_include_tax
        {
          text: 'Cert no.',
          value: 'cert_no',
          align: 'left',
          width: '120px',
          sortable: true,
        },
        {
          text: 'Invoice no.',
          value: 'invoice_no',
          align: 'left',
          width: '120px',
          sortable: true,
        },
        {
          text: 'Invoiced amount',
          value: 'total_invoiced',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'Total Received',
          value: 'total_received_in_period',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'Debit Note',
          value: 'debit_note',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'Total Receivable',
          value: 'total_receivable',
          align: 'right',
          type: 'currency',
          tag: 'p',
          sortable: true,
        },
        {
          text: 'Invoice Date',
          value: 'invoice_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Due Date',
          value: 'due_date',
          type: 'date',
          align: 'right',
          sortable: true,
        },
      ],
      options: {
        filterTableColumns: true,
        listMapWithTableColumns: true,
      }
    })
  ]
}

export const invoiceReports = new InvoiceReports();

