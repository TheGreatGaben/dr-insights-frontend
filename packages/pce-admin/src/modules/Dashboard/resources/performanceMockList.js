export const cashflowProgressColumns = [
  {
    text: '',
    value: 'type',
    align: 'left',
    type: 'singleLine',
  },
  {
    text: 'MonthName',
    value: 'feb',
    type: 'percentage',
    align: 'right',
  },
];

export const cashflowPerformanceColumns = [
  {
    text: '',
    value: 'type',
    align: 'left',
    type: 'singleLine',
  },
  {
    text: 'MonthName',
    value: 'feb',
    type: 'currency',
    align: 'right',
  },
];

export const cashflowPerformance = [
  {
    type: 'Cashflow In',
    feb: 100000,
    mar: 30000,
    apr: 40000,
    may: 45000,
    jun: 80000,
    jul: 60000,
  },
  {
    type: 'Cashflow Out',
    feb: 20000,
    mar: 50000,
    apr: 32000,
    may: 30000,
    jun: 50000,
    jul: 50000,
  },
  {
    type: 'Cashflow Net',
    feb: 80000,
    mar: -20000,
    apr: 31000,
    may: 15000,
    jun: 30000,
    jul: 10000,
  },
];

export const cashflowProgress = [
  {
    type: 'Estimate',
    feb: 5,
    mar: 5,
    apr: 5,
    may: 5,
    jun: 5,
    jul: 5,
  },
  {
    type: 'Actual',
    feb: 6.3,
    mar: 3,
    apr: 5.4,
    may: 5.3,
    jun: 5.8,
    jul: 6.4,
  },
  {
    type: 'Net',
    feb: 1.3,
    mar: -2,
    apr: 0.6,
    may: 0.3,
    jun: 0.8,
    jul: 1.4,
  },
];
