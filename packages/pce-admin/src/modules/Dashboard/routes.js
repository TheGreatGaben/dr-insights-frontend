/**
 *
 * @type {RouterOptions[]}
 */
export const dashboardRoutes = [
  {
    path: '/',
    name: 'dashboard',
    meta: {
      authRequired: true,
      title: 'Dashboard',
      icon: 'mdi-chart-line'
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "dashboardStore" */ './Dashboard.vue'),
    children: [
      {
        path: '/',
        name: 'Performance Overview',
        meta: {
          authRequired: true,
          title: 'Performance Overview',
        },
        component: () => import(/* webpackChunkName: "performance" */ './PerformanceStoreInjector.vue'),
      },
      {
        path: 'cashflow',
        name: 'Cashflow Overview',
        meta: {
          authRequired: true,
          title: 'Cashflow Overview',
        },
        component: () => import(/* webpackChunkName: "cashflowOverview" */ './CashflowOverviewStoreInjector.vue'),
      },
      {
        path: 'overdue-invoices',
        name: 'Overdue Invoices',
        meta: {
          authRequired: true,
          title: 'Overdue Invoices',
        },
        component: () => import(/* webpackChunkName: "overdueInvoices" */ './OverDueInvoicesStoreInjector.vue'),
      },
    ],
  },
];
