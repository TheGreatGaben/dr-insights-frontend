import Vue from 'vue'
import {ApiStore, BaseStore, Nested, State, Store} from '@nixel/stores';
import {ExpensesList} from './dashboardExpensesList';
import {InvoiceList} from './dashboardInvoiceList';

export const dashboardSummaryApi = (params) => Vue.axios.get('api/v2/dashboard', {
  params,
});

@Store('thisMonthDashboardSummary')
class ThisMonthSummary extends BaseStore {
  static mixins = [
    new ApiStore(dashboardSummaryApi, {})
  ];

  @State
  data = {
    invoice: {
      total_received: 0,
      total_received_in_period: 0,
      total_invoiced: 0,
      total_receivable: 0,
    },
    expense: {
      total_paid: 0,
      total_paid_in_period: 0,
      total_expenses: 0,
      total_payable: 0,
    },
    payment: {
      cashflow_nett: 0,
      projected_cashflow_nett: 0,
    }
  }
}

@Store('nextMonthDashboardSummary')
class NextMonthSummary extends BaseStore {
  static mixins = [
    new ApiStore(dashboardSummaryApi, {})
  ];

  @State
  data = {
    invoice: {
      total_received: 0,
      total_received_in_period: 0,
      total_invoiced: 0,
      total_receivable: 0,
    },
    expense: {
      total_paid: 0,
      total_paid_in_period: 0,
      total_expenses: 0,
      total_payable: 0,
    },
    payment: {
      cashflow_nett: 0,
      projected_cashflow_nett: 0,
    }
  }
}

@Store('dashboardSummary')
class DashboardSummary extends BaseStore {
  @Nested
  thisMonthDashboardSummary = new ThisMonthSummary();

  @Nested
  nextMonthDashboardSummary = new NextMonthSummary();

  @Nested
  invoiceList = new InvoiceList([
    {
      text: 'Total Invoiced',
      value: 'total_invoiced',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
    {
      text: 'Total Received',
      value: 'total_received_in_period',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
    {
      text: 'Total Receivable',
      value: 'total_receivable_in_period',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
    {
      text: 'Advance Receipt',
      value: 'total_advance_received_in_period',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
  ]);

  @Nested
  expensesList = new ExpensesList([
    {
      text: 'Total Expenses',
      value: 'total_expenses',
      type: 'currency',
      align: 'right',
      sortable: true,
    },
    {
      text: 'Total Paid',
      value: 'total_paid_in_period',
      type: 'currency',
      align: 'right',
      sortable: true,
    },
    {
      text: 'Total Payable',
      value: 'total_payable_in_period',
      type: 'currency',
      align: 'right',
      sortable: true,
    },
    {
      text: 'Total Overpaid',
      value: 'total_overpaid_in_period',
      align: 'right',
      type: 'currency',
      sortable: true,
    },
  ]);
}

export const dashboardSummary = new DashboardSummary();
