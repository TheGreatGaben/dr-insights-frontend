import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {ROLES} from '@/consts/roles';

const getProjectManagerList = () => Vue.axios.get(`/api/v2/users/role/${ROLES.PROJECT_MANAGER}`);

@Store('projectManagerList')
class ProjectManagerList extends BaseStore {
  static mixins = [
    new ApiStore(getProjectManagerList, []),
  ]
}

export const projectManagerList = new ProjectManagerList();
