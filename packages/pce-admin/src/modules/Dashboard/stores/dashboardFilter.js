import {Action, Getter, State} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
// import {dateFilter} from '../resources/dateFilter';
import {dashboardProjectList} from './dashboardProjectList';
import {date} from '@nixel/base/filters/datetime';
import {omit} from 'lodash';
import {getDate, getMonth, getYear, lastDayOfMonth, setMonth, startOfMonth} from 'date-fns';
import {dashboardSummary} from './dashboardSummary';
import {apiErrorNotification} from '@/utils/request';
import {dashboardProjectDetails} from './dashboardProjectDetails';
import {projectManagerList} from './projectManagerList';

export const today = date(new Date());
export const defaultDate = date(startOfMonth(new Date()));
export const firstDayOfThisYear = `${getYear(defaultDate)}-01-01`;

// declare it here because this year query won't change
export const thisYearOverviewQuery = {
  'payment_date[start]': firstDayOfThisYear,
  'payment_date[end]': today,
};

@Store('dashboardFilter')
class DashboardFilter extends BaseStore {
  @State
  thisMonth = {};

  @State
  nextMonth = {};

  @State
  lastDayOfThisMonthInDate;

  @State
  lastDayOfNextMonthInDate;

  @State
  dateFilter = date(defaultDate);

  @State
  projectManager = '';

  @Getter
  thisMonthQuery() {
    return {
      ...dashboardSummary.thisMonthDashboardSummary.query.get(),
      project_manager: this.projectManager.get(),
      'due_date[start]': this.dateFilter.get(),
      'due_date[end]': this.thisMonth.get().lastDayOfMonth,
    }
  }

  @Getter
  nextMonthQuery() {
    return {
      ...dashboardSummary.nextMonthDashboardSummary.query.get(),
      project_manager: this.projectManager.get(),
      'due_date[start]': this.dateFilter.get(),
      'due_date[end]': this.nextMonth.get().lastDayOfMonth,
    }
  }

  @Getter
  paymentDateQuery() {
    return {
      project_manager: this.projectManager.get(),
      'payment_date[start]': this.dateFilter.get(),
      'payment_date[end]': defaultDate,
    }
  }

  @Getter
  selectedProjectManager() {
    const selectedProjectManager = this.projectManager.get();
    const list = projectManagerList.data ? projectManagerList.data.get() : [];

    return list.find(item => item.id === selectedProjectManager);
  }

  @Action
  setDate(newDate = defaultDate) {
    this.dateFilter.assign(newDate);

    // const thisMonthInNumber = parseInt(format(defaultDate, 'M'));
    // const nextMonthInNumber = thisMonthInNumber + 1;
    const thisMonthDate = new Date();
    const nextMonthDate = setMonth(thisMonthDate, getMonth(thisMonthDate) + 1);
    const lastDayOfThisMonth = lastDayOfMonth(thisMonthDate);
    const lastDayOfNextMonth = lastDayOfMonth(nextMonthDate);

    this.lastDayOfThisMonthInDate.assign(getDate(lastDayOfThisMonth));
    this.lastDayOfNextMonthInDate.assign(getDate(lastDayOfNextMonth));

    this.thisMonth.assign({
      // number: thisMonthInNumber,
      // string: format(defaultDate, 'MMM'),
      // date: defaultDate,
      lastDayOfMonth: date(lastDayOfThisMonth),
    });

    this.nextMonth.assign({
      // number: nextMonthInNumber,
      // string: format(nextMonthDate, 'MMM'),
      // date: nextMonthDate,
      lastDayOfMonth: date(lastDayOfNextMonth),
    });
  }

  // =============================== Cashflow ==================================
  async fetchThisMonthSummary() {
    const thisMonthDateFilter = this.thisMonthQuery();

    try {
      await dashboardSummary.thisMonthDashboardSummary.fetchData(thisMonthDateFilter);
    } catch (e) {
      apiErrorNotification(e);
    }
  }

  async fetchNextMonthSummary() {
    const nextMonthDateFilter = this.nextMonthQuery();

    try {
      await dashboardSummary.nextMonthDashboardSummary.fetchData(nextMonthDateFilter);
    } catch (e) {
      apiErrorNotification(e);
    }
  }

  fetchProjectedSummaryData() {
    // noinspection JSIgnoredPromiseFromCall
    this.fetchThisMonthSummary();
    // noinspection JSIgnoredPromiseFromCall
    this.fetchNextMonthSummary();
  }

  fetchProjectList() {
    const filter = {
      ...dashboardProjectList.query.get(),
      ...this.paymentDateQuery(),
    };

    try {
      dashboardProjectList.fetchData(filter);

      // check if invoiceList has been requested (currently check invoice list only)
      const projectDetailsRequested = dashboardProjectDetails.invoiceList.isRequested.get();
      const filterWithoutSortData = omit(filter, ['sort', 'order']);

      if (projectDetailsRequested) {
        dashboardProjectDetails.invoiceList.query.update(filterWithoutSortData);
        dashboardProjectDetails.expensesList.query.update(filterWithoutSortData);

        dashboardProjectDetails.invoiceList.fetchData();
        dashboardProjectDetails.expensesList.fetchData();
      }
    } catch (e) {
      apiErrorNotification(e)
    }
  }

  // =============================== Overdue Invoices ==================================

  fetchOverdueList() {
    // dashboardOverdue.invoiceList.fetchData();
  }
}

export const dashboardFilter = new DashboardFilter();
