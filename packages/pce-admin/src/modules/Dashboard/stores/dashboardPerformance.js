import Vue from 'vue'
import {ApiStore, BaseStore, Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {projectListColumns} from '../resources/projectManagerPerformance'

export const projectManagerSummaryApi = (params) => Vue.axios.get('api/v2/project-managers/project-summary', {
  params,
});

const fetchDashboardProjectList = (params) => Vue.axios.get('/api/v2/dashboard/projects', {
  params
});

@Store('projectManagerSummary')
class ProjectManagerSummary extends BaseStore {
  static mixins = [
    new ApiStore(projectManagerSummaryApi, {}),
    new TableStore({
      tableColumns: [],
      options: {
        listMapWithTableColumns: true,
      }
    })
  ];
}

@Store('projectManagerProjectsDrillDown')
class ProjectManagerProjectsDrillDown extends BaseStore {
  static mixins = [
    new ApiStore(fetchDashboardProjectList, []),
    new TableStore({
      tableColumns: projectListColumns,
      options: {
        listMapWithTableColumns: true,
      }
    })
  ];
}

export const projectManagerSummary = new ProjectManagerSummary();
export const projectManagerProjectsDrillDown = new ProjectManagerProjectsDrillDown();
