import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {Nested, State} from '@nixel/stores';
import {defaultFilterQuery} from '@nixel/stores/src/consts/stores';

const dashboardProjectInvoiceListApi = (params) => Vue.axios.get(`api/v2/projects/${params.uuid}/invoices`, {
  params,
});

const dashboardProjectExpensesListApi = (params) => Vue.axios.get(`api/v2/projects/${params.uuid}/expenses`, {
  params,
});

@Store('invoiceList')
class DashboardProjectInvoiceList extends BaseStore {
  static mixins = [
    new ApiStore(dashboardProjectInvoiceListApi, []),
    new TableStore({
      tableColumns: [
        {
          text: 'Invoice Date',
          value: 'invoice_date',
          type: 'date',
          sortable: true,
        },
        {
          text: 'Due Date',
          value: 'due_date',
          type: 'date',
          sortable: true,
        },
        {
          text: 'Cert No.',
          value: 'cert_no',
          align: 'left',
          sortable: true,
        },
        {
          text: 'Work Done Certified',
          value: 'work_done_certified',
          type: 'currency',
          tag: 'p',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Invoice Amount',
          value: 'total_invoiced',
          type: 'currency',
          tag: 'p',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Total Receivable',
          value: 'total_receivable',
          type: 'currency',
          tag: 'p',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Total Received',
          value: 'total_received_in_period',
          type: 'currency',
          sortable: true,
        },
      ],
    })
  ];

  @State
  query = defaultFilterQuery;
}

@Store('expensesList')
class DashboardProjectExpensesList extends BaseStore {
  static mixins = [
    new ApiStore(dashboardProjectExpensesListApi, []),
    // contractor
    // date_received
    // ref_no
    // invoice_amt
    // payment_amt

    // payment_details
    // ref_no
    // tax_amt
    // total_expenses
    // total_overpaid
    // total_paid
    // total_paid_in_period
    // total_payable:
    new TableStore({
      tableColumns: [
        {
          text: 'Contractor',
          value: 'contractor_name',
          width: '100px',
          align: 'left',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Date Received',
          value: 'date_received',
          type: 'date',
          sortable: true,
        },
        {
          text: 'Due Date',
          value: 'due_date',
          type: 'date',
          sortable: true,
        },
        {
          text: 'Ref No.',
          value: 'ref_no',
          align: 'left',
          width: '90px',
          type: 'singleLine',
          sortable: true,
        },
        {
          text: 'Total Expenses',
          value: 'total_expenses',
          type: 'currency',
          tag: 'p',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Total Payable',
          value: 'total_payable',
          type: 'currency',
          tag: 'p',
          align: 'right',
          sortable: true,
        },
        {
          text: 'Total Paid',
          value: 'total_paid_in_period',
          type: 'currency',
          align: 'right',
          sortable: true,
        },
      ],
    })
  ];

  // @State
  // query = defaultFilterQuery;
}

@Store('dashboardProjectDetails')
class DashboardProjectDetails extends BaseStore {
  @Nested
  invoiceList = new DashboardProjectInvoiceList();

  @Nested
  expensesList = new DashboardProjectExpensesList();
}

export const dashboardProjectDetails = new DashboardProjectDetails();
