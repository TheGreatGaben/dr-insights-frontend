import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';
import {FormStore} from '@nixel/stores';
import {State} from '@nixel/stores';
import {date} from '@nixel/base/filters/datetime';

import {pendingExpensesTableHeaders} from '../resources/pendingExpenses'

const fetchPendingExpensesApi = (params) => Vue.axios.get('/api/v2/expenses?status=pending', {params});
export const writeOffExpensesApi = (params) => Vue.axios.put(`/api/v2/write-off/expenses/${params.uuid}`, params);

function pendingExpensesDataFormatter(response) {
  const pendingExpenses = response.data.data ? response.data.data : response.data;
  return pendingExpenses.map(pending => {
    return {
      ...pending,
      due_date: date(pending.due_date),
    }
  })
}

@Store('pendingExpenses')
class PendingExpenses extends BaseStore {

  @State
  expenseToWriteOff = {}

  static mixins = [
    new ApiStore(fetchPendingExpensesApi, [], pendingExpensesDataFormatter),
    new FormStore({
      fields: {
        reason: {
          value: '',
          placeholder: 'If yes, please give a reason for writing it off.',
          label: 'Reason',
          rules: 'required|max:50',
        },
      },
    }),
    new TableStore({
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      },
      defaultSelectedColumns: [
        'project_name',
        'invoice_date',
        'due_date',
        'overdue',
        'contractor_name',
        'ref_no',
        'description',
        'invoice_amt',
        'tax_amt',
        'dc_note',
        'total_payment',
        'balance',
        'payment_amt',
      ],
      tableColumns: [
        ...pendingExpensesTableHeaders,

        // Inputs
        {
          text: 'Payment Amount',
          value: 'payment_amt',
        },
        {
          text: 'Payment Date',
          value: 'payment_date',
          style: {
            width: '400px',
            padding: '10px 0 10px 0',
          }
        },
        {
          text: 'Cheque No.',
          value: 'cheque_no',
          style: {
            width: '400px',
            padding: '10px',
          },
        },

        {
          text: 'Write Off',
          value: 'write_off',
        },
      ],
    })
  ];

/*
  @State
  query = {
    ...defaultPaginationQuery,
    ...defaultFilterQuery,
    sort: 'due_date',
    order: 'DESC',
  };
*/
}

export const pendingExpenses = new PendingExpenses();

