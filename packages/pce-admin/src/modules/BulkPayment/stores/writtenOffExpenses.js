import Vue from 'vue'
import {ApiStore} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {TableStore} from '@nixel/stores';

import {writtenOffExpensesTableHeaders} from '../resources/pendingExpenses'

const fetchWrittenOffExpensesApi = (params) => Vue.axios.get('/api/v2/expenses?status=writeOff', {params});
export const undoWrittenOffExpensesApi = (params) => Vue.axios.delete(`/api/v2/write-off/expenses/${params.uuid}`);

@Store('writtenOffExpenses')
class WrittenOffExpenses extends BaseStore {
  static mixins = [
    new ApiStore(fetchWrittenOffExpensesApi, []),
    new TableStore({
      options: {
        selectableRows: true,
        filterTableColumns: true,
        listMapWithTableColumns: true,
      },
      tableColumns: writtenOffExpensesTableHeaders,
    })
  ]
}

export const writtenOffExpenses = new WrittenOffExpenses();
