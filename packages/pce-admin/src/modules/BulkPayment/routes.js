/**
 *
 * @type {RouterOptions[]}
 */
import BulkPayment from './BulkPayment'

export const bulkPaymentRoutes = [
  {
    path: '/bulk-payment',
    name: 'BulkPayment',
    meta: {
      authRequired: true,
      permissions: ['project.expense'], // hide reports page if user don't have permission to use
      title: 'Bulk Payment',
      icon: 'mdi-file-document-box-multiple'
    },
    redirect: '/bulk-payment/pending-expenses',
    component: BulkPayment,
    children: [
      {
        path: 'pending-expenses',
        name: 'PendingExpenses',
        meta: {
          authRequired: true,
          title: 'Pending Expenses',
        },
        component: () => import(/* webpackChunkName: "pendingExpensesList" */ './PendingExpensesList.vue'),
        children: [
          {
            path: ':uuid/write-off',
            name: 'expenseWriteOff',
            meta: {
              authRequired: true,
              title: 'Expense Write Off',
            },
          },
        ]
      },
      {
        path: 'payment-history',
        name: 'PaymentHistory',
        meta: {
          authRequired: true,
          title: 'Payment History',
        },
        component: () => import(/* webpackChunkName: "paymentHistoryList" */ './PaymentHistoryList.vue'),
      },
      {
        path: 'write-off-expenses',
        name: 'WrittenOffExpenses',
        meta: {
          authRequired: true,
          title: 'Write Offs',
        },
        component: () => import(/* webpackChunkName: "writtenOffExpensesList" */ './WriteOffList.vue'),
      },
    ]
  }
];
