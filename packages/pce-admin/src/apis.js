import Vue from 'vue';

export function forgotPasswordApi({email}) {
  return Vue.axios.post('/api/password/forgot', {
    email,
    isAdmin: true,
  });
}
