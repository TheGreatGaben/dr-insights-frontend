import Vue from 'vue';
import Vue2Filters from 'vue2-filters';
import VueTheMask from 'vue-the-mask';
import Vuebar from 'vuebar';
import Meta from 'vue-meta';
import ECharts from 'vue-echarts' // refers to components/ECharts.vue in webpack
import 'echarts/lib/chart/scatter'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/graphic'
import 'echarts/lib/component/legend'

import './axios';
import './chartist';
import './sticky';
import './vuetify';
import vuetify from './vuetify';
import installConfirm from './confirm';
import {installConfirmOnce} from './confirmOnce';

/**
 * install all plugins
 */
Vue.use(Meta);
Vue.use(Vuebar);
Vue.use(installConfirm, {
  vuetify,
  title: 'Confirmation',
  icon: 'mdi-alert',
});
Vue.use(installConfirmOnce);

Vue.use(Vue2Filters);
Vue.use(VueTheMask);
Vue.component('v-chart', ECharts);

