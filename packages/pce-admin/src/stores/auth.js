import {isEmpty} from 'lodash';
import Vue from 'vue';
import {BaseStore, ApiStore} from '@nixel/stores';
import {
  State,
  Action,
  Store,
  Getter,
  Nested,
} from '@nixel/stores';
import {
  getImpersonationToken,
  getImpersonationTokenID,
  getToken,
  removeImpersonationToken,
  removeToken,
} from '../utils/auth';

const fetchUserDetailsApi = () => Vue.axios.get('/api/v2/user/details');

// nested stores
@Store('userDetails')
class UserDetails extends BaseStore {
  static mixins = [
    new ApiStore(fetchUserDetailsApi, {})
  ];

  @Getter
  getUserName(state) {
    return state.data && state.data.name;
  }

  @Getter
  getCurrentRoles(state) {
    return state.data && state.data.roles;
  }
}

@Store('auth')
class Auth extends BaseStore {
  @Nested
  userDetails = new UserDetails();

  @State
  roles = [];

  @State
  roleNames = [];

  @State
  isAuthenticated = false;

  @State
  permissions = [];

  @State
  accessibleRoutes = [];

  // won't update till next reload
  @State
  isImpersonated = !isEmpty(getImpersonationTokenID()) && !isEmpty(getImpersonationToken());

  @Getter
  permissionNameList() {
    const permissionList = this.permissions.get();

    return permissionList.map(permission => permission.name)
  }

  @Getter
  token() {
    return getToken();
  }

  @Getter
  hasToken() {
    return !!this.token();
  }

  @Action
  logout() {
    removeToken();
    removeImpersonationToken();
    this.resetAll(true)
  }
}

export const auth = new Auth();
