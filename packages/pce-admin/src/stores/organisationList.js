import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import Vue from 'vue';

const fetchOrganisationListApi = (params) => Vue.axios.get('/api/v2/organisations', {
  params
});

@Store('organisationList')
class OrganisationList extends BaseStore {
  static mixins = [
    new ApiStore(fetchOrganisationListApi, []),
  ]
}

export const organisationList = new OrganisationList();
