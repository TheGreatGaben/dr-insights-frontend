import {ApiStore} from '@nixel/stores';
import {Store} from '@nixel/stores';
import {BaseStore} from '@nixel/stores';
import Vue from 'vue';

export const fetchAccountCodesApi = (params) => {
  if (params && params.code) {
    return Vue.axios.get('/api/v2/finance_account_codes/get_child_by_code', {params})
  }

  return Vue.axios.get(`/api/v2/finance_account_codes`, {params})
};

@Store('accountCodeList')
class AccountCodeList extends BaseStore {
  static mixins = [
    new ApiStore(fetchAccountCodesApi, []),
  ];
}

export const accountCodeList = new AccountCodeList();
