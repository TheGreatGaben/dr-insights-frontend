export const contractorTypes = [
  {
    text: 'Supplier',
    value: 1,
  },
  {
    text: 'Subcontractor',
    value: 2,
  },
];
