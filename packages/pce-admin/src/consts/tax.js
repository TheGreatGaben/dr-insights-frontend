import {isAfter} from 'date-fns';

export const SST_RATE = 0.1;
export const GST_RATE = 0.06;

export const taxTypes = [
  {
    text: 'GST',
    value: 'gst',
  },
  {
    text: 'SST',
    value: 'sst',
  },
];

export const taxRates = {
  gst: GST_RATE,
  sst: SST_RATE,
};

export const SST_DATE = '2018-08-31';

export function getTaxTypeByDate(date) {
  return isAfter(new Date(date), new Date(SST_DATE)) ? taxTypes[1].value : taxTypes[0].value;
}
