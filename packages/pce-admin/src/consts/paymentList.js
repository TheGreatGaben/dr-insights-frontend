export const paymentTableColumns = [
  {
    text: 'No.',
    type: 'index',
    align: 'left',
    width: '80px',
  },
  {
    text: 'Project Name',
    value: 'project_name',
    width: '20%',
    type: 'singleLine',
    align: 'left',
    sortable: true,
  },
  {
    text: 'Client / Supplier',
    value: 'client_or_contractor_name',
    sortable: true,
    formatter(payment) {
      if (payment.cash_flow === 'IN') {
        return payment.client_name;
      } else if (payment.cash_flow === 'OUT') {
        return payment.supplier_name;
      } else {
        return '-'
      }
    },
    width: '20%',
    align: 'left',
    type: 'singleLine'
  },
  {
    text: 'Cert No. / Ref No.',
    value: 'cert_or_ref_no',
    sortable: true,
    formatter(payment) {
      if (payment.cash_flow === 'IN') {
        return payment.cert_no;
      } else if (payment.cash_flow === 'OUT') {
        return payment.cheque_no;
      } else {
        return '-'
      }
    },
    align: 'left',
    width: '120px',
    type: 'singleLine',
  },
  {
    text: 'Type',
    value: 'cash_flow',
    width: '60px',
    align: 'center',
    sortable: true,
  },
  {
    text: 'Due Date',
    value: 'due_date',
    type: 'date',
    align: 'right',
    sortable: true,
  },
  {
    text: 'Payment Date',
    value: 'payment_date',
    type: 'date',
    align: 'right',
    sortable: true,
  },
  {
    text: 'Payment Amount',
    value: 'payment_amt',
    type: 'currency',
    align: 'right',
    sortable: true,
    tag: 'p',
  },
];
