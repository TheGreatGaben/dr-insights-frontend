export const PROJECT_LIST_COLUMNS = 'PROJECT_LIST_COLUMNS';
export const DASHBOARD_PROJECT_LIST_COLUMNS = 'DASHBOARD_PROJECT_LIST_COLUMNS';
export const PROJECT_DETAILS_INVOICE_COLUMNS = 'PROJECT_DETAILS_INVOICE_COLUMNS';
export const PROJECT_DETAILS_CONTRACTOR_COLUMNS = 'PROJECT_DETAILS_CONTRACTOR_COLUMNS';
export const PROJECT_DETAILS_EXPENSES_COLUMNS = 'PROJECT_DETAILS_EXPENSES_COLUMNS';

import {isEmpty} from 'lodash';

function getStoredColumns(key) {
  const columns = localStorage.getItem(key);

  if (!isEmpty(columns)) {
    return JSON.parse(localStorage.getItem(key));
  }

  return undefined;
}

export const getSelectedColumns = (key) => {
  return getStoredColumns(key)
};

export const setSelectedColumns = (key, columns) => {
  return localStorage.setItem(key, columns);
};

// Project list
export const getProjectListSelectedColumns = () => {
  return getStoredColumns(PROJECT_LIST_COLUMNS);
};
export const setProjectListSelectedColumns = columns => {
  return localStorage.setItem(PROJECT_LIST_COLUMNS, columns);
};

// Dashboard project list
export const getDashboardProjectListSelectedColumns = () => {
  return getStoredColumns(DASHBOARD_PROJECT_LIST_COLUMNS);
};
export const setDashboardProjectListSelectedColumns = columns => {
  return localStorage.setItem(DASHBOARD_PROJECT_LIST_COLUMNS, columns);
};
