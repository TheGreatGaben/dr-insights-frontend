export const durationByLAFormula = {
  label:
    '100 - ((Project End Date - Project Start Date) - (Today - Project Start Date) / (Project End Date - Project Start Date) * 100)',
  value({endDate, startDate, today}) {
    return `100 - ((${endDate} - ${startDate}) - (${today} - ${startDate}) / (${endDate} - ${startDate}) * 100)`;
  },
};

export const progressFormula = {
  label:
    "Indicates the health of the project's cashflow. Duration by LA (%) - Current Duration (%)",
  value({durationLA, currentDuration}) {
    return `${currentDuration} - ${durationLA}`;
  },
};

export const currentDurationFormula = {
  // $project->contract_sum + $project->variation_order - $project->retention
  // ($project->contract_sum + $project->variation_order - $project->budget) / ($project->contract_sum + $project->variation_order) * 100;
  label: '(Invoiced Amount / (Contract Sum + Variation Order - Retention)) * 100',
  value({invoiced, contractSum, variationOrder, retention}) {
    return `(${invoiced} / (${contractSum} + ${variationOrder} - ${retention})) * 100`;
  },
};

export const profitMarginFormula = {
  label: '(Invoiced Amount - Expenses Amount) / Invoiced Amount * 100',
  value({invoiced, expenses}) {
    return `(${invoiced} - ${expenses}) / ${invoiced} * 100`;
  },
};

export const projectedMarginFormula = {
  label: '(Contract Sum + Variation Order - Budget) / (Contract Sum + Variation Order) * 100',
  value({contractSum, variationOrder, expenses}) {
    return `(${contractSum} + ${variationOrder} - ${expenses}) / (${contractSum} + ${variationOrder}) * 100`;
  },
};

export const cashflowMarginFormula = {
  label: '(Collected Amount - Paid Amount) / Collected Amount * 100',
  value({collected, paid}) {
    return `(${collected} - ${paid}) / ${collected} * 100`;
  },
};

export const currentMarginFormula = {
  label:
    '(Contract Sum + Variation Order - Retention - Budget) / (Contract Sum + Variation Order - Retention) * 100',
  value({contractSum, variationOrder, retention, budget}) {
    return `(${contractSum} + ${variationOrder} - ${retention} - ${budget}) / (${contractSum} + ${variationOrder} - ${retention}) * 100`;
  },
};

export const workCertifiedMarginFormula = {
  label: 'Invoiced Amount / (Contract Sum + Variation Order) * 100',
  value({invoiced, contractSum, variationOrder}) {
    return `${invoiced} / (${contractSum} + ${variationOrder}) * 100`;
  },
};

// use in project listing
export const projectProfitMarginFormula =
  '(Collected Amount - Paid Amount) / Collected Amount * 100';

// use in project listing
export const projectCurrentDurationFormula = '(Invoiced / (Contract Sum + Variation Order)) * 100';
