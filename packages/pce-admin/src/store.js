import Vue from 'vue';
import Vuex from 'vuex';

// global stores
import {auth} from './stores/auth';
import {appStore} from './stores/appStore';
import {vuexPlugin} from '@nixel/stores/src/install';

Vue.use(Vuex);

export const store = new Vuex.Store({
  plugins: [vuexPlugin],
  modules: {
    appStore,
    auth,
  },
  // strict: isDevelopment,
});
