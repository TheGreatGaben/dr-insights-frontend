const APP_VERSION = 'APP_VERSION';

// check app version, when there's version bump, clean all localStorage
if (process.env.NODE_ENV === 'production') {
  const getVersion = localStorage.getItem(APP_VERSION);

  if (process.env.VUE_APP_VERSION && process.env.VUE_APP_VERSION !== getVersion) {
    // clear all storage on version bump
    localStorage.clear();

    // set version
    localStorage.setItem(APP_VERSION, process.env.VUE_APP_VERSION);
  }
}
