import {get} from 'lodash';
import {log} from '../utils';
import {isStoreError} from '@nixel/stores';
import {toastError} from '@nixel/base/plugins/apiErrorToast';

export const apiErrorNotification = (error, fallbackMessage = 'Something went wrong.') => {
  const isOffline = !navigator.onLine;

  // if is offline, only show 1 offline message popup
  if (isOffline) {
    // offlineMessagePopup()

    return;
  }

  const ignoreError = isStoreError(error);
  const serverError = get(error, 'response.status') === 500;

  if (!ignoreError && !serverError) {
    let errMessage =
      get(error, 'response.data.data.err_msg') ||
      get(error, 'response.data.data.message') ||
      get(error, 'response.data.message');

    // if error is a string, use this error as message
    if (typeof error === 'string') {
      errMessage = error;
    } else if (errMessage === undefined) {
      // Validation errors returned from Laravel backend are of
      // different format, e.g:
      // ...
      // errors: { field: ['The field is required'] }
      // ...
      const validationErrors =
        get(error, 'response.data.data.errors') || get(error, 'response.data.errors');
      if (validationErrors === undefined) {
        errMessage = fallbackMessage;
      } else {
        errMessage = '<ul>';
        for (const field in validationErrors) {
          // noinspection JSUnfilteredForInLoop
          errMessage += `<li>${validationErrors[field][0]}</li>`;
        }
        errMessage += '</ul>';
      }
    }

    log('we got errors: ', error, errMessage);
    toastError(errMessage)
  }
};
