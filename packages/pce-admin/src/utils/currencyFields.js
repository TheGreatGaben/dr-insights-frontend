export const DEFAULT_CURRENCY_OPTIONS = {
  prefix: 'RM ',
  suffix: '',
  allowBlank: false,
  min: Number.MIN_SAFE_INTEGER,
  max: Number.MAX_SAFE_INTEGER,
  decimal: '.',
  thousands: ',',
  precision: 2,
};

export function format(input, opt = DEFAULT_CURRENCY_OPTIONS) {
  if (opt.allowBlank && input === '') {
    return '';
  }

  if (typeof input === 'number') {
    if (input > opt.max) {
      input = opt.max;
    } else if (input < opt.min) {
      input = opt.min;
    }
    input = input.toFixed(fixed(opt.precision));
  }
  const negative = input.indexOf('-') >= 0 ? '-' : '';

  const numbers = onlyNumbers(input);
  const currency = numbersToCurrency(numbers, opt.precision);
  const parts = toStr(currency).split('.');
  const decimal = parts[1];
  let integer = parts[0];

  integer = addThousandSeparator(integer, opt.thousands);
  return negative + joinIntegerAndDecimal(integer, decimal, opt.decimal);
}

export function unformat(input, precision) {
  if (!input) {
    return input;
  }

  if (typeof input === 'number') {
    return input;
  }

  const negative = input.indexOf('-') >= 0 ? -1 : 1;
  const numbers = onlyNumbers(input);
  const currency = numbersToCurrency(numbers, precision);
  return parseFloat(currency) * negative;
}

function onlyNumbers(input) {
  return toStr(input).replace(/\D+/g, '') || '0';
}

// Uncaught RangeError: toFixed() digits argument must be between 0 and 20 at Number.toFixed
function fixed(precision) {
  return between(0, precision, 20);
}

function between(min, n, max) {
  return Math.max(min, Math.min(n, max));
}

function numbersToCurrency(numbers, precision) {
  const exp = Math.pow(10, precision);
  const float = parseFloat(numbers) / exp;
  return float.toFixed(fixed(precision));
}

function addThousandSeparator(integer, separator) {
  return integer.replace(/(\d)(?=(?:\d{3})+\b)/gm, `$1${separator}`);
}

function joinIntegerAndDecimal(integer, decimal, separator) {
  return decimal ? integer + separator + decimal : integer;
}

function toStr(value) {
  return value ? value.toString() : '';
}
