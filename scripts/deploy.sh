#!/usr/bin/env bash
# CI variables
#
# PROJECT_NAME
# SSH_PRIVATE_KEY - key for ssh access server
# SSH_PATH - exp: user@address:/project/path

DIST_FOLDER=./packages/${PROJECT_NAME}/dist/
SSH_ADDRESS="$SSH_PATH/$PROJECT_NAME"
SSH_FOLDER=/$(whoami)/.ssh
SSH_KEY_PATH=${SSH_FOLDER}/id_rsa

mkdir -p ${SSH_FOLDER}

# create SSH key
touch ${SSH_FOLDER}/id_dsa
echo "$SSH_PRIVATE_KEY" > ${SSH_KEY_PATH}
chmod 700 ${SSH_KEY_PATH}
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
echo "deploying $PROJECT_NAME..."
echo "ssh path - $SSH_ADDRESS"
rsync -rav --delete ${DIST_FOLDER} -e ssh ${SSH_ADDRESS}
