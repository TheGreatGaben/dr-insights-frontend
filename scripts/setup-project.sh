#!/usr/bin/env bash

PROJECT_NAME=$1
PROJECT_DOMAIN=$2 # exp: domain.example
FOLDER_ARG=$3
PROJECT_FOLDER=${FOLDER_ARG:-/var/www} # exp: /var/www (default)
NGINX_SITE_AVAILABLE_FOLDER=/etc/nginx/sites-enabled
NEW_FILE_PATH=${NGINX_SITE_AVAILABLE_FOLDER}/${PROJECT_DOMAIN}

# add folder
mkdir -p ${PROJECT_FOLDER}/${PROJECT_DOMAIN}
# copy config template
cp ./templates/nginx-config.txt ${NEW_FILE_PATH}
# replace placeholder text
sed -e "s/<%DOMAIN_NAME%>/$PROJECT_DOMAIN/g" ${NEW_FILE_PATH}
